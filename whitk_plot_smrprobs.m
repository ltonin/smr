function whitk_plot_smrprobs(pp, upp, evt, th)

    NumPpTypes = size(upp, 3);

    [pp, tstop, tcls] = trial_extraction(pp, evt);
    
    post_pp = zeros(size(pp, 1), size(pp, 2), NumPpTypes);
    for i = 1:NumPpTypes
        cupp = trial_extraction(upp(:, :, i), evt);
        post_pp(:, :, i) = cupp;
    end
        
%     post_pp = trial_extraction(upp, evt);
    hold on;
    plot(pp(:, 2), '.', 'Color', [190 190 190]/255);
    plot(squeeze(post_pp(:, 2, :)));
    plot(norm_lb(tcls, -0.2, 1.2), '.r');
    plot_vline(tstop, 'k');
    plot_hline(th, 'k');
    hold off;
    keyboard
end

function [pp_cut, pos, cls] = trial_extraction(pp, evt)
    
%     ClassEvents = [769 770 771];
%     NumClasses  = length(ClassEvents);

    TrialStart = find(evt == 781);
    TrialStop  = find(evt == 897 | evt == 898);
    NumTrials = length(TrialStart);
    
    [TrialCls, TrialClsLb] = get_classes(evt, [769 770 771]);
    
    
%     keyboard
    
    pp_cut = [];
    pos = [];
    cls = [];
    for trId = 1:NumTrials
        cstart = TrialStart(trId);
        cstop  = TrialStop(trId);
        cclass = TrialClsLb(trId);
        pp_cut = cat(1, pp_cut, pp(cstart:cstop, :));
        pos    = cat(1, pos, length(pp_cut));
        cls    = cat(1, cls, cclass*ones(length(cstart:cstop), 1));
    end
    
end

function [ClassEvt, ClassLb] = get_classes(evt, dflclass)

    cevents = false(length(evt), 1);
    for i = 1:length(dflclass)
        cevents  = cevents | evt == dflclass(i);
    end
    
    ClassEvt = evt(cevents);
        
    Classes    = unique(ClassEvt);
    NumClasses = length(Classes);
    ClassLb = ClassEvt;
    
    for cId = 1:NumClasses
        ClassLb(ClassEvt == Classes(cId)) = cId;
    end
end

function val = norm_lb(lb, minV, maxV)

    y_n = ((lb - min(lb))./(max(lb) - min(lb)));
    val = y_n*(abs(maxV) + abs(minV)) - abs(minV);

end

