clear all; clc; close all;

rootpath   = '/mnt/data/Research/smr/';
subfolder  = '20150311_b4/';
TrainFiles = {[rootpath subfolder 'b4.20150311.121421.offline.mi.mi_rhbf.gdf'];
              [rootpath subfolder 'b4.20150311.122133.offline.mi.mi_rhbf.gdf'];
              [rootpath subfolder 'b4.20150311.123433.online.mi.mi_rhbf.gdf'];
              [rootpath subfolder 'b4.20150311.125413.online.mi.mi_rhbf.gdf'];
              [rootpath subfolder 'b4.20150311.130042.online.mi.mi_rhbf.gdf']};
% subfolder  = '20150326_p1/';
% TrainFiles = {[rootpath subfolder 'p1.20150326.152559.offline.mi.mi_rlbf.gdf'];
%               [rootpath subfolder 'p1.20150326.153721.offline.mi.mi_rlbf.gdf'];
%               [rootpath subfolder 'p1.20150326.154756.offline.mi.mi_rlbf.gdf'];
%               [rootpath subfolder 'p1.20150326.163128.online.mi.mi_bflh.gdf'];
%               [rootpath subfolder 'p1.20150326.164002.online.mi.mi_bflh.gdf'];
%               [rootpath subfolder 'p1.20150326.165944.online.mi.mi_bflh.gdf']};
               

%% Configuration Windows/Buffer
buffercfg.winshift = 32;
buffercfg.winsize  = 512;

%% Configuration events
evtcfg.cues      = [770 771];
%evtcfg.cues      = [769 771];
evtcfg.cfeedback = 781;
evtcfg.minduration = 3*512;

%% Configuration Gaussian classifier
gaucfg.somunits     = [2 2];
gaucfg.sharedcov    = 't';
gaucfg.mimean       = 0.1;
gaucfg.micov        = 0.01;
gaucfg.epochs       = 20;
gaucfg.th           = 0.7;
gaucfg.terminate    = true;

%% Configuration QDA
qdacfg.priors       = [0.5 0.5];
qdacfg.sph          = 0;
qdacfg.nrm          = 0;
qdacfg.diag         = 0;


%% Train classifiers
[CSP, gau, qda] = wtk_smr_train_csp(TrainFiles, buffercfg, evtcfg, gaucfg, qdacfg);