function [idevents, idtasks] = fcnbi_gettasks(events, tasktypes)

    if nargin < 2
        tasktypes = [769 770 771];
    end
    
    
    idtasks = [];
    idevents = false(size(events));
    
    for i = 1:length(tasktypes)
        cres = events == tasktypes(i);
        idevents = idevents | cres;
        
        if sum(cres) > 0
            idtasks = cat(1, idtasks, tasktypes(i));
        end
        
    end
        


end