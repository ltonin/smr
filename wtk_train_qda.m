function qda = wtk_train_qda(data, labels, qdacfg)
% function qda = wtk_train_qda(data, labels, qdacfg [, qdastr])
%
% Function to train a QDA classifier. From te input data, random 70% is
% used for training and the remaining 30% for testing. Function utilizes
% qda_train and qda_test functions.
%
% Inputs:
%
% data:         Data matrix samples x features
%
% labels:       Labels vector samples x 1
%
% qdacfg:       Struct array with the following field:
%               .priors     (typical: [0.5 0.5])
%               .sph        (typical: 0)
%               .nrm        (typical: 0)
%               .diag       (typical: 0)
%
% qdastr:       NOT IMPLEMENTED
%
% Outputs:
%
% qdastr MATLAB struct containing the means and covariances of the QDA
% classifier 



    values = unique(labels);
    if(length(values) ~= 2)
        error('chk:lbl', 'Labels vector must have only two unique values (classes)');
    end

    labels(labels == values(1)) = 1;
    labels(labels == values(2)) = 2;

    % Split possible 50/50
    % Find class samples
    C{1} = find(labels==1);
    C{2} = find(labels==2);

    % Find minimum
    [CminN Cmin]  = min([length(C{1}) length(C{2})]);
    [CmaxN Cmax]  = max([length(C{1}) length(C{2})]);
    disp(['[wtk_train_qda] Dropping ' num2str(100*(CmaxN-CminN)/CmaxN) '% first'... 
        ' coming data of class ' num2str(Cmax) ' to avoid unbalanced classes']);
    C{Cmax} = C{Cmax}(CmaxN-CminN+1:end);
    dataind = union(C{1},C{2});
    bdata = data(dataind,:);
    blabels = labels(dataind,:);


    % Random permutation
    brand = randperm(length(blabels));
    blabels = blabels(brand);
    bdata = bdata(brand,:);

    % Now split to training and testing set, 70/30 split
    Pind = [1:round(0.7*length(blabels))];
    Tind = [max(Pind)+1:length(blabels)];

    P = bdata(Pind,:);
    Pk = blabels(Pind,:);

    T = bdata(Tind,:);
    Tk = blabels(Tind,:);

    Size = size(bdata,1);
    TrSize = size(Pk,1);

    % Test the balance of samples in training set
    Pc1 = length(find(Pk==1))/TrSize;
    Pc2 = length(find(Pk==2))/TrSize;

    disp(['[wtk_train_qda] Training samples (%): Class 1 vs Class 2: '...
        num2str(100*Pc1) ' - ' num2str(100*Pc2)]);
    if(abs(Pc1-Pc2)>0.2)
        disp(['[wtk_train_qda] Warning: Training samples seem to be too unbalanced...']);
    end

    % Test the balance of samples in testing set
    Tc1 = length(find(Tk==1))/(Size-TrSize);
    Tc2 = length(find(Tk==2))/(Size-TrSize);
    disp(['[wtk_train_qda] Testing samples (%): Class 1 vs Class 2: '...
        num2str(100*Tc1) ' - ' num2str(100*Tc2)]);
    if(abs(Tc1-Tc2)>0.2)
        disp(['[wtk_train_qda] Warning: Testing samples seem to be too unbalanced...']);
    end
    
    % Train QDA classifier
    qda = qda_train(P, Pk, qdacfg.priors, qdacfg.sph, qdacfg.nrm, qdacfg.diag);
    
    
    % Test QDA classifier
    ppP = qda_test(qda, P);
    ppT = qda_test(qda, T);
    
    % Performance single sample
    perfP = 100*sum( (ppP(:, 1) > 0.5) == (Pk == 1))./length(Pk);
    perfT = 100*sum( (ppT(:, 1) > 0.5) == (Tk == 1))./length(Tk);
    
    figure;
    bar([perfP; perfT]);
    set(gca, 'XTickLabel', {'TrainSet', 'TestSet'});
    xlim([0 3]);
    ylim([0 100]);
    ylabel('Performance [%]');
    title('QDA classifier');
    grid on;
    
end