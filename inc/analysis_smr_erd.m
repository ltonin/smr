clear all; clc;

rootpath = '/mnt/data/Research/smr/20150311_b4/';
subject  = 'b4';
FilesId = {'All'};

EVTSTART= 1;
EVTFIX  = 786;
EVTCUES = [769 770 771];
EVTFEED = 781;
EVTENDS = [897 898];

WinShift = 32;  % Samples
FreqGrid = 0:2:256;
NumFreqs = length(FreqGrid);
Fs       = 512;
CLS_INC  = 1;
CLS_CTR  = 2;


PSDfiles = util_getfiles({rootpath}, '.dat', 'online', FilesId);
GDFfiles = util_getfiles({rootpath}, '.gdf', 'online', FilesId);

psd = [];
for fId = 1:length(PSDfiles)
    cpsd = wc_load(PSDfiles{fId}, 'eigen', 'double');
    csamples = size(cpsd, 1)/NumFreqs;
    cchannels = size(cpsd, 2);
    cpsd = wc_2dTo3d(cpsd, [NumFreqs cchannels csamples]);
    psd = cat(1, psd, permute(cpsd, [3 1 2]));
end

[~, h] = util_cat_gdf(GDFfiles(:));

NumSamples  = size(psd, 1);
NumChannels = size(psd, 3);
NumTrials   = sum(h.typ == 1);

%%%% Labeling
[LblInc, PosInc] = util_getlabels(h.typ, floor(h.pos/WinShift), EVTFIX, EVTFEED, NumSamples);
[LblEng, PosEng] = util_getlabels(h.typ, floor(h.pos/WinShift), EVTFEED, EVTENDS, NumSamples);
[LblTrl, PosTrl] = util_getlabels(h.typ, floor(h.pos/WinShift), EVTFIX, EVTENDS, NumSamples);

%% ERD/ERS
PSDRef      = zeros(NumFreqs, NumChannels, NumTrials);
MinTrialLen = min(diff(PosTrl, [], 2));
MinFeedLen  = min(diff(PosEng, [], 2));

for trId = 1:size(PosInc, 1)
    cstart = PosInc(trId, 1);
    cstop  = PosInc(trId, 2);
    PSDRef(:, :, trId) = squeeze(mean(psd(cstart:cstop, :, :), 1));
end

ERD = zeros(MinTrialLen, NumFreqs, NumChannels, NumTrials);
for trId = 1:size(PosTrl, 1)
    cstart = PosTrl(trId, 1);
    cstop  = cstart + MinTrialLen -1;
    cpsd   = psd(cstart:cstop, :, :);
    cref   = repmat(PSDRef(:, :, trId), [1 1 length(cstart:cstop)]);
    cref   = permute(cref, [3 1 2]);
    cerd   = 100*(cref - cpsd)./cref;
    ERD(:, :, :, trId) = cerd;
end


%% Plotting ERD/ERS
chanlayout = [0 0 1 0 0; 2 3 4 5 6; 7 8 9 10 11; 12 13 14 15 16];
channames = {'Fz', 'FC3', 'FC1', 'FCz', 'FC2', 'FC4', 'C3', 'C1', 'Cz', 'C2', 'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};

% Grand average
SubFreqs    = 4:2:48;
[~, SubFreqIds] = intersect(FreqGrid, SubFreqs);
t = [linspace(1, (MinTrialLen - MinFeedLen), 3) MinTrialLen];
tname = [linspace(- (MinTrialLen - MinFeedLen)*WinShift/Fs, 0, 3) MinFeedLen*WinShift/Fs];
for chId = 1:NumChannels
    ind = find(reshape(chanlayout, [numel(chanlayout) 1]) == chId);
    [nrow, ncol] = ind2sub(size(chanlayout), ind);
    
    
    subPind = ncol + (nrow - 1)*size(chanlayout, 2);
    
    subplot(size(chanlayout, 1), size(chanlayout, 2), subPind);
    imagesc(fliplr(mean(ERD(:, SubFreqIds, chId, :), 4))', [-60 60]);
    plot_vline((MinTrialLen - MinFeedLen), 'k');
    
    set(gca, 'YTick', []);
    set(gca, 'XTick', []);
    title(['Channel ' channames{chId}]);
    
    if (ncol == 1 || (nrow == 1 && ncol == 3))
        set(gca, 'YTick', 1:2:length(SubFreqs));
        set(gca, 'YTickLabel', flipud(num2str(SubFreqs(1:2:end)')));
        ylabel('Frequency [Hz]');
    end
    
    if (nrow == size(chanlayout, 1))
        set(gca, 'XTick', t');
        set(gca, 'XTickLabel', num2str(tname', '%3.1f'));
        xlabel('Time [s]');
    end
    axis normal;
end
