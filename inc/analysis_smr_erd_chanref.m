clear all; clc;

rootpath = '/mnt/data/Research/smr/20150311_b4/';
subject  = 'b4';
FilesId = {'All'};

EVTSTART= 1;
EVTFIX  = 786;
EVTCUES = [769 770 771];
EVTFEED = 781;
EVTENDS = [897 898];

WinShift = 32;  % Samples
FreqGrid = 0:2:256;
NumFreqs = length(FreqGrid);
Fs       = 512;
CLS_INC  = 1;
CLS_CTR  = 2;


PSDfiles = util_getfiles({rootpath}, '.dat', 'online', FilesId);
GDFfiles = util_getfiles({rootpath}, '.gdf', 'online', FilesId);

psd = [];
for fId = 1:length(PSDfiles)
    cpsd = wc_load(PSDfiles{fId}, 'eigen', 'double');
    csamples = size(cpsd, 1)/NumFreqs;
    cchannels = size(cpsd, 2);
    cpsd = wc_2dTo3d(cpsd, [NumFreqs cchannels csamples]);
    psd = cat(1, psd, permute(cpsd, [3 1 2]));
end
psd = log(psd+1);
[~, h] = util_cat_gdf(GDFfiles(:));

NumSamples  = size(psd, 1);
NumChannels = size(psd, 3);
NumTrials   = sum(h.typ == 1);

%%%% Labeling
[LblInc, PosInc] = util_getlabels(h.typ, floor(h.pos/WinShift), EVTFIX, EVTFEED, NumSamples);
[LblEng, PosEng] = util_getlabels(h.typ, floor(h.pos/WinShift), EVTFEED, EVTENDS, NumSamples);
[LblTrl, PosTrl] = util_getlabels(h.typ, floor(h.pos/WinShift), EVTFIX, EVTENDS, NumSamples);

MinTrialLen = min(diff(PosTrl, [], 2));
MinFeedLen  = min(diff(PosEng, [], 2));

%%%% ERD/ERS on channel ref
SubFreqs        = 4:2:48;
[~, SubFreqIds] = intersect(FreqGrid, SubFreqs);
NumSubFreqs = length(SubFreqIds);
SubPSD = psd(:, SubFreqIds, :);
cref = mean(SubPSD, 3);
PSDRef = repmat(cref, [1 1 NumChannels]);

ERD = (PSDRef - SubPSD)./PSDRef;

ERDr = zeros(MinTrialLen, NumSubFreqs, NumChannels, NumTrials);
for trId = 1:size(PosTrl, 1)
    cstart = PosTrl(trId, 1);
    cstop  = cstart + MinTrialLen -1;
    ERDr(:, :, :, trId) = ERD(cstart:cstop, :, :);
end

%% Plotting ERD/ERS
figure;
chanlayout = [0 0 1 0 0; 2 3 4 5 6; 7 8 9 10 11; 12 13 14 15 16];
channames = {'Fz', 'FC3', 'FC1', 'FCz', 'FC2', 'FC4', 'C3', 'C1', 'Cz', 'C2', 'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};

% Grand average
t = [linspace(1, (MinTrialLen - MinFeedLen), 3) MinTrialLen];
tname = [linspace(- (MinTrialLen - MinFeedLen)*WinShift/Fs, 0, 3) MinFeedLen*WinShift/Fs];
for chId = 1:NumChannels
    ind = find(reshape(chanlayout, [numel(chanlayout) 1]) == chId);
    [nrow, ncol] = ind2sub(size(chanlayout), ind);
    
    
    subPind = ncol + (nrow - 1)*size(chanlayout, 2);
    
    subplot(size(chanlayout, 1), size(chanlayout, 2), subPind);
    imagesc(fliplr(mean(ERDr(:, :, chId, :), 4))');
    plot_vline((MinTrialLen - MinFeedLen), 'k');
    
    set(gca, 'YTick', []);
    set(gca, 'XTick', []);
    title(['Channel ' channames{chId}]);
    
    if (ncol == 1 || (nrow == 1 && ncol == 3))
        set(gca, 'YTick', 1:2:length(SubFreqs));
        set(gca, 'YTickLabel', flipud(num2str(SubFreqs(1:2:end)')));
        ylabel('Frequency [Hz]');
    end
    
    if (nrow == size(chanlayout, 1))
        set(gca, 'XTick', t');
        set(gca, 'XTickLabel', num2str(tname', '%3.1f'));
        xlabel('Time [s]');
    end
    axis normal;
end







