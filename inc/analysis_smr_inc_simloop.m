clear all; clc;

rootpath     = '/mnt/data/Research/smr/';
subfolder    = '20150311_b4/';
subject      = 'b4';
TrainFiles   = [1 2 3];
FilesId      = {[4]};
basename     = [rootpath subfolder];
clf_smr_name = 'b4_rhbf_20150311.mat';
clf_smr      = load([basename clf_smr_name]);
clf_inc_name = ['./' subject '_csp_classifier.mat'];
clf_inc      = load(clf_inc_name);

EVTSTART= 1;
EVTFIX  = 786;
EVTCUES = [769 770 771];
EVTFEED = 781;
EVTENDS = [897 898];

BufferSize  = 512;
WinShift    = 32;  % Samples
FreqGrid    = 0:2:256;
NumFreqs    = length(FreqGrid);
Fs          = 512;
BCIrej      = 0.55;
BCIexp      = 0.98;
NumCSP      = 4;
DeliverTh   = 0.7;

CLS_INC  = 1;
CLS_CTR  = 2;

%% Import files
% GDF files
disp('[io] - Importing gdf files');
GDFfiles = util_getfiles({basename}, '.gdf', 'online', FilesId);
[s, h, info] = util_cat_gdf(GDFfiles(:));

NumSamples  = size(s, 1);
NumChannels = size(s, 2);
NumFiles    = length(GDFfiles);

% PSD files
disp('[io] - Importing pds files');
PSDfiles = util_getfiles({basename}, '.dat', 'online', FilesId);
psd = [];
for fId = 1:length(PSDfiles)
    cpsd      = wc_load(PSDfiles{fId}, 'eigen', 'double');
    csamples  = size(cpsd, 1)/NumFreqs;
    cchannels = size(cpsd, 2);
    cpsd      = wc_2dTo3d(cpsd, [NumFreqs cchannels csamples]);
    psd       = cat(1, psd, permute(cpsd, [3 1 2]));
end
psd = log(psd);
NumFrames = size(psd, 1);

%% Computing bci - CHECK
% disp('[check] - Running CNBI simloop');
% bci = cnbi_smr_simloop(GDFfiles{1}, [], [basename clf_smr_name], BCIrej, BCIexp);

%% PSD Features conversion
disp('[proc] - Converting imported features');
features         = proc_reshape_ts_bc(psd);
[freqId, chanId] = fcnbi_feature_conversion(clf_smr.analysis.tools.features, FreqGrid);
featId           = fcnbi_feature_sub2ind([NumFreqs NumChannels], freqId, chanId);

%% SMR GDF Events extraction
disp('[proc] - Extracting event for SMR BCI');
evttyp      = h.typ;
evtpos      = ceil(h.pos/WinShift) + 1;

events = zeros(NumFrames, 1);
events(evtpos) = evttyp;

%% SMR-INC simulated loop
disp('[bci] - Simulating SMR-INC BCI loop');
rawpp = 0.5*ones(NumFrames, 2);
rejpp = 0.5*ones(NumFrames, 2);
intpp = inf(NumFrames, 2);
csppp = 0.5*ones(NumFrames, 2);

p_intpp = [0.5 0.5];
deliverprob = [];
StartWin = 1:WinShift:NumSamples;
StopWin  = StartWin + BufferSize - 1;
StopWin(StopWin > NumSamples) = NumSamples;

for frId = 1:NumFrames
    
    cfeatures = features(frId, featId);
    csig = mean(s(StartWin(frId):StopWin(frId), :), 1);
    
    [sCSP, pCSP] = filt_csp_apply(csig, clf_inc.analysis.CSP, clf_inc.analysis.NumCSP);
    cpp_csp = qda_test(clf_inc.analysis.qdastr, sCSP(:, clf_inc.analysis.FeatId(1:clf_inc.analysis.NumFeatures)));
    
    [~, crawpp] = gauClassifier(clf_smr.analysis.tools.net.gau.M, ... 
                                     clf_smr.analysis.tools.net.gau.C, ...
                                     cfeatures);
    crejpp = crawpp;                         
    if max(crawpp) < BCIrej
        crejpp = p_intpp;
    end
    
    cintpp = eegc3_expsmooth(p_intpp, crejpp, BCIexp);
    
    rawpp(frId, :) = crawpp; 
    rejpp(frId, :) = crejpp; 
    intpp(frId, :) = cintpp;
    csppp(frId, :) = cpp_csp;
    
    %if (events(frId) == 781) || (events(frId) == 898) || (events(frId) == 897)
    if (max(intpp(frId, :)) >= DeliverTh)
        cintpp = [0.5 0.5];
    end
    
%     if (events(frId) == 897 || events(frId) == 898)
%         deliverprob = cat(1, deliverprob, cintpp);
%     end

    p_intpp = cintpp;
end