clear all; clc;

rootpath = '/mnt/data/Research/smr/20150311_b4/';
subject  = 'b4';
FilesId = {'All'};

EVTSTART= 1;
EVTFIX  = 786;
EVTCUES = [769 770 771];
EVTFEED = 781;
EVTENDS = [897 898];

WinShift = 32;  % Samples
FreqGrid = 0:2:256;
NumFreqs = length(FreqGrid);
Fs       = 512;
CLS_INC  = 1;
CLS_CTR  = 2;


PSDfiles = util_getfiles({rootpath}, '.dat', 'online', FilesId);
GDFfiles = util_getfiles({rootpath}, '.gdf', 'online', FilesId);

psd = [];
for fId = 1:length(PSDfiles)
    cpsd = wc_load(PSDfiles{fId}, 'eigen', 'double');
    csamples = size(cpsd, 1)/NumFreqs;
    cchannels = size(cpsd, 2);
    cpsd = wc_2dTo3d(cpsd, [NumFreqs cchannels csamples]);
    psd = cat(1, psd, permute(cpsd, [3 1 2]));
end
%psd = log(psd);
[~, h] = util_cat_gdf(GDFfiles(:));

NumSamples  = size(psd, 1);
NumChannels = size(psd, 3);
NumTrials   = sum(h.typ == 1);

%%%% Labeling
[LblInc, PosInc] = util_getlabels(h.typ, floor(h.pos/WinShift), EVTFIX, EVTFEED, NumSamples);
[LblEng, PosEng] = util_getlabels(h.typ, floor(h.pos/WinShift), EVTFEED, EVTENDS, NumSamples);
[LblTrl, PosTrl] = util_getlabels(h.typ, floor(h.pos/WinShift), EVTFIX, EVTENDS, NumSamples);

%%%% Windowing
WinPeriod = 0.5;
WinLength = floor(WinPeriod*Fs/WinShift);
WinStart  = 1:(NumSamples - WinLength);
WinStop   = WinStart + WinLength - 1;
NumWins   = length(WinStart);

%%%% Frequencies selection
SubFreqs    = 4:2:48;
[~, SubFreqIds] = intersect(FreqGrid, SubFreqs);
NumSubFreqs = length(SubFreqIds);

SubPSD = squeeze(mean(psd(:, SubFreqIds, :), 2));

%%%% Time Correlation
tcorr = zeros(NumWins, NumChannels);
for wId = 2:NumWins
    for chId = 1:NumChannels
            ppsd = SubPSD(WinStart(wId-1):WinStop(wId-1), chId);
            cpsd = SubPSD(WinStart(wId):WinStop(wId), chId);
            tcorr(wId, chId) = corr(ppsd, cpsd);
    end    
end

%%%% Channel Correlation

ccorr = corrcoef(SubPSD');

    
%%%% Trial reshape
MinTrialLen = min(diff(PosTrl, [], 2));
CorrTime = zeros(MinTrialLen, NumChannels, NumTrials);
CorrChan = zeros(MinTrialLen, NumTrials);
for trId = 1:NumTrials
    cstart = PosTrl(trId, 1);
    cstop  = cstart + MinTrialLen -1; 
    CorrTime(:, :, trId) = tcorr(cstart:cstop, :);
    CorrChan(:, trId) = ccorr(cstart:cstop, end);
end


