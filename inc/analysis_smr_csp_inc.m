clear all; clc;

rootpath   = '/mnt/data/Research/smr/';
subfolder  = '20150311_b4/';
%subfolder  = '20150309_a1/';
%subfolder  = '20150326_p1/';
%subfolder  = '20150410_p1/';
subject    = 'b4';
%subject    = 'a1';
%subject    = 'p1';
TrainFiles = [1 2 3];

FilesId    = {'All'};
basename  = [rootpath subfolder];
savename = ['./' subject '_csp_classifier.mat'];


EVTSTART= 1;
EVTFIX  = 786;
EVTCUES = [769 770 771];
EVTFEED = 781;
EVTENDS = [897 898];

WinShift = 32;  % Samples
FreqGrid = 0:2:256;
NumFreqs = length(FreqGrid);
Fs       = 512;
CLS_INC  = 1;
CLS_ENG  = 2;
BPFlag   = false;
BPRange  = [6 128];
NumCSP   = 4;



%% Import file for train and test
disp('[io] - Importing gdf files');
GDFfiles     = util_getfiles({basename}, '.gdf', 'online', FilesId);
[s, h, info] = util_cat_gdf(GDFfiles(:));

NumSamples  = size(s, 1);
NumChannels = size(s, 2);

NumFiles    = length(GDFfiles);

%% Labeling
disp('[proc] - Data Labeling');
[LblInc, PosInc] = util_getlabels(h.typ, h.pos, EVTFIX, EVTFEED, NumSamples);
[LblEng, PosEng] = util_getlabels(h.typ, h.pos, EVTFEED, EVTENDS, NumSamples);
[LblTrl, PosTrl] = util_getlabels(h.typ, h.pos, EVTFIX, EVTENDS, NumSamples);

LblFiles = zeros(NumSamples, 1);
for i = 1:size(info.datasize, 2)
    cstart = info.datasize(1, i);
    cstop  = info.datasize(2, i);
    LblFiles(cstart:cstop) = i;
end

LblCls = zeros(NumSamples, 1);
LblCls(LblEng) = CLS_ENG;
LblCls(LblInc) = CLS_INC;

LblTrain = false(NumSamples, 1);
for fId = 1:length(TrainFiles)
    LblTrain = LblTrain | LblFiles == TrainFiles(fId);
end

IdTrialTrain  = LblTrain(PosTrl(:, 1));
NumTrialTrain = sum(IdTrialTrain);
NumTrialTest  = sum(~IdTrialTrain);
NumTrials     = size(PosTrl, 1);

%% Filtering
if BPFlag
    disp('[proc] - Data filtering');
    s = filt_bp(s, 4, BPRange, Fs);
end


%% CSP
disp('[proc] - Computing CSP filters');
% Computing CSP coefficient
CSP = filt_csp_dec(s(LblCls > 0 & LblTrain, :), LblCls(LblCls > 0 & LblTrain));

disp('[proc] - Applying CSP filters');
% Applying CSP
[sCSP, pCSP] = filt_csp_apply(s, CSP, NumCSP);


%% Trial reshape
disp('[proc] - Trial reshape');
MinTrialLen = min(diff(PosTrl, [], 2));
MinFeedLen  = min(diff(PosEng, [], 2));
IncLen = MinTrialLen - MinFeedLen;

sCSP_r = zeros(MinTrialLen, NumCSP, NumTrials);
for trId = 1:NumTrials
    cstart = PosTrl(trId, 1);
    cstop  = cstart + MinTrialLen - 1;
    sCSP_r(:, :, trId) = sCSP(cstart:cstop, :);
end

%% Discriminancy
disp('[proc] - Computing discriminancy on CSP patterns');
dp = eegc3_cva2(sCSP(LblCls > 0 & LblCls == CLS_ENG & LblTrain, :), sCSP(LblCls > 0 & LblCls == CLS_INC & LblTrain, :));

NumFeatures = 2;
[~, FeatId] = sort(dp, 'descend'); 

%% Classifier Training
disp('[proc] - Training classifier');
qdastr = qda_train(sCSP(LblCls > 0 & LblTrain, FeatId(1:NumFeatures)), LblCls(LblCls > 0 & LblTrain));

%% Classifier Testing
disp('[proc] - Testing classifier');
pp = qda_test(qdastr, sCSP(:, FeatId(1:NumFeatures)));

%% Saving classifier
disp('[proc] - Saving classifier at:');
analysis.qdastr = qdastr;
analysis.CSP    = CSP;
analysis.NumCSP = NumCSP;
analysis.NumFeatures = NumFeatures;
analysis.FeatId = FeatId;

save(savename, 'analysis');

%% Accuracy
disp('[proc] - Computing accuracy');
Th = 0.5;
AccIncTrain = 100*sum(pp(LblInc & LblTrain, CLS_INC) > Th)./sum(LblInc & LblTrain);
AccEngTrain = 100*sum(pp(LblEng & LblTrain, CLS_ENG) > Th)./sum(LblEng & LblTrain);
AccIncTest  = 100*sum(pp(LblInc & ~LblTrain, CLS_INC) > Th)./sum(LblInc & ~LblTrain);
AccEngTest  = 100*sum(pp(LblEng & ~LblTrain, CLS_ENG) > Th)./sum(LblEng & ~LblTrain);

ThBlock  = 0.5;
BlockSigTrain = pp(:, CLS_INC) > ThBlock & LblTrain;
CorrectBlockedTrain = 100*sum(BlockSigTrain & (~LblEng & LblTrl & LblTrain))./sum((~LblEng & LblTrl & LblTrain));
CorrectEnabledTrain = 100*sum((~BlockSigTrain) & LblEng & LblTrain)./sum(LblEng & LblTrain);
DeliverBlockedTrain = 100*sum(BlockSigTrain(PosTrl(IdTrialTrain, 2)))./size(PosTrl(IdTrialTrain, :), 1);

BlockSigTest = pp(:, CLS_INC) > ThBlock & ~LblTrain;
CorrectBlockedTest = 100*sum(BlockSigTest & (~LblEng & LblTrl & ~LblTrain))./sum((~LblEng & LblTrl & ~LblTrain));
CorrectEnabledTest = 100*sum((~BlockSigTest) & LblEng & ~LblTrain)./sum(LblEng & ~LblTrain);
DeliverBlockedTest = 100*sum(BlockSigTest(PosTrl(~IdTrialTrain, 2)))./size(PosTrl(~IdTrialTrain, :), 1);

%% Plotting 
disp('[proc] - Plotting');
figure;
fig_set_position(gcf, 'All');
NumCols = 2;
NumRows = ceil(NumCSP/NumCols) + 2;
CSPNames = cell(NumCSP, 1);
CSPAbbr  = cell(NumCSP, 1);
for pId = 1:NumCSP
    CSPNames{pId} = ['CSP pattern ' num2str(pId)];
    CSPAbbr{pId}  = ['CSP' num2str(pId)];
end

% Signals
subplot(NumRows, NumCols, 1:NumCols);
t = -IncLen/Fs:1/Fs:(MinFeedLen/Fs - 1/Fs);
plot_boundedline(t, mean(sCSP_r(:, :, IdTrialTrain), 3), permute(std(sCSP_r(:, :, IdTrialTrain), [], 3), [1 3 2]), 'alpha');
plot_vline(0, 'k');
xlim([t(1) t(end)]);
grid on;
legend(CSPNames);
title('Mean and standard deviation for CSP patterns for TrainSet');
xlabel('Trial period [s]');
ylabel('[uV]');

subplot(NumRows, NumCols, (NumCols+1):2*NumCols);
t = -IncLen/Fs:1/Fs:(MinFeedLen/Fs - 1/Fs);
plot_boundedline(t, mean(sCSP_r(:, :, ~IdTrialTrain), 3), permute(std(sCSP_r(:, :, ~IdTrialTrain), [], 3), [1 3 2]), 'alpha');
plot_vline(0, 'k');
xlim([t(1) t(end)]);
grid on;
legend(CSPNames);
title('Mean and standard deviation for CSP patterns for TestSet');
xlabel('Trial period [s]');
ylabel('[uV]');

% CSP patterns
load('./chanlocs16');
MaxCSP = max(max(abs(pCSP)));
for pId = 1:NumCSP
    subplot(NumRows, NumCols, pId + 2*NumCols);
    
    topoplot(pCSP(:, pId), chanlocs16, 'intrad', 0.4, 'headrad', 0.4);
    axis image;
    title(CSPNames{pId});
    plot_setCLim(gca, [-MaxCSP MaxCSP]);
end

figure;
fig_set_position(gcf, 'Top');
% Discriminant power
subplot(1, 2, 1);
hold on;
bar(dp);
hdl = plot(FeatId(1:NumFeatures), max(dp) + 3, 'ro', 'MarkerSize', 9, 'MarkerFaceColor', 'r');
hold off;
plot_setXTick(gca, CSPAbbr, 1:NumCSP);
grid on;
xlabel('CSP patterns');
ylabel('dp');
title('Discriminant power for CSP patterns on TrainSet');
legend(hdl, 'Selected feature');



% Blocking signal
subplot(1, 2, 2);
bar([CorrectBlockedTrain CorrectEnabledTrain DeliverBlockedTrain; CorrectBlockedTest CorrectEnabledTest DeliverBlockedTest]');
plot_setXTick(gca, {'Correct Blocked', 'Correct Enabled', 'Delivery Blocked'}, 1:3);
grid on;
ylabel('%');
title('Percentage of accuracy for blocking signal');
legend('TrainSet', 'TestSet');



% %% Training - Classifier Training
% qdastr = qda_train(sigCSP, lblW(lblW > 0));
% 
% 
% %% Testing - Labeling
% NumSamples  = size(Stest, 1);
% NumChannels = size(Stest, 2);
% 
% [Labels, Events] = util_getdata_labels(Ktest.typ, Ktest.pos, Ktest.dur, NumSamples);
% 
% % INC Labels
% LblINC = false(NumSamples, 1);
% id = find(Events == EVTFIX | Events == EVTCUE(1) | Events == EVTCUE(2) | Events == EVTCUE(3));
% for i = 1:length(id)
%     LblINC = LblINC | Labels(:, id(i));
% end
% 
% % ENG Labels
% LblENG = false(NumSamples, 1);
% id = find(Events == EVTFEED);
% for i = 1:length(id)
%     LblENG = LblENG | Labels(:, id(i));
% end
% 
% % Total Lbl vector
% Lbl = LblINC*CLS_INC + LblENG*CLS_ENG;
% Lbl(Lbl == (CLS_INC + CLS_ENG)) = max(CLS_INC, CLS_ENG);
% 
% 
% %% Testing - Processing
% 
% % Laplacian
% if lapflag
%     Stest = Stest * lapmask;
% end
% 
% % Windowing
% WinStart = 1:WinShift:(NumSamples - WinLength);
% WinStop  = WinStart + WinLength - 1;
% NumWins  = length(WinStart);
% sigW     = zeros(NumWins, NumChannels);
% lblW     = zeros(NumWins, 1);
% 
% for wId = 1:NumWins
%     sigW(wId, :) = mean(Stest(WinStart(wId):WinStop(wId), :), 1);
%     lblW(wId)    = Lbl(WinStart(wId));
% end
% 
% %% Testing - CSP
% 
% % Applying CSP
% sigCSP = filt_csp_apply(sigW(lblW > 0, :)', cCSP, NumCSP)';
% 
% %% Testing - Classifier Testing
% th = 0;
% pp = qda_test(qdastr, sigCSP);
% cl = min(CLS_INC, CLS_ENG)*ones(size(pp, 1), 1);
% cl(pp(:, 2) > th) = max(CLS_INC, CLS_ENG);
% 
% classvalues = [CLS_INC CLS_ENG];
% classnames  = {'INC', 'Engaged'};
% 
% lb = lblW(lblW > 0);
% for i = 1:size(pp, 2)
%     disp(['Performances class ' classnames{i} ': ' num2str(sum((lb == classvalues(i) & cl == classvalues(i))/sum(cl == classvalues(i))))]);
% end
% 
% %% Plotting
% hold on; plot(lblW(lblW > 0)); plot(pp(:, 2)+1, 'r.'); plot(cl, 'g.'); hold off;

