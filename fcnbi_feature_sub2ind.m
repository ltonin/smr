function ind = fcnbi_feature_sub2ind(siz, bSubs, cSubs)
% ind = fcnbi_feature_sub2ind(siz, bSubs, cSubs)
%
% Given two vectors of the same length with the subscripts for bands and
% channels, it returns the index of the features according to the siz
% argument.

    if (length(cSubs) ~= length(bSubs))
        error('chk:in', 'Channels and frequency subscripts must have the length');
    end
    
    
    NumFeatures = length(cSubs);
    ind = zeros(NumFeatures, 1);
    
    for fId = 1:NumFeatures
        ind(fId) = sub2ind(siz, bSubs(fId), cSubs(fId));
    end

end