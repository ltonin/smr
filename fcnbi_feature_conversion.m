function [bnd, chn, bndl] = fcnbi_feature_conversion(featstr, bndgrid)
% [bnd, chn, bndl] = fcnbi_feature_conversion(featstr, bndgrid)
%
% It converts the features subscripts from the cnbi gaussian classifier
% format (featstr is the structure in analysis.tools.features) in three
% vectors of the same length with respectively:
% - bnd  -> indexes of the frequencies according to the frequency grid provided
% - chn  -> indexes of the channels
% - bndl -> labels of the frequencies (the same provided by the cnbi gaussian classifier, but in vector format)
    

    chans = featstr.channels;
    bands = featstr.bands;
    
    
    bndl = [];
    chn = [];
    for chId = 1:length(chans)
        cchn = chans(chId);
        cbndl = bands{cchn}';
        nbnd = length(cbndl);
        bndl = cat(1, bndl, cbndl);
        chn = cat(1, chn, cchn*ones(nbnd, 1));
    end
    
    bnd = zeros(length(bndl), 1);
    for fId = 1:length(bndl)
        bnd(fId) = find(bndl(fId) == bndgrid);
    end
    

    
end
