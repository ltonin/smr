clc;
clear all;
close all;

subject  = 'b4';
pattern  = [subject '*mi_rlsf*'];
rootpath = '/mnt/data/Research/smr/';
% pattern  = 'offline';
% rootpath = '/mnt/data/Research/smr-inc/';

dirpath   = util_getdir(rootpath, subject);
filenames = util_getfile(dirpath, '.gdf', pattern);
NumFiles = length(filenames);

load('lapmask_16ch');

s = [];
pos = [];
typ = [];
dur = [];
for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    [cs, ch] = sload(cfilename);
    
    cs = cs(:, 1:end-1);
    
    disp( '[proc] - Filtering');
    
    % Band-pass
    csf = filt_bp(cs, 4, [8 12], ch.SampleRate);
    
    % Laplacian
    csl = csf*lapmask;
    
    
    pos = cat(1, pos, ch.EVENT.POS + size(s, 1));
    typ = cat(1, typ, ch.EVENT.TYP);
    dur = cat(1, dur, ch.EVENT.DUR);
    s = cat(1, s, csl);
    
    NumSamples  = size(s, 1);
    NumChannels = size(s, 2);
    SampleRate  = ch.SampleRate;
end

h.pos = pos;
h.typ = typ;
h.dur = dur;

TrialPeriod  = [0.5 3];

[Ck, Tk] = smr_data_label(NumSamples, h, floor(TrialPeriod*SampleRate));

StartTrials = find(diff((Ck > 0)) == 1);
StopTrials  = find(diff((Ck > 0)) == -1) -1;
TrialLenght = StopTrials(1) - StartTrials(1) + 1;

NumTrials   = length(StartTrials);

D = zeros(TrialLenght, NumChannels, NumTrials);

for trId = 1:NumTrials
    cstart = StartTrials(trId);
    cstop  = StopTrials(trId);
    D(:, :, trId)  = s(cstart:cstop, :);  
end

EntWinMin  = floor(0.50*SampleRate);
EntWinStep = 32;%floor(0.25*SampleRate);
EntWinStart = EntWinMin:EntWinStep:TrialLenght-EntWinMin;
EntWinStop  = EntWinStart + EntWinMin;
%EntWinStop  = EntWinMin:EntWinStep:TrialLenght - EntWinStep;

NumEntWin = length(EntWinStop);

E = zeros(NumEntWin, NumChannels, NumTrials);
for trId = 1:NumTrials
    for eWId = 1:NumEntWin
        cstart = EntWinStart(eWId);
        cstop = EntWinStop(eWId);
        cdata = D(cstart:cstop, :, trId);
        E(eWId, :, trId) = proc_entropy(cdata, 16);
    end
end

%TrialId(:, 1) = Tk == 769 | Tk == 770 | Tk == 771;
TrialId(:, 1) = Tk == 771 | Tk == 773;
TrialId(:, 2) = Tk == 783;

ChanLayout = [0 0 1 0 0; 2 3 4 5 6; 7 8 9 10 11; 12 13 14 15 16];
ChanLabels = {'Fz', 'Fc3', 'Fc1', 'Fcz', 'Fc2', 'Fc4', 'C3', 'C1', 'Cz', 'C2', 'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};
color = {'b', 'r'};

h = zeros(NumEntWin, NumChannels);
p = zeros(NumEntWin, NumChannels);

%t = TrialPeriod(1):EntWinStep/SampleRate:TrialPeriod(2) - EntWinStep/SampleRate;
t = 1:size(E, 1);

% avg_data = zeros(size(E, 1), size(E, 2), 2);
% 
% avg_data(:, :, 1) = mean(E(:, :, TrialId(:, 1)), 3);
% avg_data(:, :, 2) = mean(E(:, :, TrialId(:, 2)), 3);
% 
% plot_subplot(t, avg_data, ...
%                     'layout', ChanLayout, 'subtitles', ChanLabels, ...
%                     'xlabel', 't [s]', 'ylabel', 'Entropy');
% 

ht = zeros(NumChannels);
hl = zeros(2, NumChannels);
hp = zeros(2, NumChannels);


f = figure;
set(gcf,'name', 'Entropy of alpha band with increasing window','numbertitle','off');
fig_set_position(f, 'All');


NumRows = 4;
NumCols = 5;
for chId = 1:NumChannels
    [cx, cy] = find(ChanLayout == chId); 
    index = (cx-1)*NumCols + cy;
    subplot(NumRows, NumCols, index);
    
    cdata = squeeze(E(:, chId, :));
    
    for eWId = 1:NumEntWin
        [h(eWId, chId), p(eWId, chId)] = ttest2(cdata(eWId, TrialId(:, 1)), cdata(eWId, TrialId(:, 2)), 'alpha', 0.01);
    end
    
    
    
    
    hold on;
    [ht(chId)] = plot(t, h(:, chId)*3.5, 'k');
    for clId = 1:size(TrialId, 2)
        cindex = TrialId(:, clId);
        cmean = mean(E(:, chId, cindex), 3);
        cstd  = std(E(:, chId, cindex), [], 3);
        [hl(clId, chId), hp(clId, chId)] = plot_boundedline(t, cmean, cstd, color{clId}, 'alpha');
        %plot(cdata, color{clId});
    end
    hold off;
    grid on;
    title(ChanLabels{chId});
    %ylim([2.5 3.5]);
    %xlim([TrialPeriod(1) TrialPeriod(2)]);
    xlabel('Time [s]');
    ylabel('Amplitude [uV]');
    
    if chId == NumChannels
        legend([hl(1, chId) hl(2, chId) ht(chId)], {'SMR', 'Rest', 'p<0.01'});
    end
end
    




