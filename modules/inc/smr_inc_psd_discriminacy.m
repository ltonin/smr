clc;
clear all;
close all;

subject  = 'b4';
pattern  = [subject '*mi_rlsf*'];
rootpath = [pwd '/analysis/psd/'];

filenames = util_getfile(rootpath, '.mat', pattern);
NumFiles = length(filenames);

EVENT_CUE_ID = [769 770 771 783];
EVENT_CUE_LB = {'left-hand', 'right-hand', 'both-feet', 'rest'};
NumCues = length(EVENT_CUE_ID);

D   = [];
Rk  = [];
typ = [];
pos = [];
dur = [];

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading psd data ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    cdata = load(cfilename);
    ctyp  = cdata.analysis.event.typ;
    cpos  = cdata.analysis.event.pos;
    cdur  = cdata.analysis.event.dur;
    
    typ = cat(1, typ, ctyp);
    dur = cat(1, dur, cdur);
    pos = cat(1, pos, cpos + size(D, 1));
    Rk  = cat(1, Rk, fId*ones(size(cdata.psd, 1), 1));
    
    D = cat(1, D, cdata.psd);
    
   
    SampleRate  = cdata.analysis.buffer.SampleRate;
    Freqs       = cdata.analysis.proc.freqs;
    WinLength   = floor(cdata.analysis.buffer.WinPeriod*SampleRate);
    WinStep     = floor(cdata.analysis.buffer.WinOverlap*SampleRate);
end
hdr.typ = typ;
hdr.pos = pos;
hdr.dur = dur;

FreqGrid = 4:2:48;
[~, FreqId] = intersect(Freqs, FreqGrid); 

U = D(:, FreqId, :);

NumSamples  = size(U, 1);
NumFreqs    = size(U, 2);
NumChannels = size(U, 3);

[Ck, Tk] = smr_data_label(NumSamples, hdr);

StartTrials = find(diff((Ck > 0)) == 1);
StopTrials  = find(diff((Ck > 0)) == -1) -1;
NumTrials   = length(StartTrials);
TrialLenght = StopTrials(1) - StartTrials(1) + 1;

P = zeros(TrialLenght, NumFreqs, NumChannels, NumTrials);
for trId = 1:NumTrials
    P(:, :, :, trId) = U(StartTrials(trId):StopTrials(trId), :, :);
end


%% Accumulation and normalization framework
U = log(1+P);
alpha = 0.5;
A = zeros(size(U));
N = zeros(size(U));
for trId = 1:NumTrials
   cdata = U(:, :, :, trId);
   bdata = repmat(mean(cdata, 3), [1 1 NumChannels]);
   ndata = cdata./bdata;
   
   adata = ndata;
   for wId = 2:size(U, 1)
       adata(wId, :, :) = alpha*ndata(wId, :, :) + (1 - alpha)*adata(wId-1, :, :);
   end
   %adata = cumsum(ndata, 1);
   A(:, :, :, trId) = adata;
   N(:, :, :, trId) = ndata;
end


%% Discriminant power
[Ur, Ukr] = proc_reshape_ts_bc(U, Tk);
[Nr, Nkr] = proc_reshape_ts_bc(N, Tk);
[Ar, Akr] = proc_reshape_ts_bc(A, Tk);


Idx1 = Ukr == 769 | Ukr == 770 | Ukr == 771;
Idx2 = Ukr == 783;

Labels = zeros(length(Ukr), 1);
Labels(Idx1) = 1;
Labels(Idx2) = 2;

cindex = Labels > 0;

ClassName{1} = 'All Motor Imagery';
ClassName{2} = 'Rest';

[Udp, ~, ~, ~, Udisc] = cva_tun_opt(Ur(cindex, :), Labels(cindex));
[Ndp, ~, ~, ~, Ndisc] = cva_tun_opt(Nr(cindex, :), Labels(cindex));
[Adp, ~, ~, ~, Adisc] = cva_tun_opt(Ar(cindex, :), Labels(cindex));

subplot(2, 3, 1);
imagesc(reshape(Udp, [NumFreqs NumChannels])');
set(gca, 'CLim', [0 5]);
title('Standard PSD');

subplot(2, 3, 2);
imagesc(reshape(Ndp, [NumFreqs NumChannels])');
set(gca, 'CLim', [0 5]);
title('Normalized PSD');

subplot(2, 3, 3);
imagesc(reshape(Adp, [NumFreqs NumChannels])');
set(gca, 'CLim', [0 5]);
title('Accumulated PSD');

colors = {'b', 'r'};

subplot(2, 3, 4);
for cId = 1:length(colors)
    p = gkdeb(Udisc(Labels(cindex) == cId));
    hold on;
    plot(p.x, p.f, colors{cId});
    hold off;
end
legend(ClassName);
grid on;

subplot(2, 3, 5);
for cId = 1:length(colors)
    p = gkdeb(Ndisc(Labels(cindex) == cId));
    hold on;
    plot(p.x, p.f, colors{cId});
    hold off;
end
legend(ClassName);
grid on;

subplot(2, 3, 6);
for cId = 1:length(colors)
    p = gkdeb(Adisc(Labels(cindex) == cId));
    hold on;
    plot(p.x, p.f, colors{cId});
    hold off;
end
legend(ClassName);
grid on;
