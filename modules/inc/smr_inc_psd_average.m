clc;
clear all;
close all;

subject  = 'b4';
pattern  = [subject '*mi_rlsf*'];
rootpath = [pwd '/analysis/psd/'];

filenames = util_getfile(rootpath, '.mat', pattern);
NumFiles = length(filenames);

EVENT_CUE_ID = [769 770 771 783];
EVENT_CUE_LB = {'left-hand', 'right-hand', 'both-feet', 'rest'};
NumCues = length(EVENT_CUE_ID);


D   = [];
Rk  = [];
typ = [];
pos = [];
dur = [];

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading psd data ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    cdata = load(cfilename);
    ctyp  = cdata.analysis.event.typ;
    cpos  = cdata.analysis.event.pos;
    cdur  = cdata.analysis.event.dur;
    
    typ = cat(1, typ, ctyp);
    dur = cat(1, dur, cdur);
    pos = cat(1, pos, cpos + size(D, 1));
    Rk  = cat(1, Rk, fId*ones(size(cdata.psd, 1), 1));
    
    D = cat(1, D, cdata.psd);
    
   
    SampleRate  = cdata.analysis.buffer.SampleRate;
    Freqs       = cdata.analysis.proc.freqs;
    WinLength   = floor(cdata.analysis.buffer.WinPeriod*SampleRate);
    WinStep     = floor(cdata.analysis.buffer.WinOverlap*SampleRate);
end
hdr.typ = typ;
hdr.pos = pos;
hdr.dur = dur;

FreqGrid = 4:2:48;
[~, FreqId] = intersect(Freqs, FreqGrid); 

U = D(:, FreqId, :);

NumSamples  = size(U, 1);
NumFreqs    = size(U, 2);
NumChannels = size(U, 3);

AnalysisPeriod = floor([-1 3.5]*SampleRate/WinStep);
[Ck, Tk] = smr_data_label(NumSamples, hdr, AnalysisPeriod);

StartTrials = find(diff((Ck > 0)) == 1);
StopTrials  = find(diff((Ck > 0)) == -1) -1;
NumTrials   = length(StartTrials);
TrialLenght = StopTrials(1) - StartTrials(1) + 1;

P = zeros(TrialLenght, NumFreqs, NumChannels, NumTrials);
for trId = 1:NumTrials
    P(:, :, :, trId) = U(StartTrials(trId):StopTrials(trId), :, :);
end





%% Accumulation and normalization framework
Pl = log(1+P);
alpha = 0.5;
A = zeros(size(Pl));
N = zeros(size(Pl));
for trId = 1:NumTrials
   cdata = Pl(:, :, :, trId);
   bdata = repmat(mean(cdata, 3), [1 1 NumChannels]);
   ndata = cdata./bdata;
   
   adata = ndata;
   for wId = 2:size(Pl, 1)
       adata(wId, :, :) = alpha*ndata(wId, :, :) + (1 - alpha)*adata(wId-1, :, :);
   end
   adata = cumsum(ndata, 1);
   A(:, :, :, trId) = adata;
   N(:, :, :, trId) = ndata;
end


%% Plotting accumulation data

ChanLayout = [0 0 1 0 0; 2 3 4 5 6; 7 8 9 10 11; 12 13 14 15 16];
ChanLabels = {'Fz', 'Fc3', 'Fc1', 'Fcz', 'Fc2', 'Fc4', 'C3', 'C1', 'Cz', 'C2', 'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};

t = -1:1/floor(SampleRate/WinStep):3.5 - 1/floor(SampleRate/WinStep);
f = FreqGrid;
v0 = find(t == 0);
Indexes{1} = Tk == 769 | Tk == 770 | Tk == 771;
Indexes{2} = Tk == 783;
ClassName{1} = 'All Motor Imagery';
ClassName{2} = 'Rest';

for cId = 1:length(Indexes)
    cdata_std = mean(Pl(:, :, :, Indexes{cId}), 4);
    cdata_nrm = mean(N(:, :, :, Indexes{cId}), 4);
    cdata_acc = mean(A(:, :, :, Indexes{cId}), 4);
    
    
    plot_subimagesc(t, f, cdata_std, ...
                    'clim', [0 0.5], ...
                    'fhandle', @plot_vline, ...
                    'fargs', {v0, 'k'}, ...
                    'figtitle', [ClassName{cId} ' - Standard PSD'], ...
                    'layout', ChanLayout, 'subtitles', ChanLabels, ...
                    'xlabel', 't [s]', 'ylabel', 'f [Hz]');
    plot_subimagesc(t, f, cdata_nrm, ...
                    'clim', [0 2], ...
                    'fhandle', @plot_vline, ...
                    'fargs', {v0, 'k'}, ...
                    'figtitle', [ClassName{cId} ' - Normalized PSD'], ...
                    'layout', ChanLayout, 'subtitles', ChanLabels, ...
                    'xlabel', 't [s]', 'ylabel', 'f [Hz]');
    plot_subimagesc(t, f, cdata_acc, ...
                    'clim', [0 50], ...
                    'fhandle', @plot_vline, ...
                    'fargs', {v0, 'k'}, ...
                    'figtitle', [ClassName{cId} '- Normalized and Accumulated PSD'], ...
                    'layout', ChanLayout, 'subtitles', ChanLabels, ...
                    'xlabel', 't [s]', 'ylabel', 'f [Hz]');
end

% L = proc_reshape_ts_bc(A(33:end, :, :, :));
% CueTypS = reshape(repmat(CueTyp, [1 56])', [size(A, 4)*56 1]);
% 
% CuePairs(:, 1) = [EVENT_CUE_ID(1) EVENT_CUE_ID(2)];
% CuePairs(:, 2) = [EVENT_CUE_ID(1) EVENT_CUE_ID(3)];
% CuePairs(:, 3) = [EVENT_CUE_ID(1) EVENT_CUE_ID(4)];
% CuePairs(:, 4) = [EVENT_CUE_ID(2) EVENT_CUE_ID(3)];
% CuePairs(:, 5) = [EVENT_CUE_ID(2) EVENT_CUE_ID(4)];
% CuePairs(:, 6) = [EVENT_CUE_ID(3) EVENT_CUE_ID(4)];
% 
% figure;
% for pId = 1:size(CuePairs, 2)
%     
%     cindex = false(length(CueTypS), 1);
%     for cId = 1:size(CuePairs, 1)
%         cindex  = cindex | CueTypS == CuePairs(cId, pId);
%     end
%     
%     
%     [dp, ~, v, ~, disc] = cva_tun_opt(L(cindex, :), CueTypS(cindex));
%     dp_r = reshape(dp, [NumFreqs NumChannels]);
%     subplot(size(CuePairs, 2) + 1, 2, size(CuePairs, 1)*(pId - 1) + [1 2])
%     imagesc(dp_r');
%     set(gca, 'CLim', [0 8]);
%     title([num2str(CuePairs(1, pId)) ' vs. ' num2str(CuePairs(2, pId))]);
%     
% %     subplot(size(CuePairs, 2), size(CuePairs, 1), pId*size(CuePairs, 1))
% %     
% %     ClassColors = {'b', 'r'};
% %     for cId = 1:size(CuePairs, 1)
% %         p = gkdeb(disc(CueTypS == CuePairs(cId, pId)));
% % 
% %         hold on;
% %         plot(p.x, p.f, ClassColors{cId});
% %         hold off;
% %     end
% %     grid on;
% %     legend(num2str(CuePairs(:, pId)'));
% %     xlabel('Canonical Space');
% %     title('Distribution on canonical space');
% end

% cindex = CueTypS == 783;
% LbAllRest(cindex == 0) = 1;
% LbAllRest(cindex == 1) = 2;
% [dp, ~, v, ~, disc] = cva_tun_opt(L, LbAllRest');
% dp_r = reshape(dp, [NumFreqs NumChannels]);
% subplot(size(CuePairs, 2) + 1, 2, (size(CuePairs, 2) + 1)*2 - [1 0])
% imagesc(dp_r');
% set(gca, 'CLim', [0 8]);
% title(['All vs. Rest']);
% 
% for cId = 1:NumCues
%    
%     EventOfInterest = EVENT_CUE_ID(cId);
% 
%     figure;
%     set(gcf,'name',['Class ' EVENT_CUE_LB{cId}],'numbertitle','off')
%     NumRows = 4;
%     NumCols = 5;
%     t = AnalysisStart:1/floor(SampleRate/WinStep):AnalysisStop - 1/floor(SampleRate/WinStep);
% 
%     ChanLayout = [0 0 1 0 0; 2 3 4 5 6; 7 8 9 10 11; 12 13 14 15 16];
%     ChanLabels = {'Fz', 'Fc3', 'Fc1', 'Fcz', 'Fc2', 'Fc4', 'C3', 'C1', 'Cz', 'C2', 'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};
%     for chId = 1:NumChannels
%         [x, y] = find(ChanLayout == chId); 
%         index = (x-1)*NumCols + y;
%         subplot(NumRows, NumCols, index);
% 
%         cdata_acc = mean(A(:, :, chId, CueTyp == EventOfInterest), 4);
% 
%         imagesc(fliplr(cdata_acc)');
% 
%         set(gca, 'CLim', [0 2]);
%         ytick = get(gca, 'YTick');
%         xtick = [1 abs(AnalysisStartS) AnalysisDurS];
% 
%         set(gca, 'YTickLabel', flipud(num2str(FreqGrid(ytick)')));
%         set(gca, 'XTick', xtick);
%         set(gca, 'XTickLabel', [AnalysisStart 0 AnalysisStop]);
%         axis square
%         title(['Channel ' ChanLabels{chId}]);
% 
%         ylabel('f [Hz]');
%         xlabel('t [samples]');
%         grid on;
% 
%     end
%     
%     
%     
%     
% end

