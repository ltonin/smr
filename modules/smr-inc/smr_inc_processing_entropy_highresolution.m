clearvars; clc;

subject  = 'AN14VE'; %'b4';
pattern  = 'rfsb'; %'hfr';
psdpath  = 'analysis/smr-inc/psd/';


filenames = util_getfile(psdpath, '.mat', [subject '*' pattern]);
NumFiles  = length(filenames);

NumBins  = 16;
dolog  = true;

if dolog == true
    savepath = 'analysis/smr-inc/entropy_hr/log/';
else
    savepath = 'analysis/smr-inc/entropy_hr/nolog/';
end

if (exist(savepath, 'dir') ~= 7)
    mkdir(savepath);
end

Bands = 4:2:48;
NumBands = length(Bands);

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading psd data ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
      
    cdata    = load(cfilename);
    analysis = cdata.analysis;

    D = cdata.psd;
    
    NumSamples  = size(D, 1);
    NumFreqs    = size(D, 2);
    NumChannels = size(D, 3);
    
    Freqs       = analysis.processing.f;
    
    %% Vector labels
    event = analysis.event;
    
    % Create continuous feedback vector labels
    [~, CFeedPos] = proc_getevents(event, size(D, 1), 781);
   
    %% Compute Entropy on all data

    NumTrials = length(CFeedPos);
    entropy = zeros(NumSamples, NumBands, NumChannels);
    trId = 0;
    cstart = 1;
    disp('[proc] - Computing entropy for each trial:');
    for sId = 1:NumSamples

        % Check for reset entropy accumulation (Continuous feedback starts)
        if isempty(find(sId == CFeedPos, 1)) == false 
            cstart = sId;
            trId = trId +1;
            util_disp_progress(trId, NumTrials, '        ');
        end   

        [~, bandId] = intersect(Freqs, Bands); 
        for bId = 1:length(bandId)
            cbandId = bandId(bId);
            cpsd = squeeze(D(cstart:sId, cbandId, :));
            if dolog == true
                cpsd = log(cpsd + 1);
            end
            entropy(sId, bId, :) = proc_entropy(cpsd, NumBins);     
        end
    end
    
    analysis.entropy.bins  = NumBins;
    analysis.entropy.bands = Bands;
    analysis.entropy.dolog = dolog;

    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    disp(['[out] - Saving entropy in: ' sfilename]);
    save(sfilename, 'entropy', 'analysis'); 
end