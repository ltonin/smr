clc;
clear all;
close all;

subject  = 'b4';
dataroot = [pwd '/analysis/psd/'];

filenames = util_getfile(dataroot, '.mat', 'mi_hfr');
NumFiles  = length(filenames);
classifpath = '/mnt/data/Dropbox/Research/smr-inc/20160225_b4/classifier_bhbf.dat';
lpppath = '/mnt/data/Dropbox/Research/smr-inc/20160225_b4/20160225182142_smr-inc_debug-proc.txt';

D   = [];
Rk  = [];
Mk  = [];
typ = [];
pos = [];
dur = [];

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading psd data ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    cdata = load(cfilename);
    ctyp  = cdata.analysis.event.typ;
    cpos  = cdata.analysis.event.pos;
    cdur  = cdata.analysis.event.dur;
    
    typ = cat(1, typ, ctyp);
    dur = cat(1, dur, cdur);
    pos = cat(1, pos, cpos + size(D, 1));
    Rk  = cat(1, Rk, fId*ones(size(cdata.psd, 1), 1));
    
    D = cat(1, D, cdata.psd);
    
   
    SampleRate  = cdata.analysis.buffer.SampleRate;
    Freqs       = cdata.analysis.proc.freqs;
end

hdr.typ = typ;
hdr.pos = pos;
hdr.dur = dur;

FreqGrid = 4:2:48;
[~, FreqId] = intersect(Freqs, FreqGrid); 

NumSamples  = size(D, 1);
NumChans    = size(D, 3);
NumFreqs    = length(FreqId);

% Find event == 1000 (processing start)
ids = hdr.pos(hdr.typ == 1000);
ids = 1;
U = log(D(ids:end, FreqId, :));
F = proc_reshape_ts_bc(U);

%% Load online classifier
disp(['[io] - Loading online classifier: ' classifpath]);
[chdr, cM, cC] = wc_load_classifier(classifpath, 'cnbi-gaussian');

model.type = 'gau';
model.map  = [2 2];
model.shared = chdr.shrdcov;
model.traino = 0;
model.NumEp = 5;
model.M = cM;
model.C = cC;
% model.M(1, :, :) = squeeze(cM(1, :, :))';
% model.M(2, :, :) = squeeze(cM(2, :, :))';
% model.C(1, :, :) = squeeze(cC(1, :, :))';
% model.C(2, :, :) = squeeze(cC(2, :, :))';
model.class_labs = chdr.classlbs;

idf = [];
for i = 1:chdr.nfeatures
    idf = cat(1, idf, sub2ind([NumFreqs NumChans], find(FreqGrid == chdr.idfreq(i)), chdr.idchan(i)));
end

%% Testing classifier on all data
[pp, Gk] = classTest(model, F(:, idf));


%% Load pp from online processing loop
 


