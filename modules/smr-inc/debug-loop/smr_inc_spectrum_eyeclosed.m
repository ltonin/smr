clc;
clear all;
close all;

subject  = 'b9';
pattern  = 'eyeclosed';
rootpath = '/mnt/data/Research/smr-inc/20160308_b9/';
savepath = [pwd '/analysis/psd/'];
filenames = util_getfile(savepath, '.mat', pattern);
NumFiles  = length(filenames);

D   = [];
Rk  = [];
Mk  = [];
typ = [];
pos = [];
dur = [];

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading psd data ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    cdata = load(cfilename);
    ctyp  = cdata.analysis.event.typ;
    cpos  = cdata.analysis.event.pos;
    cdur  = cdata.analysis.event.dur;
    
    typ = cat(1, typ, ctyp);
    dur = cat(1, dur, cdur);
    pos = cat(1, pos, cpos + size(D, 1));
    Rk  = cat(1, Rk, fId*ones(size(cdata.psd, 1), 1));
    
    D = cat(1, D, cdata.psd);
    
   
    SampleRate  = cdata.analysis.buffer.SampleRate;
    Freqs       = cdata.analysis.proc.freqs;
    FrameSize   = cdata.analysis.buffer.FrameSize;
end

NumChannels = size(D, 3);

ChanLayout = [0 0 1 0 0; 2 3 4 5 6; 7 8 9 10 11; 12 13 14 15 16];
ChanLabels = {'Fz', 'Fc3', 'Fc1', 'Fcz', 'Fc2', 'Fc4', 'C3', 'C1', 'Cz', 'C2', 'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};

color = {'b', 'r', 'g'};
typlb = {'WS - Lab', 'LP - CAD', 'WS - CAD'};
NumRows = 4;
NumCols = 5;
for chId = 1:NumChannels
    [cx, cy] = find(ChanLayout == chId); 
    index = (cx-1)*NumCols + cy;
    subplot(NumRows, NumCols, index);
    
    hold on;
    for typ = 1:3
        cindex = Rk == typ;
        
        cdata = squeeze(log(D(cindex, 1:50, chId)));
        plot(Freqs(1:50), cdata, color{typ});
        
    end
    
    hold off;
    grid on;
    title(ChanLabels{chId});
    xlabel('Frequency');
    ylabel('dB');
    
    if(chId == NumChannels)
        legend(typlb);
    end
end