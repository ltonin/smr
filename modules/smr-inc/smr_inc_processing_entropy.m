clearvars; clc;

%%%%%%%%%%%%%%%%%%
% subject  = 'AN14VE';
% pattern  = 'rfsb';
%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%
% subject  = 'b4'; 
% pattern  = 'hfr';
%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%
subject  = 'a1'; 
pattern  = 'hfr';
%%%%%%%%%%%%%%%%%%

psdpath  = 'analysis/smr-inc/psd/csp/';

filenames = util_getfile(psdpath, '.mat', [subject '*' pattern]);
NumFiles  = length(filenames);

NumBins     = 32;
SelFreqs    = 4:2:48;

dolog  = false;

if dolog == true
    savepath = 'analysis/smr-inc/entropy/csp/log/';
else
    savepath = 'analysis/smr-inc/entropy/csp/nolog/';
end

if (exist(savepath, 'dir') ~= 7)
    mkdir(savepath);
end

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading psd data ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
      
    cdata    = load(cfilename);
    analysis = cdata.analysis;

    D = cdata.psd;
    
    NumWindows  = size(D, 1);
    NumFreqs    = size(D, 2);
    NumChannels = size(D, 3);
    
    Freqs          = analysis.processing.f;
    [~, SelFreqId] = intersect(Freqs, SelFreqs); 
    NumSelFreqs    = length(SelFreqId);
    
    %% Vector labels
    event = analysis.event;
    
    % Create continuous feedback vector labels
    [Ck, CPos, CDur] = proc_getevents(event, NumWindows, 781);
   
    %% Compute Entropy on all data

    NumTrials = length(CPos);
    entropy = nan(NumWindows, NumSelFreqs, NumChannels);
    
    trId = 0;
    cstart = 1;
    cstop = [];
    disp('[proc] - Computing entropy for each trial:');
    for sId = 1:NumWindows

        % Check for trial start
        if isempty(find(sId == CPos, 1)) == false 
            cstart = sId; 
            trId = trId +1;
            cstop  = cstart + CDur(trId) - 1;
            util_disp_progress(trId, NumTrials, '        ');
        end   
        
        % Check if the sample is inside the trial
        if isempty(cstop) == false && (sId <= cstop)
            for bId = 1:NumSelFreqs
                cfreqId = SelFreqId(bId);
                cpsd = squeeze(D(cstart:sId, cfreqId, :));
                if(sum(sum(isnan(cpsd))) > 0)
                    keyboard;
                end
                if dolog == true
                    cpsd = log(cpsd + 1);
                end
                entropy(sId, bId, :) = proc_entropy(cpsd, NumBins);     
            end
        end
    end

    analysis.entropy.bins  = NumBins;
    analysis.entropy.freqs = Freqs(SelFreqId);
    analysis.entropy.dolog = dolog;

    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    disp(['[out] - Saving entropy in: ' sfilename]);
    save(sfilename, 'entropy', 'analysis'); 
end