clear all; clc;

subject     = 'c6';

pattern  = 'hfr';
entropypath = 'analysis/smr-inc/entropy/log/';
filenames = util_getfile(entropypath, '.mat', [subject '*' pattern]);
NumFiles  = length(filenames);

AlphaTTest = 0.01;
BandsLb{1} = '2-6 Hz';
BandsLb{2} = '8-12 Hz';
BandsLb{3} = '14-20 Hz';
BandsLb{4} = '22-30 Hz';
BandsLb{5} = '32-48 Hz';
BandsLb{6} = '2-36 Hz';
BandsLb{7} = '8-30 Hz';

E   = [];
Rk  = [];
Mk  = [];
typ = [];
pos = [];
dur = [];

%% Loading pre-computed entropy
for fId = 1:NumFiles
    cfilename = filenames{fId};
    util_bdisp(['[io] - Loading pre-computed entropy ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
     
    if(isempty(regexp(cfilename, 'offline', 'ONCE')) == false)
        cmod = 0;
    elseif(isempty(regexp(cfilename, 'online', 'ONCE')) == false)
        cmod = 1;
    else
        error('chk:mod', 'Cannot retrieve modality from filename');
    end
    
    cdata    = load(cfilename);

    ctyp  = cdata.analysis.event.TYP;
    cpos  = cdata.analysis.event.POS;
    cdur  = cdata.analysis.event.DUR;
    
    typ = cat(1, typ, ctyp);
    dur = cat(1, dur, cdur);
    pos = cat(1, pos, cpos + size(E, 1));
    
    E  = cat(1, E, cdata.entropy);
    Rk = cat(1, Rk, fId*ones(size(cdata.entropy, 1), 1));
    Mk = cat(1, Mk, cmod*ones(size(cdata.entropy, 1), 1));
    
    SampleRate = cdata.analysis.processing.fs;
    FrameSize  = cdata.analysis.processing.fsize;
end
  
NumSamples  = size(E, 1);
NumBands    = size(E, 2);
NumChannels = size(E, 3);

%% Vector labels
event.TYP = typ;
event.POS = pos;
event.DUR = dur;

%[Ck, CPos, CDur] = proc_getevents(event, NumSamples, 781, [], floor(4*SampleRate/FrameSize));
[Ck, CPos, CDur] = proc_getevents(event, NumSamples, 781);

TrialClass = event.TYP(event.TYP == 771 | event.TYP == 773 | event.TYP == 783);
Classes = [1 2];
Ek = zeros(NumSamples, 1);
for trId = 1:length(CPos)
    cstart = CPos(trId);
    cstop  = cstart + CDur(trId);
    cclass = TrialClass(trId);
    if(cclass ~= 783)
        Ek(cstart:cstop) = Classes(1);
    else
        Ek(cstart:cstop) = Classes(2);
    end
end

%% Dataset creation
util_bdisp('[proc] - Balancing datasets');
% For offline modality
discard_train = false(length(Ek) , 1);
minsamples = min([sum(Ek == Classes(1) & Mk == 0 & Ck) sum(Ek == Classes(2) & Mk == 0 & Ck)]);
[maxsamples, maxclass] = max([sum(Ek == Classes(1) & Mk == 0 & Ck) sum(Ek == Classes(2) & Mk == 0 & Ck)]);
maxidx = find(Ek == Classes(maxclass) & Mk == 0 & Ck);
discard_train(maxidx(randperm(length(maxidx), maxsamples - minsamples))) = true;
discarded = maxsamples - minsamples;
disp(['[proc] - ' num2str(100*(discarded)/maxsamples, 3) '% of samples removed from class ' num2str(maxclass) ' to balance Training dataset']);


% For online modality
discard_eval = false(length(Ek) , 1);
minsamples = min([sum(Ek == Classes(1) & Mk == 1 & Ck) sum(Ek == Classes(2) & Mk == 1 & Ck)]);
[maxsamples, maxclass] = max([sum(Ek == Classes(1) & Mk == 1 & Ck) sum(Ek == Classes(2) & Mk == 1 & Ck)]);
maxidx = find(Ek == Classes(maxclass) & Mk == 1 & Ck);
discard_eval(maxidx(randperm(length(maxidx), maxsamples - minsamples))) = true;
discarded = maxsamples - minsamples;
disp(['[proc] - ' num2str(100*(discarded)/maxsamples, 3) '% of samples removed from class ' num2str(maxclass) ' to balance Evaluation dataset']);

Discarded = discard_train | discard_eval;

EvalK = Ck & Mk == 0 & ~Discarded;

%% Grand average differences and statistical tests
AvgDiff = squeeze(mean(E(EvalK & Ek == 1, :, :)) - mean(E(EvalK & Ek == 2, :, :)));

HValues  = zeros(NumBands, NumChannels);
PValues  = zeros(NumBands, NumChannels);

for chId = 1:NumChannels
    for bId = 1:NumBands
        [h, p] = ttest2(squeeze(E(EvalK & Ek == 1, bId, chId)), squeeze(E(EvalK & Ek == 2, bId, chId)), AlphaTTest);
        HValues(bId, chId) = h;
        PValues(bId, chId) = p;
    end
end
    

%% Topoplots
load('chanlocs16');
figure;
fig_set_position(gcf, 'Top');
for bId = 1:NumBands
    subplot(1, NumBands, bId);
    idsign = find(HValues(bId, :) == 1);
    topoplot(AvgDiff(bId, :)', chanlocs16, 'intrad', 0.4, 'headrad', 0.4, 'emarker2', {[idsign],'.','r'} );
    axis image;
    title(BandsLb{bId});
end

