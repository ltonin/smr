clear all; clc;

%%%%%%%%%%%%%%%%
% subject          = 'b4';
% rootpath         = '/mnt/data/Research/smr-inc/20160301_b4/';
% smr.alpha        = 0.98;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'a1';
% rootpath         = '/mnt/data/Research/smr-inc/20160310_a1/';
% smr.alpha        = 0.97;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'b9';
% rootpath         = '/mnt/data/Research/smr-inc/20160316_b9/';
% smr.alpha        = 0.97;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'c2';
% rootpath         = '/mnt/data/Research/smr-inc/20160321_c2/';
% smr.alpha        = 0.97;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'c4';
% rootpath         = '/mnt/data/Research/smr-inc/20160324_c4/';
% smr.alpha        = 0.97;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'c5';
% rootpath         = '/mnt/data/Research/smr-inc/20160329_c5/';
% smr.alpha        = 0.97;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
subject          = 'c6';
rootpath         = '/mnt/data/Research/smr-inc/20160330_c6/';
smr.alpha        = 0.97;
%%%%%%%%%%%%%%%%

pattern       = 'hfr';
psdpath       = 'analysis/smr-inc/psd/';
savepath      = 'analysis/smr-inc/pillonetto/';

psdfiles = util_getfile(psdpath, '.mat', [subject '*' pattern]);
smr.filename = util_getfile(rootpath, '.dat', [subject '_bhbf_*']); 

NumFiles  = length(psdfiles);
ModalityRun = [0 0 0 1 1 1];

D         = [];
Modk        = [];
event.TYP = [];
event.POS = [];
event.DUR = [];

for fId = 1:NumFiles
    cpsdfile = psdfiles{fId};
    util_bdisp(['[io] - Loading PSD ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - PSD file:     ' cpsdfile]);
    
    cpsd = load(cpsdfile);
    
    ctyp  = cpsd.analysis.event.TYP;
    cpos  = cpsd.analysis.event.POS;
    cdur  = cpsd.analysis.event.DUR;
    
    event.TYP = cat(1, event.TYP, cpsd.analysis.event.TYP);
    event.DUR = cat(1, event.DUR, cpsd.analysis.event.DUR);
    event.POS = cat(1, event.POS, cpsd.analysis.event.POS + size(D, 1));
    
    Modk  = cat(1, Modk, ModalityRun(fId)*ones(size(cpsd.psd, 1), 1));
    D   = cat(1, D, cpsd.psd);
    
    Freqs = cpsd.analysis.processing.f;   
end

FreqGrid = 4:2:48;
[~, FreqId] = intersect(Freqs, FreqGrid); 
D = D(:, FreqId, :);
NumSamples  = size(D, 1);
NumFreqs    = size(D, 2);
NumChannels = size(D, 3);

%% Vector labels
util_bdisp('[proc] - Creating vector labels for SMR');
[~, CPos, CDur] = proc_getevents(event, size(D, 1), 781);


TrialClass = event.TYP(event.TYP == 771 | event.TYP == 773 | event.TYP == 783);
NumTrials = length(CPos);

Dk = zeros(NumSamples, 1);
for trId = 1:NumTrials
    cstart = CPos(trId);
    cstop  = cstart + CDur(trId);
    cclass = TrialClass(trId);
    Dk(cstart:cstop) = cclass;
end

%% Data Extraction
util_bdisp('[proc] - Extracting SMR features');
Sk = Dk == 771 | Dk == 773;
F  = proc_reshape_ts_bc(log(D));
Y  = Dk(Sk);
X  = F(Sk, :);
Mk = Modk(Sk);

%% SMR classification
util_bdisp('[proc] - SMR classification:');
% Loading SMR classifier
disp('       - Loading SMR classifier:');
disp(['       - Filename: ' smr.filename{1}]);
smr.classifier = smr_inc_load_cnbismr_classifier(smr.filename{1}, NumFreqs, NumChannels, FreqGrid);

% Simulating online SMR classification
disp('       - Simulating online SMR classifier');
[pp, Gk] = classTest(smr.classifier.model, X(:, smr.classifier.features));
disp(['       - Accuracy on Train set: ' num2str(100*sum(Gk(Mk == 0) == Y(Mk == 0))/length(Y(Mk == 0))) '%']);
disp(['       - Accuracy on Test set:  ' num2str(100*sum(Gk(Mk == 1) == Y(Mk == 1))/length(Y(Mk == 1))) '%']);

%% Saving data
util_bdisp(['[out] - Saving data for subject ' subject ': ' savepath subject '_smr_features.mat']);
Y(Y==771) =  1;
Y(Y==773) = -1;
save([savepath subject '_smr_features.mat'], 'X', 'Y', 'Mk', 'smr');