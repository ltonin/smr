clear all; clc;

%%%%%%%%%%%%%%%%
subject     = 'b4';
Alpha       = 0.97;
Rejection   = 0.7;
NumSelected = 4;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject     = 'a1';
% Alpha       = 0.98;
% Rejection   = 0.55;
% NumSelected = 4;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject     = 'b9';
% Alpha       = 0.97;
% Rejection   = 0.75;
% NumSelected = 8;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject     = 'c2';
% Alpha       = 0.97;
% Rejection   = 0.8;
% NumSelected = 5;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject     = 'c4';
% Alpha       = 0.98;
% Rejection   = 0.65;
% NumSelected = 4;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject     = 'c5';
% Alpha       = 0.97;
% Rejection   = 0.70;
% NumSelected = 1;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject     = 'c6';
% Alpha       = 0.97;
% Rejection   = 0.70;
% NumSelected = 3;
%%%%%%%%%%%%%%%%

pattern  = 'hfr';
entropypath = 'analysis/smr-inc/entropy/log/';
filenames = util_getfile(entropypath, '.mat', [subject '*' pattern]);
NumFiles  = length(filenames);

proc_pattern = 'csp';
ClassifierType = 'qda';

E   = [];
Rk  = [];
Mk  = [];
typ = [];
pos = [];
dur = [];

%% Loading pre-computed entropy
for fId = 1:NumFiles
    cfilename = filenames{fId};
    util_bdisp(['[io] - Loading pre-computed entropy ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
     
    if(isempty(regexp(cfilename, 'offline', 'ONCE')) == false)
        cmod = 0;
    elseif(isempty(regexp(cfilename, 'online', 'ONCE')) == false)
        cmod = 1;
    else
        error('chk:mod', 'Cannot retrieve modality from filename');
    end
    
    cdata    = load(cfilename);

    ctyp  = cdata.analysis.event.TYP;
    cpos  = cdata.analysis.event.POS;
    cdur  = cdata.analysis.event.DUR;
    
    typ = cat(1, typ, ctyp);
    dur = cat(1, dur, cdur);
    pos = cat(1, pos, cpos + size(E, 1));
    
    E  = cat(1, E, cdata.entropy);
    Rk = cat(1, Rk, fId*ones(size(cdata.entropy, 1), 1));
    Mk = cat(1, Mk, cmod*ones(size(cdata.entropy, 1), 1));
    
    SampleRate = cdata.analysis.processing.fs;
    FrameSize  = cdata.analysis.processing.fsize;
end
  
NumSamples  = size(E, 1);
NumBands    = size(E, 2);
NumChannels = size(E, 3);

%% Vector labels
event.TYP = typ;
event.POS = pos;
event.DUR = dur;

[Ck, CPos, CDur] = proc_getevents(event, NumSamples, 781);
TrialClass = event.TYP(event.TYP == 771 | event.TYP == 773 | event.TYP == 783);
Classes = [1 2];
Ek = zeros(NumSamples, 1);
for trId = 1:length(CPos)
    cstart = CPos(trId);
    cstop  = cstart + CDur(trId);
    cclass = TrialClass(trId);
    if(cclass ~= 783)
        Ek(cstart:cstop) = Classes(1);
    else
        Ek(cstart:cstop) = Classes(2);
    end
end


TrainK = Ck & Mk == 0;
EvalK  = Ck & Mk == 1;
RealK  = EvalK;

%% Pattern processing
util_bdisp('[proc] - Extracting features');
P = E;
if (strcmpi(proc_pattern, 'csp')  == true)
    disp('[proc] - Computing CSP on training');
    coeff  = zeros(NumChannels, NumChannels, NumBands);
    for bId = 1:NumBands
       cdata = squeeze(E(:, bId, :));
       coeff(:, :, bId) = filt_csp_dec(cdata(TrainK, :), Ek(TrainK));
    end
    disp('[proc] - Applying CSP');
    for bId = 1:NumBands
        cdata = squeeze(E(:, bId, :));
        P(:, bId, :) = filt_csp_apply(cdata, coeff(:, :, bId), NumChannels);
    end
elseif (strcmpi(proc_pattern, 'pca')  == true)
    disp('[proc] - Computing PCA on training');
    coeff  = zeros(NumChannels, NumChannels, NumBands);
    for bId = 1:NumBands
       cdata = squeeze(E(:, bId, :));
       coeff(:, :, bId) = pca(cdata(TrainK, :));
    end
    disp('[proc] - Applying PCA');
    for bId = 1:NumBands
        cdata = squeeze(E(:, bId, :));
        P(:, bId, :) = cdata*coeff(:, :, bId);
    end
    
end
    
%% Variance computation
WinStep = 1;
WinSize = 16; % ==> To be changed: now 1 seconds

WinStart = 1:WinStep:NumSamples;
WinStop  = WinStart + WinSize - 1;

NumWins  = length(WinStart);

util_bdisp('[proc] - Features variance');
V = zeros(NumWins, NumBands, NumChannels);
for  wId = 1:NumWins
    cstart = WinStart(wId);
    cstop  = WinStop(wId);
    
    if cstop > NumSamples
        cstop = NumSamples;
    end
    for bId = 1:NumBands
        cdata = squeeze(P(cstart:cstop, bId, :));
        %V(wId, bId, :) = log(diag(cdata'*cdata)/trace(cdata'*cdata));
        V(wId, bId, :) = diag(cdata'*cdata)/trace(cdata'*cdata);
    end
end



F  = proc_reshape_ts_bc(V);
Fk = Ek;

%% Feature selection
util_bdisp('[proc] - Selecting features');
[dp, idfeatures] = proc_cva_selection(F(TrainK, :), Fk(TrainK), Rk(TrainK), NumSelected, 'draw', [NumBands size(F, 2)/NumBands]);

%% Classification
util_bdisp('[proc] - Classification');
disp(['[proc] - Training classifier on Training data - ' ClassifierType]);
model = classTrain(F(TrainK, idfeatures), Fk(TrainK), ClassifierType);

disp('[proc] - Testing classifier on all data');
[rawpp, Gk.raw] = classTest(model, F(:, idfeatures)); 

%% Integration
disp('[proc] - Integrate raw probabilities');
intpp = rawpp;
trial = 0;

for sId = 2:size(rawpp, 1)
    cpp  = rawpp(sId, :);
    pdec = intpp(sId -1, :);
    
    if isempty(find(CPos == sId, 1)) == false
        pdec = [0.5 0.5];
        trial = trial+1;
    end
    
    if (max(cpp) > Rejection)
        intpp(sId, :) = pdec.*Alpha + cpp.*(1-Alpha);
    else
        intpp(sId, :) = pdec;
    end
    [~, Gk.int(sId)] = max(intpp(sId, :)); 
end

%% Accuracy
util_bdisp('[out] - Accuracy on raw probs:');

kstr.raw.train        = kappa(Gk.raw(TrainK), Fk(TrainK));
kstr.raw.eval.balance = kappa(Gk.raw(EvalK), Fk(EvalK));
kstr.raw.eval.real    = kappa(Gk.raw(RealK), Fk(RealK));

disp(['[out] - Average accuracy on balance training data: ' num2str(mean(diag(100*2*confusionmat(Fk(TrainK), Gk.raw(TrainK))/sum(TrainK))), 4) '%'])
disp(['[out] - Average accuracy on balance testing data:  ' num2str(mean(diag(100*2*confusionmat(Fk(EvalK), Gk.raw(EvalK))/sum(EvalK))), 4) '%'])
disp(['[out] - Average accuracy on real testing data:     ' num2str(mean(diag(100*2*confusionmat(Fk(RealK), Gk.raw(RealK))/sum(RealK))), 4) '%'])

disp(['[proc] - Kappa value on balance training data: ' num2str(kstr.raw.train.kappa, 3)]);
disp(['[proc] - Kappa value on balance testing data:  ' num2str(kstr.raw.eval.balance.kappa, 3)]);
disp(['[proc] - Kappa value on real testing data:     ' num2str(kstr.raw.eval.real.kappa, 3)]);

util_bdisp('[out] - Accuracy on integrated probs:');

kstr.int.train        = kappa(Gk.int(TrainK), Fk(TrainK));
kstr.int.eval.balance = kappa(Gk.int(EvalK), Fk(EvalK));
kstr.int.eval.real    = kappa(Gk.int(RealK), Fk(RealK));

disp(['[out] - Average accuracy on balance training data: ' num2str(mean(diag(100*2*confusionmat(Fk(TrainK), Gk.int(TrainK))/sum(TrainK))), 4) '%'])
disp(['[out] - Average accuracy on balance testing data:  ' num2str(mean(diag(100*2*confusionmat(Fk(EvalK), Gk.int(EvalK))/sum(EvalK))), 4) '%'])
disp(['[out] - Average accuracy on real testing data:     ' num2str(mean(diag(100*2*confusionmat(Fk(RealK), Gk.int(RealK))/sum(RealK))), 4) '%'])

disp(['[proc] - Kappa value on balance training data: ' num2str(kstr.int.train.kappa, 3)]);
disp(['[proc] - Kappa value on balance testing data:  ' num2str(kstr.int.eval.balance.kappa, 3)]);
disp(['[proc] - Kappa value on real testing data:     ' num2str(kstr.int.eval.real.kappa, 3)]);

% %% Saving Entropy probabilities
% analysis.entropy.processing.pattern         = proc_pattern;
% analysis.entropy.processing.coefficients    = coeff;
% analysis.entropy.classification.integration = Alpha;
% analysis.entropy.classification.rejection   = Rejection;
% analysis.entropy.classification.idfeatures  = idfeatures;
% 
% sfilename = [entropypath subject '_entropy_classifier.mat'];
% util_bdisp(['[out] - Saving entropy classifier in: ' sfilename]);
% save(sfilename, 'model', 'analysis'); 

