clearvars; clc;

%%%%%%%%%%%%%%%%
subject     = 'AN14VE'; %'b4';
Alpha       = 0.98;
Rejection   = 0.7;
NumSelected = 6;
%%%%%%%%%%%%%%%%

pattern  = 'rfsb'; %'hfr';
entropypath = 'analysis/smr-inc/entropy_hr/log/';
filenames = util_getfile(entropypath, '.mat', [subject '*' pattern]);
NumFiles  = length(filenames);

proc_pattern = 'csp';
ClassifierType = 'qda';

E   = [];
Rk  = [];
Mk  = [];
typ = [];
pos = [];
dur = [];

%% Loading pre-computed entropy
for fId = 1:NumFiles
    cfilename = filenames{fId};
    util_bdisp(['[io] - Loading pre-computed entropy ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
     
    if(isempty(regexp(cfilename, 'offline', 'ONCE')) == false)
        cmod = 0;
    elseif(isempty(regexp(cfilename, 'online', 'ONCE')) == false)
        cmod = 1;
    else
        error('chk:mod', 'Cannot retrieve modality from filename');
    end
    
    cdata    = load(cfilename);

    ctyp  = cdata.analysis.event.TYP;
    cpos  = cdata.analysis.event.POS;
    cdur  = cdata.analysis.event.DUR;
    
    typ = cat(1, typ, ctyp);
    dur = cat(1, dur, cdur);
    pos = cat(1, pos, cpos + size(E, 1));
    
    E  = cat(1, E, cdata.entropy);
    Rk = cat(1, Rk, fId*ones(size(cdata.entropy, 1), 1));
    Mk = cat(1, Mk, cmod*ones(size(cdata.entropy, 1), 1));
    
    SampleRate = cdata.analysis.processing.fs;
    FrameSize  = cdata.analysis.processing.fsize;
end
  
NumSamples  = size(E, 1);
NumBands    = size(E, 2);
NumChannels = size(E, 3);

%% Vector labels
event.TYP = typ;
event.POS = pos;
event.DUR = dur;

[Ck, CPos, CDur] = proc_getevents(event, NumSamples, 781);

% Events Trials & Events Class Vector
EventT = event.TYP(event.TYP == 770 |event.TYP == 771 | event.TYP == 773 | event.TYP == 774 | event.TYP == 783);
EventK = zeros(NumSamples, 1);      
for trId = 1:length(CPos)
    cstart = CPos(trId);
    cstop  = cstart + CDur(trId);
    EventK(cstart:cstop) = EventT(trId);
end

% Remapping Events Class Vector to Tasks of interest
TaskCodes  = {783, [770 771]};
TaskLabels = {'Rest', 'Smr'};
TaskK = zeros(NumSamples, 1);
for gId = 1:length(TaskCodes)
    for tId = 1:length(TaskCodes{gId})
        ccode = TaskCodes{gId}(tId);
        TaskK(EventK == ccode) = gId;
    end
end
    
%% CSP

entropy_csp_coeff = filt_csp_dec(E(TaskK > 0, :, :), TaskK(TaskK > 0));
entropy_csp       = filt_csp_apply(E, entropy_csp_coeff, NumChannels);

%% Dataset creation
MaskTrain  = TaskK > 0 & Mk == 0;
MaskEval   = TaskK > 0 & Mk == 1;

%% Feature selection

F  = proc_reshape_ts_bc(entropy_csp);
Fk = TaskK;

util_bdisp('[proc] - Selecting features');
[dp, idfeatures] = proc_cva_selection(F(MaskTrain, :), Fk(MaskTrain), Rk(MaskTrain), NumSelected, 'draw', [NumBands size(F, 2)/NumBands]);

%% Classification
util_bdisp('[proc] - Classification');
disp(['[proc] - Training classifier on Training data - ' ClassifierType]);
model = classify_train(F(MaskTrain, idfeatures), Fk(MaskTrain), 'qda', 'estimation', 'shrink', 'lambda', 0.4);
%model = classify_train(F(MaskTrain, idfeatures), Fk(MaskTrain), 'lda');

disp('[proc] - Testing classifier on all data');
[rawpp, Gk] = classify_eval(model, F(:, idfeatures)); 




