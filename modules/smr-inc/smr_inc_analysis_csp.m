clearvars; clc;

%%%%%%%%%%%%%%%%
% subject = 'AN14VE';
% pattern = 'rfsb';
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
subject = 'b4'; 
pattern = 'hfr';
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject = 'a1'; 
% pattern = 'hfr';
%%%%%%%%%%%%%%%%


psdpath  = 'analysis/smr-inc/psd/csp/';
entpath  = 'analysis/smr-inc/entropy/csp/nolog/';
psdfiles = util_getfile(psdpath, '.mat', [subject '*' pattern]);
entfiles = util_getfile(entpath, '.mat', [subject '*' pattern]);



if isequal(length(entfiles), length(psdfiles)) == false
    error('[io] - Different number of entropy and psd filenames');
end

NumFiles = length(entfiles);

S   = [];
E   = [];
Rk  = [];
Mk  = [];
typ = [];
pos = [];
dur = [];

%% Loading pre-computed psd
for fId = 1:NumFiles
    cfilename = psdfiles{fId};
    util_bdisp(['[io] - Loading pre-computed psd ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
     
    if(isempty(regexp(cfilename, 'offline', 'ONCE')) == false)
        cmod = 0;
    elseif(isempty(regexp(cfilename, 'online', 'ONCE')) == false)
        cmod = 1;
    else
        error('chk:mod', 'Cannot retrieve modality from filename');
    end
    
    cdata    = load(cfilename);
    analysis = cdata.analysis;
    
    
    ctyp  = cdata.analysis.event.TYP;
    cpos  = cdata.analysis.event.POS;
    cdur  = cdata.analysis.event.DUR;
    
    typ = cat(1, typ, ctyp);
    dur = cat(1, dur, cdur);
    pos = cat(1, pos, cpos + size(S, 1));
    
    S  = cat(1, S, cdata.psd);
    Rk = cat(1, Rk, fId*ones(size(cdata.psd, 1), 1));
    Mk = cat(1, Mk, cmod*ones(size(cdata.psd, 1), 1));
    
    SampleRate = cdata.analysis.processing.fs;
    FrameSize  = cdata.analysis.processing.fsize;
    PsdFreqs   = analysis.processing.f;
end

NumWindows  = size(S, 1);
NumPsdFreqs = size(S, 2);
NumChannels = size(S, 3);

event.TYP = typ;
event.POS = pos;
event.DUR = dur;

%% Loading pre-computed entropy
for fId = 1:NumFiles
    cfilename = entfiles{fId};
    util_bdisp(['[io] - Loading pre-computed entropy ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    cdata    = load(cfilename);
    analysis = cdata.analysis;
    
    E  = cat(1, E, cdata.entropy);
    
    EntropyFreqs = analysis.entropy.freqs;
end

NumEntFreqs = size(E, 2);

%% Reducing number of PSD frequencies
[~, psdfreqId] = intersect(PsdFreqs, EntropyFreqs);
S = S(:, psdfreqId, :);
Freqs    = EntropyFreqs;
NumFreqs = length(Freqs);

%% Event vector generation
[Ck, CPos, CDur] = proc_getevents(event, NumWindows, 781);

% Events Trials & Events Class Vector
EventT = event.TYP(event.TYP == 770 |event.TYP == 771 | event.TYP == 773 | event.TYP == 774 | event.TYP == 783);
EventK = zeros(NumWindows, 1);      
for trId = 1:length(CPos)
    cstart = CPos(trId);
    cstop  = cstart + CDur(trId) - 1;
    if (cstop > NumWindows)
        cstop = NumWindows;
    end
    EventK(cstart:cstop) = EventT(trId);
end

% Remapping Events Class Vector to Tasks of interest
TaskCodes  = {783, [770 771 773]};
TaskLabels = {'Rest', 'Smr'};
TaskK = zeros(NumWindows, 1);
for gId = 1:length(TaskCodes)
    for tId = 1:length(TaskCodes{gId})
        ccode = TaskCodes{gId}(tId);
        TaskK(EventK == ccode) = gId;
    end
end


[Ik, IPos, IDur] = proc_getevents(event, NumWindows, 781);

%% Dataset creation
TrainMask = TaskK > 0 & Mk == 0;
EvalMask  = TaskK > 0 & Mk == 1;
%% Feature selection
FS = proc_reshape_ts_bc(S);
[dp, idfeatures] = proc_cva_selection(FS(TrainMask, :), TaskK(TrainMask) , Rk(TrainMask), 2, 'draw', [NumFreqs NumChannels]);

%% Classification
util_bdisp('[proc] - Classification');
ClassifierType = 'lda';
disp(['[proc] - Training classifier on Training data - ' ClassifierType]);
model = classify_train(FS(TrainMask, idfeatures), TaskK(TrainMask), ClassifierType, 'estimation', 'shrink', 'lambda', 0.0);
%model = classify_train(FS(TrainMask, idfeatures), TaskK(TrainMask), ClassifierType);

disp('[proc] - Testing classifier on all data');
[rawpp, Gk] = classify_eval(model, FS(:, idfeatures)); 
[TrainCM, ~, TrainAcc] = classify_confusion_matrix(TaskK(TrainMask), Gk(TrainMask));
[EvalCM, ~, EvalAcc] = classify_confusion_matrix(TaskK(EvalMask), Gk(EvalMask));

disp('[proc] - Training confusion matrix:');
disp(TrainCM);
disp(['[proc] - Training accuracy: ' num2str(TrainAcc)]);

disp('[proc] - Evaluation confusion matrix:');
disp(EvalCM);
disp(['[proc] - Evaluation accuracy: ' num2str(EvalAcc)]);

%% Plotting
Splot = S;
Eplot = E;
Patterns = [1 NumChannels];
PlotFreqs = 8:30;
[~, plotfreqId] = intersect(Freqs, PlotFreqs);

figure
subplot(2, 2, 1);
hold on;
plot(mean(Splot(TaskK == 1 & Mk == 0, plotfreqId, Patterns(1)), 2), mean(Splot(TaskK == 1  & Mk == 0, plotfreqId, Patterns(2)), 2), '.b');
plot(mean(Splot(TaskK == 2 & Mk == 0, plotfreqId, Patterns(1)), 2), mean(Splot(TaskK == 2  & Mk == 0, plotfreqId, Patterns(2)), 2), 'gx');
hold off;
legend('Rest', 'Smr');
title('CSP+PSD (offline)');
grid on;
xlabel(['Pattern ' num2str(Patterns(1))]);
ylabel(['Pattern ' num2str(Patterns(2))]);

subplot(2, 2, 2);
hold on;
plot(mean(Splot(TaskK == 1 & Mk == 1, plotfreqId, Patterns(1)), 2), mean(Splot(TaskK == 1  & Mk == 1, plotfreqId, Patterns(2)), 2), '.b');
plot(mean(Splot(TaskK == 2 & Mk == 1, plotfreqId, Patterns(1)), 2), mean(Splot(TaskK == 2  & Mk == 1, plotfreqId, Patterns(2)), 2), 'gx');
hold off;
legend('Rest', 'Smr');
title('CSP+PSD (online)');
grid on;
xlabel(['Pattern ' num2str(Patterns(1))]);
ylabel(['Pattern ' num2str(Patterns(2))]);

subplot(2, 2, 3);
hold on;
plot(mean(Eplot(TaskK == 1 & Mk == 0 & Ik, plotfreqId, Patterns(1)), 2), mean(Eplot(TaskK == 1  & Mk == 0 & Ik, plotfreqId, Patterns(2)), 2), '.b');
plot(mean(Eplot(TaskK == 2 & Mk == 0 & Ik, plotfreqId, Patterns(1)), 2), mean(Eplot(TaskK == 2  & Mk == 0 & Ik, plotfreqId, Patterns(2)), 2), 'gx');
hold off;
legend('Rest', 'Smr');
title('CSP+PSD+Entropy (offline)');
grid on;
xlabel(['Pattern ' num2str(Patterns(1))]);
ylabel(['Pattern ' num2str(Patterns(2))]);

subplot(2, 2, 4);
hold on;
plot(mean(Eplot(TaskK == 1 & Mk == 1 & Ik, plotfreqId, Patterns(1)), 2), mean(Eplot(TaskK == 1  & Mk == 1 & Ik, plotfreqId, Patterns(2)), 2), '.b');
plot(mean(Eplot(TaskK == 2 & Mk == 1 & Ik, plotfreqId, Patterns(1)), 2), mean(Eplot(TaskK == 2  & Mk == 1 & Ik, plotfreqId, Patterns(2)), 2), 'gx');
hold off;
legend('Rest', 'Smr');
title('CSP+PSD+Entropy (online)');
grid on;
xlabel(['Pattern ' num2str(Patterns(1))]);
ylabel(['Pattern ' num2str(Patterns(2))]);

% %% Buffering and decimation (hard)
% 
% BufferLength = 1;
% FrameLength  = 0.0625;
% 
% BufferSize = BufferLength*h.SampleRate;
% FrameSize  = FrameLength*h.SampleRate;
% 
% BufferStart = 1:FrameSize:NumSamples;
% BufferStop  = BufferStart + BufferSize - 1;
% BufferStop(BufferStop > NumSamples) = NumSamples;
% NumWindows = length(BufferStart);
% 
% S = zeros(NumWindows, NumChannels);
% 
% for wId = 1:NumWindows
%     cstart = BufferStart(wId);
%     cstop  = BufferStop(wId);
%     S(wId, :) = mean(D(cstart:cstop, :), 1);
% end
% 
% event.POS = floor(event.POS/FrameSize);
% event.DUR = floor(event.DUR/FrameSize);
% 
% %% Event vector generation
% [Ck, CPos, CDur] = proc_getevents(event, NumWindows, 781);
% 
% % Events Trials & Events Class Vector
% EventT = event.TYP(event.TYP == 770 |event.TYP == 771 | event.TYP == 773 | event.TYP == 774 | event.TYP == 783);
% EventK = zeros(NumWindows, 1);      
% for trId = 1:length(CPos)
%     cstart = CPos(trId);
%     cstop  = cstart + CDur(trId);
%     if (cstop > NumWindows)
%         cstop = NumWindows;
%     end
%     EventK(cstart:cstop) = EventT(trId);
% end
% 
% % Remapping Events Class Vector to Tasks of interest
% TaskCodes  = {783, [770 771 773]};
% TaskLabels = {'Rest', 'Smr'};
% TaskK = zeros(NumWindows, 1);
% for gId = 1:length(TaskCodes)
%     for tId = 1:length(TaskCodes{gId})
%         ccode = TaskCodes{gId}(tId);
%         TaskK(EventK == ccode) = gId;
%     end
% end
% 
% %% Entropy on original data
% NumBins = 32;
% NumTrials = length(CPos);
% entropy_flt = zeros(NumWindows, NumChannels);
% trId = 0;
% cstart = 1;
% disp('[proc] - Computing entropy for each trial:');
% for sId = 1:NumWindows
% 
%     % Check for reset entropy accumulation
%     if isempty(find(sId == CPos, 1)) == false 
%         cstart = sId;
%         trId = trId +1;
%         util_disp_progress(trId, NumTrials, '        ');
%     end   
%     
%     cS   = S(cstart:sId, :);
%     entropy_flt(sId, :) = proc_entropy(cS, NumBins);     
% end
% 
% 
% %% Common Spatial Patterns on original data and on entropy
% entropy_coeff = filt_csp_dec(entropy_flt(TaskK > 0, :), TaskK(TaskK > 0, :));
% entropy_csp   = filt_csp_apply(entropy_flt, entropy_coeff, NumChannels);
% 
% flt_coeff = filt_csp_dec(S(TaskK > 0, :), TaskK(TaskK > 0, :));
% flt_csp   = filt_csp_apply(S, flt_coeff, NumChannels);
% 
% %% Classification
% ClassifierType = 'qda';
% util_bdisp('[proc] - Classification');
% disp(['[proc] - Training classifier on Training data - ' ClassifierType]);
% model = classify_train(flt_csp(TaskK > 0, [1:NumChannels]), TaskK(TaskK > 0), ClassifierType, 'estimation', 'shrink', 'lambda', 0);
% 
% disp('[proc] - Testing classifier on all data');
% [rawpp, Gk] = classify_eval(model, flt_csp(:, [1:NumChannels])); 
% [cm, ~, acc] = classify_confusion_matrix(TaskK(TaskK > 0), Gk(TaskK > 0));
% disp('[output] - Confusion matrix');
% disp(cm);
% disp(['[output] - Overall accuracy: ' num2str(acc) '%']);
% 
% %% Plotting
% Channels = [9 11];
% Patterns = [1 NumChannels];
% subplot(2, 2, 1);
% hold on;
% plot(S(TaskK == 1, Channels(1)), S(TaskK == 1, Channels(2)), '.');
% plot(S(TaskK == 2, Channels(1)), S(TaskK == 2, Channels(2)), 'gx');
% hold off;
% title('Filtered data');
% xlabel(['Channel ' num2str(Channels(1))]);
% ylabel(['Channel ' num2str(Channels(2))]);
% 
% subplot(2, 2, 2);
% hold on;
% plot(flt_csp(TaskK == 1, Patterns(1)), flt_csp(TaskK == 1, Patterns(2)), '.');
% plot(flt_csp(TaskK == 2, Patterns(1)), flt_csp(TaskK == 2, Patterns(2)), 'gx');
% hold off;
% title('CSP patterns on filtered data');
% xlabel(['Pattern ' num2str(Patterns(1))]);
% ylabel(['Pattern ' num2str(Patterns(2))]);
% 
% subplot(2, 2, 3);
% hold on;
% plot(entropy_flt(TaskK == 1, Channels(1)), entropy_flt(TaskK == 1, Channels(2)), '.');
% plot(entropy_flt(TaskK == 2, Channels(1)), entropy_flt(TaskK == 2, Channels(2)), 'gx');
% hold off;
% title('Entropy on Filtered data');
% xlabel(['Channel ' num2str(Channels(1))]);
% ylabel(['Channel ' num2str(Channels(2))]);
% 
% subplot(2, 2, 4);
% hold on;
% plot(entropy_csp(TaskK == 1, Patterns(1)), entropy_csp(TaskK == 1, Patterns(2)), '.');
% plot(entropy_csp(TaskK == 2, Patterns(1)), entropy_csp(TaskK == 2, Patterns(2)), 'gx');
% hold off;
% title('Entropy on CSP patterns on Filtered data');
% xlabel(['Pattern ' num2str(Patterns(1))]);
% ylabel(['Pattern ' num2str(Patterns(2))]);

