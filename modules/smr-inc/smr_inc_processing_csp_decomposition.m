clearvars; clc;

%%%%%%%%%%%%%%%%%%
subject  = 'AN14VE';
rootpath = '/mnt/data/Research/smr-inc/20160624_AN14VE/';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'b4';
% rootpath = '/mnt/data/Research/smr-inc/20160301_b4/';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'a1';
% rootpath = '/mnt/data/Research/smr-inc/20160310_a1/';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'b9';
% rootpath = '/mnt/data/Research/smr-inc/20160316_b9/';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'c4';
% rootpath = '/mnt/data/Research/smr-inc/20160324_c4/';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'c5';
% rootpath = '/mnt/data/Research/smr-inc/20160329_c5/';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'c6';
% rootpath = '/mnt/data/Research/smr-inc/20160330_c6/';
%%%%%%%%%%%%%%%%%%

pattern   = 'offline';
filenames = util_getfile(rootpath, '.gdf', pattern);    
NumFiles = length(filenames);

FilterFlag  = true;
FilterType  = 'bandpass';
FilterOrder = 4;
FilterBands = [8 30];

savepath = [pwd '/analysis/smr-inc/cspcoeff/'];

if (exist(savepath, 'dir') ~= 7)
    mkdir(savepath);
end

TaskCodes  = {783, [770 771 773]};
TaskLabels = {'Rest', 'Smr'};

D   = [];
typ = [];
dur = [];
pos = [];

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    [s, h] = sload(cfilename);
    
    s = s(:, 1:end-1);
    
    if FilterFlag == true
        disp(['[proc] - Apply pre-filtering (' FilterType '|' num2str(FilterOrder) '|' num2str(FilterBands(1)) '-' num2str(FilterBands(2)) 'Hz)']);
        s = filt_bp(s, FilterOrder, FilterBands, h.SampleRate);
    end
    
    ctyp  = h.EVENT.TYP;
    cpos  = h.EVENT.POS;
    cdur  = h.EVENT.DUR;
    
    typ = cat(1, typ, ctyp);
    dur = cat(1, dur, cdur);
    pos = cat(1, pos, cpos + size(D, 1));
    
    D = cat(1, D, s);
end

NumSamples  = size(D, 1);
NumChannels = size(D, 2);

%% Vector labels
event.TYP = typ;
event.POS = pos;
event.DUR = dur;

[Ck, CPos, CDur] = proc_getevents(event, NumSamples, 781);

% Events Trials & Events Class Vector
EventT = event.TYP(event.TYP == 770 |event.TYP == 771 | event.TYP == 773 | event.TYP == 774 | event.TYP == 783);
EventK = zeros(NumSamples, 1);      
for trId = 1:length(CPos)
    cstart = CPos(trId);
    cstop  = cstart + CDur(trId);
    EventK(cstart:cstop) = EventT(trId);
end

% Remapping Events Class Vector to Tasks of interest
TaskK = zeros(NumSamples, 1);
for gId = 1:length(TaskCodes)
    for tId = 1:length(TaskCodes{gId})
        ccode = TaskCodes{gId}(tId);
        TaskK(EventK == ccode) = gId;
    end
end

%% CSP
csp_coeff = filt_csp_dec(D(TaskK > 0, :), TaskK(TaskK > 0));

%% Saving coefficients
analysis.filter.flag  = FilterFlag;
analysis.filter.order = FilterOrder;
analysis.filter.bands = FilterBands;
analysis.csp.coeff    = csp_coeff;

filedate = char(regexp(rootpath, '/\d*_', 'match'));

sfilename = [savepath filedate(2:end-1) '.' subject '.'  pattern '.cspcoeff.mat'];
disp(['[out] - Saving csp coefficients in: ' sfilename]);
save(sfilename, 'analysis'); 