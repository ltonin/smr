function classifier = smr_inc_load_cnbismr_classifier(filename, NumFreqs, NumChannels, FreqGrid)

    [hdr, M, C] = wc_load_classifier(filename, 'cnbi-gaussian');
    
    model.type       = 'gau';
    model.map        = [2 2];
    model.shared     = hdr.shrdcov;
    model.traino     = 0;
    model.NumEp      = 5;
    model.M          = M;
    model.C          = C;
    model.class_labs = hdr.classlbs;

    idfeatures = [];
    for i = 1:hdr.nfeatures
        idfeatures = cat(1, idfeatures, sub2ind([NumFreqs NumChannels], find(FreqGrid == hdr.idfreq(i)), hdr.idchan(i)));
    end
    
    classifier.model      = model;
    classifier.features = idfeatures;
end