function inc = smr_inc_load_inc_analysis(filename)

    canalysis      = load(filename);
    
    inc.alpha      = canalysis.analysis.entropy.classification.integration;
    inc.rejection  = canalysis.analysis.entropy.classification.rejection;
    inc.pattern    = canalysis.analysis.entropy.processing.pattern;
    inc.coeff      = canalysis.analysis.entropy.processing.coefficients;
    
    inc.filename   = filename;
    inc.classifier.model    = canalysis.model;
    inc.classifier.features = canalysis.analysis.entropy.classification.idfeatures;
end
    
    