clear all; clc;

%%%%%%%%%%%%%%%%
subject          = 'b4';
rootpath         = '/mnt/data/Research/smr-inc/20160301_b4/';
smr.alpha        = 0.98;
smr.rejection    = 0.55;
smr.threshold    = 0.7;
fusion.type      = 'proportional';
fusion.threshold = 0.5;
fusion.alpha     = 0.97;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'a1';
% rootpath         = '/mnt/data/Research/smr-inc/20160310_a1/';
% smr.alpha        = 0.97;
% smr.rejection    = 0.55;
% smr.threshold    = 0.7;
% fusion.type      = 'proportional';
% fusion.threshold = 0.5;
% fusion.alpha     = 0.96;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'b9';
% rootpath         = '/mnt/data/Research/smr-inc/20160316_b9/';
% smr.alpha        = 0.97;
% smr.rejection    = 0.55;
% smr.threshold    = 0.7;
% fusion.type      = 'proportional';
% fusion.threshold = 0.5;
% fusion.alpha     = 0.96;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'c2';
% rootpath         = '/mnt/data/Research/smr-inc/20160321_c2/';
% smr.alpha        = 0.97;
% smr.rejection    = 0.55;
% smr.threshold    = 0.7;
% fusion.type      = 'proportional';
% fusion.threshold = 0.5;
% fusion.alpha     = 0.97;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'c4';
% rootpath         = '/mnt/data/Research/smr-inc/20160324_c4/';
% smr.alpha        = 0.97;
% smr.rejection    = 0.55;
% smr.threshold    = 0.7;
% fusion.type      = 'proportional';
% fusion.threshold = 0.5;
% fusion.alpha     = 0.95;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'c5';
% rootpath         = '/mnt/data/Research/smr-inc/20160329_c5/';
% smr.alpha        = 0.97;
% smr.rejection    = 0.55;
% smr.threshold    = 0.7;
% fusion.type      = 'proportional';
% fusion.threshold = 0.5;
% fusion.alpha     = 0.95;
%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%
% subject          = 'c6';
% rootpath         = '/mnt/data/Research/smr-inc/20160330_c6/';
% smr.alpha        = 0.97;
% smr.rejection    = 0.55;
% smr.threshold    = 0.7;
% fusion.type      = 'proportional';
% fusion.threshold = 0.5;
% fusion.alpha     = 0.955;
%%%%%%%%%%%%%%%%

debug_random = false;

pattern       = 'online*hfr';
psdpath       = 'analysis/smr-inc/psd/';
entpath       = 'analysis/smr-inc/entropy/log/';

psdfiles = util_getfile(psdpath, '.mat', [subject '*' pattern]);
entfiles = util_getfile(entpath, '.mat', [subject '*' pattern]);

smr.filename = util_getfile(rootpath, '.dat', [subject '_bhbf_*']); 
inc.filename = util_getfile(entpath,  '.mat', [subject '_entropy_classifier']); 

NumFiles  = length(psdfiles);

D         = [];
E         = [];
Rk        = [];
event.TYP = [];
event.POS = [];
event.DUR = [];

for fId = 1:NumFiles
    cpsdfile = psdfiles{fId};
    centfile = entfiles{fId};
    util_bdisp(['[io] - Loading PSD & Entropy data ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - PSD file:     ' cpsdfile]);
    disp(['[io] - Entropy file: ' centfile]);
    
    cpsd = load(cpsdfile);
    cent = load(centfile);
    
    ctyp  = cpsd.analysis.event.TYP;
    cpos  = cpsd.analysis.event.POS;
    cdur  = cpsd.analysis.event.DUR;
    
    event.TYP = cat(1, event.TYP, cpsd.analysis.event.TYP);
    event.DUR = cat(1, event.DUR, cpsd.analysis.event.DUR);
    event.POS = cat(1, event.POS, cpsd.analysis.event.POS + size(E, 1));
    
    Rk  = cat(1, Rk, fId*ones(size(cpsd.psd, 1), 1));
    D   = cat(1, D, cpsd.psd);
    E   = cat(1, E, cent.entropy);
    
    Freqs = cpsd.analysis.processing.f;
    Bands = cent.analysis.entropy.bands;     
end

FreqGrid = 4:2:48;
[~, FreqId] = intersect(Freqs, FreqGrid); 
D = D(:, FreqId, :);
NumSamples  = size(D, 1);
NumFreqs    = size(D, 2);
NumChannels = size(D, 3);
NumBands    = size(E, 2);

%% Vector labels
util_bdisp('[proc] - Creating vector labels for SMR and INC');
[~, CPos, CDur] = proc_getevents(event, size(D, 1), 781);
NumTrials = length(CPos);

TrialClass = event.TYP(event.TYP == 771 | event.TYP == 773 | event.TYP == 783);
IncClasses = [1 2];

Dk = zeros(NumSamples, 1);
Ek = zeros(NumSamples, 1);
for trId = 1:length(CPos)
    cstart = CPos(trId);
    cstop  = cstart + CDur(trId);
    cclass = TrialClass(trId);
    
    if(cclass ~= 783)
        % INC vector labels
        Ek(cstart:cstop) = IncClasses(1);
        % SMR vector labels
        Dk(cstart:cstop) = cclass;
    else
        Ek(cstart:cstop) = IncClasses(2);
    end
end


%% SMR classification
util_bdisp('[proc] - SMR classification:');
% Loading SMR classifier
disp('       - Loading SMR classifier:');
disp(['       - Filename: ' smr.filename{1}]);
smr.classifier = smr_inc_load_cnbismr_classifier(smr.filename{1}, NumFreqs, NumChannels, FreqGrid);

% SMR Feature extraction
disp('       - Extracting SMR features');
F = proc_reshape_ts_bc(log(D));

% Simulating online SMR classification
disp('       - Simulating online SMR classifier');
[pp.smr.raw, Gk.smr.raw] = classTest(smr.classifier.model, F(:, smr.classifier.features));

% Integrating raw SMR probabilities
disp('       - Integrating raw SMR probabilities');
pp.smr.integrated = pp.smr.raw;
Gk.smr.integrated = zeros(NumSamples, 1);
for sId = 2:NumSamples
    cpp = pp.smr.raw(sId, :);
    pdc = pp.smr.integrated(sId-1, :);
    
    if isempty(find(CPos == sId, 1)) == false
        pdc = [0.5 0.5];
    end
    
    if (max(cpp) > smr.rejection)
        pp.smr.integrated(sId, :) = pdc.*smr.alpha + cpp.*(1-smr.alpha);
    else
        pp.smr.integrated(sId, :) = pdc;
    end
    [~, cguess] = max(pp.smr.integrated(sId, :)); 
    Gk.smr.integrated(sId) = smr.classifier.model.class_labs(cguess);
end

disp('       - Computing SMR classification accuracy:');
kaccuracy.smr.raw        = kappa(Gk.smr.raw(Dk > 0), Dk(Dk > 0));
kaccuracy.smr.integrated = kappa(Gk.smr.integrated(Dk > 0), Dk(Dk > 0));
disp(['         => kappa value on raw probabilities:        ' num2str(kaccuracy.smr.raw.kappa, 3)]);
disp(['         => kappa value on integrated probabilities: ' num2str(kaccuracy.smr.integrated.kappa, 3)]);

%% INC classification
util_bdisp('[proc] - INC classification:');

% Loading Entropy classifier
disp('       - Loading INC classifier:');
disp(['       - Filename: ' inc.filename{1}]);
inc = smr_inc_load_inc_analysis(inc.filename{1});

% Feature extraction for Entropy
disp('       - Extracting Entropy features');
P = E;
if strcmpi(inc.pattern, 'csp')
    disp('       - Applying CSP coefficients');
    for bId = 1:NumBands
            cdata = squeeze(E(:, bId, :));
            P(:, bId, :) = filt_csp_apply(cdata, inc.coeff(:, :, bId), NumChannels);
    end
elseif strcmpi(inc.pattern, 'pca')
    disp('       - Applying PCA coefficients');
    for bId = 1:NumBands
            cdata = squeeze(E(:, bId, :));
            P(:, bId, :) = cdata*inc.coeff(:, :, bId);
    end
end
U = proc_reshape_ts_bc(P);

% Simulating online Entropy classifier
disp('       - Simulating online INC classifier');
[pp.inc.raw, Gk.inc.raw] = classTest(inc.classifier.model, U(:, inc.classifier.features));

if debug_random == true
    util_bdisp('************** RANDOM INC RAW PROBABILITIES ********************');
    randval = rand(NumSamples, 1);
    pp.inc.raw = [randval 1-randval];
    Gk.inc.raw = ones(NumSamples, 1);
    Gk.inc.raw(pp.inc.raw(:, 2) > 0.5) = 2;
end

% Integrating raw SMR probabilities
disp('       - Integrating raw SMR probabilities');
pp.inc.integrated = pp.inc.raw;
Gk.inc.integrated = zeros(NumSamples, 1);
for sId = 2:NumSamples
    cpp = pp.inc.raw(sId, :);
    pdc = pp.inc.integrated(sId-1, :);
    
    if isempty(find(CPos == sId, 1)) == false
        pdc = [0.5 0.5];
    end
        
    if (max(cpp) > inc.rejection)
        pp.inc.integrated(sId, :) = pdc.*inc.alpha + cpp.*(1-inc.alpha);
    else
        pp.inc.integrated(sId, :) = pdc;
    end
    [~, cguess] = max(pp.inc.integrated(sId, :)); 
    Gk.inc.integrated(sId) = inc.classifier.model.class_labs(cguess);
end
% pp.inc.integrated(Ek == 1, 1) = 1;
% pp.inc.integrated(Ek == 1, 2) = 0;
% pp.inc.integrated(Ek == 2, 1) = 0;
% pp.inc.integrated(Ek == 2, 2) = 1;

disp('       - Computing SMR classification accuracy:');
kaccuracy.inc.raw        = kappa(Gk.inc.raw(Ek > 0), Ek(Ek > 0));
kaccuracy.inc.integrated = kappa(Gk.inc.integrated(Ek > 0), Ek(Ek > 0));
disp(['         => kappa value on raw probabilities:        ' num2str(kaccuracy.inc.raw.kappa, 3)]);
disp(['         => kappa value on integrated probabilities: ' num2str(kaccuracy.inc.integrated.kappa, 3)]);

%% Fusion framework
util_bdisp('[proc] - SMR and INC fusion framework');
disp(['        - Fusion type: ' fusion.type]);
disp(['        - Fusion alpha: ' num2str(fusion.alpha)]);
disp(['        - Fusion threshold: ' num2str(fusion.threshold) ' for (thresholding)']);


pp.fusion.decision = pp.smr.raw;
for sId = 2:NumSamples
    curr_smr_pp    = pp.smr.raw(sId, :);
    prev_fusion_dc = pp.fusion.decision(sId-1, :);
    
    curr_inc_dc = pp.inc.integrated(sId, :);
    prev_inc_dc = pp.inc.integrated(sId-1, :);
    
    if isempty(find(CPos == sId, 1)) == false
        prev_fusion_dc = [0.5 0.5];
    end
    
    if strcmpi(fusion.type, 'thresholding')
        if (curr_inc_dc(2) >= fusion.threshold)
            pp.fusion.decision(sId, :) = prev_fusion_dc;
        else
            if (max(curr_smr_pp) > smr.rejection)
                pp.fusion.decision(sId, :) = prev_fusion_dc.*fusion.alpha + curr_smr_pp.*(1-fusion.alpha);
            else
                pp.fusion.decision(sId, :) = prev_fusion_dc;
            end
        end
    elseif strcmpi(fusion.type, 'proportional')
        if(curr_inc_dc(2) > 0.5)
            curr_alpha = proc_normalize(curr_inc_dc(2), [fusion.alpha 1], [0.5 1]);  
        else
            curr_alpha = fusion.alpha;
        end
        if (max(curr_smr_pp) > smr.rejection)
            pp.fusion.decision(sId, :) = prev_fusion_dc.*curr_alpha + curr_smr_pp.*(1-curr_alpha);
        else
            pp.fusion.decision(sId, :) = prev_fusion_dc;
        end
    elseif strcmpi(fusion.type, 'pid') 
        if (max(curr_smr_pp) > smr.rejection)
            pp.fusion.decision(sId, :) = dm_accumulation_pid(fusion.alpha, curr_smr_pp, [curr_inc_dc; prev_inc_dc], prev_fusion_dc);
        else
            pp.fusion.decision(sId, :) = prev_fusion_dc;
        end
    elseif strcmpi(fusion.type, 'pid2') 
        if (max(curr_smr_pp) > smr.rejection)
            pp.fusion.decision(sId, :) = dm_accumulation_pid2(fusion.alpha, curr_smr_pp, [curr_inc_dc; prev_inc_dc], prev_fusion_dc);
        else
            pp.fusion.decision(sId, :) = prev_fusion_dc;
        end
    else
        error('chk:fusion', 'Unrecognized fusion type');
    end
end


%% Computing Trial decision with and without fusion
util_bdisp('[proc] - Computing trial decisions with and without fusion');
decision.nofusion.value     = zeros(NumTrials, 1);
decision.nofusion.class     = zeros(NumTrials, 1);
decision.nofusion.delivered = zeros(NumTrials, 1);
decision.fusion.value       = zeros(NumTrials, 1);
decision.fusion.class       = zeros(NumTrials, 1);
decision.fusion.delivered   = zeros(NumTrials, 1);
for trId = 1:NumTrials
    cstart = CPos(trId);
    cstop  = cstart + CDur(trId);
    cclass = TrialClass(trId);
    
    % Values and class WITHOUT fusion for SMR trials (at the END of the trial)
    if(cclass ~= 783)
        end_nofusion_probs = pp.smr.integrated(cstop, :);
        [end_nofusion_pp, end_nofusion_class] = max(end_nofusion_probs);

        decision.nofusion.value(trId)     = end_nofusion_pp;
        decision.nofusion.class(trId)     = smr.classifier.model.class_labs(end_nofusion_class);
        decision.nofusion.delivered(trId) = 1;
    % Values and class WITHOUT fusion for INC trials (first probability over smr.threshold, if not NaN)
    elseif (cclass == 783)
        [overth_nofusion_sample, overth_nofusion_class] = find(pp.smr.integrated(cstart:cstop, :) >= smr.threshold, 1, 'first');
        if isempty(overth_nofusion_sample) == true
            decision.nofusion.value(trId)     = nan;
            decision.nofusion.class(trId)     = nan;
            decision.nofusion.delivered(trId) = 0;
        else
            decision.nofusion.value(trId)     = max(pp.smr.integrated(cstart + overth_nofusion_sample - 1, :));
            decision.nofusion.class(trId)     = smr.classifier.model.class_labs(overth_nofusion_class);
            decision.nofusion.delivered(trId) = 1;
        end
    end

    % Values and class WITH fusion for SMR trials (first probability over smr.threshold OR at the END of the trial)
    if(cclass ~= 783)
        [overth_fusion_sample, overth_fusion_class] = find(pp.fusion.decision(cstart:cstop, :) >= smr.threshold, 1, 'first');
        if isempty(overth_fusion_sample) == true
            end_fusion_probs = pp.fusion.decision(cstop, :);
            [end_fusion_pp, end_fusion_class] = max(end_fusion_probs);
            decision.fusion.value(trId) = end_fusion_pp;
            decision.fusion.class(trId) = smr.classifier.model.class_labs(end_fusion_class);
        else
            decision.fusion.value(trId)     = max(pp.fusion.decision(cstart + overth_fusion_sample - 1, :));
            decision.fusion.class(trId)     = smr.classifier.model.class_labs(overth_fusion_class);
        end
    % Values and class WITH fusion for INC trials (first probability over smr.threshold OR maximum in the trial)
    elseif (cclass == 783)
        [overth_fusion_sample, overth_fusion_class] = find(pp.fusion.decision(cstart:cstop, :) >= smr.threshold, 1, 'first');
        if isempty(overth_fusion_sample) == true
            [max_fusion_pp, max_fusion_class] = max(max(pp.fusion.decision(cstart:cstop, :)));
            decision.fusion.value(trId) = max_fusion_pp;
            decision.fusion.class(trId) = smr.classifier.model.class_labs(max_fusion_class);
        else
            decision.fusion.value(trId)     = max(pp.fusion.decision(cstart + overth_fusion_sample - 1, :));
            decision.fusion.class(trId)     = smr.classifier.model.class_labs(overth_fusion_class);
        end
    end
            
end


%% Computing Decision accuracy
util_bdisp('[out] - Computing decisions accuracy');
SmrTrial = TrialClass ~= 783;
IncTrial = TrialClass == 783;

decision.nofusion.smr.accuracy  = 100*sum((decision.nofusion.class(SmrTrial) == TrialClass(SmrTrial)) & decision.nofusion.delivered(SmrTrial))./sum(SmrTrial);
decision.nofusion.smr.meanvalue = mean(decision.nofusion.value(SmrTrial));
decision.nofusion.smr.stdvalue  = std(decision.nofusion.value(SmrTrial));
decision.nofusion.inc.delivered = 100*sum(decision.nofusion.delivered(IncTrial))./sum(IncTrial);

decision.fusion.smr.accuracy  = 100*sum((decision.fusion.class(SmrTrial) == TrialClass(SmrTrial)))./sum(SmrTrial);
decision.fusion.smr.meanvalue = mean(decision.fusion.value(SmrTrial));
decision.fusion.smr.stdvalue  = std(decision.fusion.value(SmrTrial));
decision.fusion.inc.delivered = 100*sum(decision.fusion.value(IncTrial) >= decision.fusion.smr.meanvalue)./sum(IncTrial);
%decision.fusion.inc.delivered = 100*sum(decision.fusion.value(IncTrial) >= smr.threshold)./sum(IncTrial);


%% Plotting
figure;
bar([decision.nofusion.smr.accuracy decision.fusion.smr.accuracy; decision.nofusion.inc.delivered decision.fusion.inc.delivered]);
set(gca, 'XTickLabel', {'Smr accuracy', 'Inc misclassification'});
ylabel('Percentage [%]');
legend('Without detector', 'With INC detector');
grid on;
ylim([0 100]);
title([subject ' accuracies - alpha fusion: ' num2str(fusion.alpha) ' - smr avg fusion: ' num2str(decision.fusion.smr.meanvalue, 3) '+/-' num2str(decision.fusion.smr.stdvalue, 2)]);