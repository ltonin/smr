clear all; clc;

% subject  = 'b4';
% pattern  = '';
% rootpath = '/mnt/data/Research/smr-inc/20160301_b4/';
% subject  = 'a1';
% pattern  = '';
% rootpath = '/mnt/data/Research/smr-inc/20160310_a1/';
subject  = 'b9';
pattern  = '';
rootpath = '/mnt/data/Research/smr-inc/20160316_b9/';
savepath = [pwd '/analysis/smr-inc/psd/'];

filenames = util_getfile(rootpath, '.gdf', pattern);    
load('lapmask_16ch');
NumFiles = length(filenames);

BufferLength = 1;         % seconds
FrameSize    = 32;        % samples
WinLength    = 0.5;       % seconds

if (exist(savepath, 'dir') ~= 7)
    mkdir(savepath);
end

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    
    [s, h] = sload(cfilename);
    
    s = s(:, 1:end-1);
    
    NumSamples   = size(s, 1);
    NumChannels  = size(s, 2);
    BufferSize   = floor(BufferLength*h.SampleRate);
    WinSize      = floor(WinLength*h.SampleRate);
    WinOverlap   = floor(WinSize/2);
    
    BufStart    = 1:FrameSize:(NumSamples - BufferSize);
    BufStop     = BufStart + BufferSize - 1;
    NumBuffers  = length(BufStart); 
    
    [NFFT, NumFreqs] = proc_getNFFT(WinSize);
    psd = zeros(NumBuffers, NumFreqs, NumChannels);
    
    disp( '[proc] - Computing psd:');
    disp(['       - SampleRate:         ' num2str(h.SampleRate)  '  [Hz]']);
    disp(['       - FrameSize:          ' num2str(FrameSize)     '  [sample]']);
    disp(['       - BufferLength:       ' num2str(BufferLength)  '  [s]']);
    disp(['       - PSD WinSize:        ' num2str(WinSize)       '  [samples]']);
    disp(['       - PSD WinOverlap:     ' num2str(WinOverlap)    '  [s]']);
    disp( '       - Type:               pwelch');
    disp( '       - WinType:            hamming' );
    disp(['       - NFFT:               ' num2str(NFFT)]);
    
    disp('[proc] - Processing each frame:');
    for bId = 1:NumBuffers
        util_disp_progress(bId, NumBuffers);
        cstart = BufStart(bId);
        cstop  = BufStop(bId);

        cbuffer = s(cstart:cstop, :);

        cbuffer = cbuffer*lapmask;

        for chId = 1:NumChannels 
            [psd(bId, :, chId), f] = pwelch(cbuffer(:, chId), WinSize, WinOverlap, NFFT, h.SampleRate);     
        end
    end
    
    analysis.buffer.BufferSize  = BufferSize;
    analysis.buffer.FrameSize   = FrameSize;
    analysis.buffer.SampleRate  = h.SampleRate;
    
    analysis.event.typ = h.EVENT.TYP;
    analysis.event.pos = floor(h.EVENT.POS/FrameSize);
    analysis.event.dur = floor(h.EVENT.DUR/FrameSize);
    
    analysis.proc.type       = 'pwelch';
    analysis.proc.WinSize    = WinSize;
    analysis.proc.WinOverlap = WinOverlap;
    analysis.proc.WinType    = 'hamming';
    analysis.proc.NFFT       = NFFT;
    analysis.proc.freqs      = f;

    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    disp(['[out] - Saving psd in: ' sfilename]);
    save(sfilename, 'psd', 'analysis'); 
end