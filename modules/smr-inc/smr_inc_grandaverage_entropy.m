clc;
clearvars;
close all;

subject  = 'AN14VE'; %'b4';
pattern  = 'offline*rfsb'; %'offline*hfr';
entropypath = 'analysis/smr-inc/entropy/log/';

filenames = util_getfile(entropypath, '.mat', [subject '*' pattern]);
NumFiles  = length(filenames);


MinPeriod   = 4; % seconds
alpha       = 0.05;

E         = [];
event.TYP = [];
event.POS = [];
event.DUR = [];

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading psd data ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    cdata = load(cfilename);
    ctyp  = cdata.analysis.event.TYP;
    cpos  = cdata.analysis.event.POS;
    cdur  = cdata.analysis.event.DUR;
    
    event.TYP = cat(1, event.TYP, cdata.analysis.event.TYP);
    event.DUR = cat(1, event.DUR, cdata.analysis.event.DUR);
    event.POS = cat(1, event.POS, cdata.analysis.event.POS + size(E, 1));

    E = cat(1, E, cdata.entropy);
    
    SampleRate  = cdata.analysis.processing.fs;
    Freqs       = cdata.analysis.processing.f;
    FrameSize   = cdata.analysis.processing.fsize;
end

NumSamples  = floor(MinPeriod*SampleRate/FrameSize);
NumBands    = size(E, 2);
NumChannels = size(E, 3);

[Ck, CPos, CDur] = proc_getevents(event, size(E, 1), 781, NumSamples);
NumTrials = length(CPos);

CueTyp = event.TYP(event.TYP == 770 | event.TYP == 771 | event.TYP == 774 | event.TYP == 783);
%CueTyp = event.TYP(event.TYP == 771 | event.TYP == 773 | event.TYP == 783);
IncTyp = CueTyp == 783;
SmrTyp = CueTyp == 770;
%SmrTyp = ~IncTyp;

P = zeros(NumSamples, NumBands, NumChannels, NumTrials);
for trId = 1:NumTrials
    cstart = CPos(trId);
    cstop  = cstart + CDur(trId) - 1;
    
    P(:, :, :, trId) = E(cstart:cstop, :, :);
end


B = repmat(mean(P(1:16, :, :, :), 1), [size(P, 1) 1 1 1]);
T = P./B;
%T = P;


M = zeros(size(P, 1), size(P, 2), size(P, 3));

for chId = 1:NumChannels
    for bId = 1:NumBands
        for sId = 1:NumSamples
            cdata = squeeze(T(sId, bId, chId, :));
            M(sId, bId, chId) = ttest2(cdata(SmrTyp), cdata(IncTyp), 'alpha', alpha);
        end
    end
end

%% Plotting

ChanLayout = [0 0 1 0 0; 2 3 4 5 6; 7 8 9 10 11; 12 13 14 15 16];
ChanLabels = {'Fz', 'Fc3', 'Fc1', 'Fcz', 'Fc2', 'Fc4', 'C3', 'C1', 'Cz', 'C2', 'C4', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4'};

figure;
t = 0:FrameSize/SampleRate:MinPeriod - FrameSize/SampleRate;
plot_subimagesc(t, 1:NumBands, M, 'layout', ChanLayout, 'subtitles', ChanLabels, 'xlabel', 'time [s]', 'ylabel', 'bands');


f = figure;
set(f,'name', 'Entropy of alpha band with increasing window','numbertitle','off');
fig_set_position(f, 'All');

SelBand = 4; %alpha
t = 0:FrameSize/SampleRate:MinPeriod - FrameSize/SampleRate;
NumRows = 4;
NumCols = 5;
for chId = 1:NumChannels
    [cx, cy] = find(ChanLayout == chId); 
    index = (cx-1)*NumCols + cy;
    subplot(NumRows, NumCols, index);
    
    cdata = squeeze(T(:, SelBand, chId, :));
    
    for eWId = 1:size(cdata, 1)
        [h(eWId, chId), p(eWId, chId)] = ttest2(cdata(eWId, SmrTyp), cdata(eWId, IncTyp), 'alpha', alpha);
    end
    
    
    msmr = mean(cdata(:, SmrTyp), 2);
    ssmr = std(cdata(:, SmrTyp), [], 2);
    minc = mean(cdata(:, IncTyp), 2);
    sinc = std(cdata(:, IncTyp), [], 2);
    
    hold on;
    plot_boundedline(t, msmr, ssmr, 'b', 'alpha');
    plot_boundedline(t, minc, sinc, 'r', 'alpha');
    plot(t, h(:, chId)*5, 'k');
    hold off;
    grid on;
    title(ChanLabels{chId});
    ylim([3 5]);
    xlim([1 MinPeriod]);
    xlabel('Time [s]');
    ylabel('Amplitude [uV]');
    
 
end

figure;
SelChan = 12;
cdata = squeeze(P(:, SelBand, SelChan, :));
for eWId = 1:size(cdata, 1)
    [h_sel(eWId), p_sel] = ttest2(cdata(eWId, SmrTyp), cdata(eWId, IncTyp), 'alpha', alpha);
end
msmr = mean(cdata(:, SmrTyp), 2);
ssmr = std(cdata(:, SmrTyp), [], 2);
minc = mean(cdata(:, IncTyp), 2);
sinc = std(cdata(:, IncTyp), [], 2);

hold on;
plot_boundedline(t, msmr, ssmr, 'b', 'alpha');
plot_boundedline(t, minc, sinc, 'r', 'alpha');
plot(t, h_sel*5, 'k');
hold off;
grid on;
title(ChanLabels{chId});
ylim([1 6]);
xlim([0 MinPeriod]);
xlabel('Time [s]');
ylabel('Amplitude [uV]'); 

