clearvars; clc;

%%%%%%%%%%%%%%%%%%
% subject  = 'AN14VE';
% rootpath = '/mnt/data/Research/smr-inc/20160624_AN14VE/';
% csppath  = './analysis/smr-inc/cspcoeff/20160624.AN14VE.offline.cspcoeff.mat';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'b4';
% rootpath = '/mnt/data/Research/smr-inc/20160301_b4/';
% csppath  = './analysis/smr-inc/cspcoeff/20160301.b4.offline.cspcoeff.mat';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
subject  = 'a1';
rootpath = '/mnt/data/Research/smr-inc/20160310_a1/';
csppath  = './analysis/smr-inc/cspcoeff/20160310.a1.offline.cspcoeff.mat';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'b9';
% rootpath = '/mnt/data/Research/smr-inc/20160316_b9/';
% csppath  = './analysis/smr-inc/cspcoeff/20160316.b9.offline.cspcoeff.mat';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'c4';
% rootpath = '/mnt/data/Research/smr-inc/20160324_c4/';
% csppath  = './analysis/smr-inc/cspcoeff/20160324.c4.offline.cspcoeff.mat';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'c5';
% rootpath = '/mnt/data/Research/smr-inc/20160329_c5/';
% csppath  = './analysis/smr-inc/cspcoeff/20160329.c5.offline.cspcoeff.mat';
%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
% subject  = 'c6';
% rootpath = '/mnt/data/Research/smr-inc/20160330_c6/';
% csppath  = './analysis/smr-inc/cspcoeff/20160330.c6.offline.cspcoeff.mat';
%%%%%%%%%%%%%%%%%%


filenames = util_getfile(rootpath, '.gdf', '');    
NumFiles = length(filenames);

FilterFlag  = true;
FilterType  = 'bandpass';
FilterOrder = 4;
FilterBands = [8 30];

SpatialFlag   = true;
SpatialType   = 'csp';
LaplacianPath = './lapmask_16ch.mat';

BufferSize = 1;         % seconds
FrameSize  = 0.0625;    % seconds
PSDWinSize = 0.5;       % => BufferSize*PSDWinSize = 0.5  seconds;
PSDWinOvl  = 0.5;       % => PSDWinSize*PSDWinOvl  = 0.25 seconds;

savepath = [pwd '/analysis/smr-inc/psd/' SpatialType '/'];

if (exist(savepath, 'dir') ~= 7)
    mkdir(savepath);
end

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    [s, h] = sload(cfilename);
    
    s = s(:, 1:end-1);
    
    if FilterFlag == true
        disp(['[proc] - Apply pre-filtering (' FilterType '|' num2str(FilterOrder) '|' num2str(FilterBands(1)) '-' num2str(FilterBands(2)) 'Hz)']);
        s = filt_bp(s, FilterOrder, FilterBands, h.SampleRate);
    end
    
    
    disp(['[proc] - Apply spatial filter (' SpatialType '):']);
    switch(SpatialType)
        case 'laplacian'
            load(LaplacianPath);
            LaplacianMask = lapmask;
            ss = s*LaplacianMask;
        case 'car'
            ss = proc_car(s);
        case 'csp'
            load(csppath);
            ss = filt_csp_apply(s, analysis.csp.coeff, size(s, 2));
        case 'none'
            ss = s;
        otherwise
            error(['[proc] - ' SpatialType ': Unknown filter type']);
    end
              
    [psd, settings] = proc_simonline_psd(ss, h.SampleRate, FrameSize, BufferSize, PSDWinSize, PSDWinOvl);
    
    analysis.filter.flag  = FilterFlag;
    analysis.filter.order = FilterOrder;
    analysis.filter.bands = FilterBands;
    analysis.spatial.type = SpatialType;
    analysis.processing   = settings;
    analysis.event.TYP    = h.EVENT.TYP;
    analysis.event.POS    = floor(h.EVENT.POS/settings.fsize);
    analysis.event.DUR    = floor(h.EVENT.DUR/settings.fsize);
    
    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    disp(['[out] - Saving psd in: ' sfilename]);
    save(sfilename, 'psd', 'analysis'); 
end