clearvars; clc;

%%%%%%%%%%%%%%%%%%
subject  = 'AN14VE';
rootpath = '/mnt/data/Research/smr-inc/20160624_AN14VE/';
%%%%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%
% subject  = 'b4';
% rootpath = '/mnt/data/Research/smr-inc/20160301_b4/';
% %%%%%%%%%%%%%%%%

% %%%%%%%%%%%%%%%%
% subject  = 'b9';
% rootpath = '/mnt/data/Research/smr-inc/20160331_c7/';
% %%%%%%%%%%%%%%%%

filenames = util_getfile(rootpath, '.gdf', 'offline');    
NumFiles = length(filenames);

load('lapmask_16ch');

FilterOrder = 4;
FilterBands = [8 30];

D  = [];
Rk = [];
TYP = [];
POS = [];
DUR = [];

%% Filtering and concatanion
for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    [s, h] = sload(cfilename);
    
    s = s(:, 1:end-1);
    
    % Event extraction
    ctyp = h.EVENT.TYP;
    cpos = h.EVENT.POS + size(D, 1);
    cdur = h.EVENT.DUR;
    
    cL = s*lapmask;
    
    % Data, vectors and event concatanation
    D  = cat(1, D, cL`);
    Rk = cat(1, Rk, fId*ones(size(s, 1), 1));
    TYP = cat(1, TYP, ctyp);
    POS = cat(1, POS, cpos);
    DUR = cat(1, DUR, cdur);  
end

NumSamples  = size(D, 1);
NumChannels = size(D, 2);

%% Buffering
BufferLength = 1;
FrameLength  = 0.0625;

BufferSize = floor(BufferLength*h.SampleRate);
FrameSize  = floor(FrameLength*h.SampleRate);
FrameStart = 1:FrameSize:(NumSamples - FrameSize);
FrameStop  = FrameStart + FrameSize - 1;
NumFrames  = length(FrameStart);

rbuffer = proc_ringbuffer_init(BufferSize, NumChannels);

C = zeros(NumFrames, NumChannels, NumChannels);

fprintf('[proc] - Processing each frame:\n');
for frId = 1:NumFrames
    util_disp_progress(frId, NumFrames, '        ');
    cstart = FrameStart(frId);
    cstop  = FrameStop(frId);
    cframe = D(cstart:cstop, :);
    
    [rbuffer, isfull] = proc_ringbuffer_add(rbuffer, cframe);
        
    if isfull == false
        continue;
    end
    
    % Bandpass filtering
    fbuffer = filt_bp(rbuffer, FilterOrder, FilterBands, h.SampleRate);
    
    C(frId, :, :) = corrcoef(fbuffer);
end

%% Event vector generation
event.TYP  = TYP;
event.POS  = floor(POS/FrameSize);
event.DUR  = floor(DUR/FrameSize);

[Ck, CPos, CDur] = proc_getevents(event, NumFrames, 781);

% Events Trials & Events Class Vector
EventT = event.TYP(event.TYP == 770 |event.TYP == 771 | event.TYP == 773 | event.TYP == 774 | event.TYP == 783);
EventK = zeros(NumSamples, 1);      
NumTrials = length(EventT);

for trId = 1:NumTrials
    cstart = CPos(trId);
    cstop  = cstart + CDur(trId) - 1;
    EventK(cstart:cstop) = EventT(trId);
end

% Remapping Events Class Vector to Tasks of interest
TaskCodes  = {783, [770 771 773]};
TaskLabels = {'Rest', 'Smr'};
TaskK = zeros(NumFrames, 1);
for gId = 1:length(TaskCodes)
    for tId = 1:length(TaskCodes{gId})
        ccode = TaskCodes{gId}(tId);
        TaskK(EventK == ccode) = gId;
    end
end




%% Tmp plots
subplot(1, 3, 1);
imagesc(squeeze(mean(C(TaskK == 1, :, :), 1)));
plot_setCLim(gca, [0 1]);
title('Rest');

subplot(1, 3, 2);
imagesc(squeeze(mean(C(TaskK == 2, :, :), 1)));
plot_setCLim(gca, [0 1]);
title('Motor imagery');

subplot(1, 3, 3);
imagesc(squeeze(mean(C(TaskK == 1, :, :), 1)) - squeeze(mean(C(TaskK == 2, :, :), 1)));
title('Difference');
colorbar

figure;
Channels = [9 11];
hold on;
plot(mean(C(TaskK == 1, Channels(1), :), 3), mean(C(TaskK == 1, Channels(2), :), 3), 'gx');
plot(mean(C(TaskK == 2, Channels(1), :), 3), mean(C(TaskK == 2, Channels(2), :), 3), '.');
hold off;


% %% Extracting trials
% S = zeros(TrialSize, NumChannels, NumTrials);
% Sk = zeros(NumTrials, 1);
% for trId = 1:NumTrials
%     cstart = CPos(trId);
%     cstop = cstart + TrialSize - 1;
%     S(:, :, trId) = D(cstart:cstop, :);
%     Sk(trId) = unique(TaskK(cstart:cstop));
% end
% 
% %% Computing coeherence
% C = zeros(NumChannels, NumChannels, NumTrials);
% for trId = 1:NumTrials
%    cs = S(:, :, trId);
%    C(:, :, trId) = corrcoef(cs);
% end
% 
% subplot(1, 2, 1);
% imagesc(mean(C(:, :, Sk == 1), 3) - mean(C(:, :, Sk == 2), 3));
% colorbar;
% 
% subplot(1, 2, 2);
% E = squeeze(mean(C, 1));
% bar(mean(E(:, Sk == 1), 2) - mean(E(:, Sk == 2), 2));
