%clear all; clc;

%subject  = 'B02';
pattern  = '';
rootpath = '/mnt/data/Research/bcicompetition-iv/2b/';
savepath = 'analysis/bcicompetition-iv/';


[ret, msg] = mkdir(pwd, savepath);
if (ret == true)
    util_bdisp(['[proc] - Processed data will be saved in ' savepath]);
else
    disp(msg);
end

filenames   = util_getfile(rootpath, '.gdf', [subject '*' pattern]);
NumFiles    = length(filenames);

BufferSize = 1;         % seconds
FrameSize  = 0.1250;    % seconds
PSDWinSize = 0.5;       % => BufferSize*PSDWinSize = 0.5  seconds;
PSDWinOvl  = 0.5;       % => PSDWinSize*PSDWinOvl  = 0.25 seconds;

ChannelIds  = 1:3;
NumChannels = length(ChannelIds);

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);

    [s, h] = sload(cfilename);

    s = s(:, ChannelIds);
    s(isnan(s)) = 0;
    
    [psd, settings] = proc_simonline_psd(s, h.SampleRate, FrameSize, BufferSize, 'car', PSDWinSize, PSDWinOvl);
    
    analysis.processing = settings;
    analysis.event.TYP  = h.EVENT.TYP;
    analysis.event.POS  = floor(h.EVENT.POS/settings.fsize);
    analysis.event.DUR  = floor(h.EVENT.DUR/settings.fsize);
    
    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    disp(['[out] - Saving psd in: ' sfilename]);
    save(sfilename, 'psd', 'analysis'); 

end

