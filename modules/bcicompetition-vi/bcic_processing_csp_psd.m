%clear all; clc;

%subject  = 'B02';
pattern  = '';
rootpath = '/mnt/data/Research/bcicompetition-iv/2b/';
savepath = 'analysis/bcicompetition-vi/';

try
    load([savepath subject '_csp.mat']);
    disp(['Loaded csp coefficient from ' savepath subject '_csp.mat']);
catch
    error(['Can not load csp coefficient. Check path or run bcic_processing_csp.m for subject ' subject]);
end

[ret, msg] = mkdir(pwd, savepath);
if (ret == true)
    util_bdisp(['[proc] - Processed data will be saved in ' savepath]);
else
    disp(msg);
end

filenames   = util_getfile(rootpath, '.gdf', [subject '*' pattern]);
NumFiles    = length(filenames);

BufferLength = 1;         % seconds
FrameSize    = 50;        % samples
Freqs        = 0:2:64;
ChannelIds  = 1:3;
NumChannels = length(ChannelIds);

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);

    [s, h] = sload(cfilename);

    s = s(:, ChannelIds);
    s(isnan(s)) = 0;
    
    bs = filt_bp(s, 4, [5 35], h.SampleRate);
    
    NumSamples   = size(bs, 1);
    NumChannels  = size(bs, 2);
    BufferSize   = floor(BufferLength*h.SampleRate);
    
    BufStart    = 1:FrameSize:(NumSamples - BufferSize);
    BufStop     = BufStart + BufferSize - 1;
    NumBuffers  = length(BufStart); 
    [NFFT, NumFreqs] = proc_getNFFT(BufferSize);
    psd = zeros(NumBuffers, NumFreqs, NumChannels);
    
    disp( '[proc] - Computing psd:');
    disp(['       - SampleRate:         ' num2str(h.SampleRate)  '  [Hz]']);
    disp(['       - FrameSize:          ' num2str(FrameSize)     '  [sample]']);
    disp(['       - BufferLength:       ' num2str(BufferLength)  '  [s]']);
    disp( '       - Type:               periodogram');
    disp( '       - WinType:            hamming' );
    
    disp('[proc] - Processing each frame:');
    for bId = 1:NumBuffers
        util_disp_progress(bId, NumBuffers);
       cstart = BufStart(bId);
       cstop  = BufStop(bId);
       
       cbuffer = bs(cstart:cstop, :);
       
       cbuffer = filt_csp_apply(cbuffer, csp.coeff, NumChannels);
       
       for chId = 1:NumChannels 
            [psd(bId, :, chId), f] = periodogram(cbuffer(:, chId), hamming(size(cbuffer,1)), NFFT, h.SampleRate);     
       end
        
    end
    
    analysis.buffer.BufferSize  = BufferSize;
    analysis.buffer.FrameSize   = FrameSize;
    analysis.buffer.SampleRate  = h.SampleRate;
    
    analysis.event.typ = h.EVENT.TYP;
    analysis.event.pos = floor(h.EVENT.POS/FrameSize);
    analysis.event.dur = floor(h.EVENT.DUR/FrameSize);
    
    analysis.proc.type       = 'periodogram';
    analysis.proc.WinType    = 'hamming';
    analysis.proc.freqs      = f;

    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    disp(['[out] - Saving psd in: ' sfilename]);
    save(sfilename, 'psd', 'analysis'); 

end

