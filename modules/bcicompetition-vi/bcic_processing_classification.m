clear all; clc;

subject  = 'B09';
pattern  = '';
rootpath = '/mnt/data/Research/bcicompetition-iv/2b/';
savepath = 'analysis/bcicompetition-iv/';
trlbpath = [rootpath 'truelabels/'];

[ret, msg] = mkdir(pwd, savepath);
if (ret == true)
    util_bdisp(['[proc] - Processed data will be saved in ' savepath]);
else
    disp(msg);
end

filenames = util_getfile(savepath, '.mat', [subject '*0']);
trlbfiles = util_getfile(trlbpath, '.mat', [subject '*' pattern]);
NumFiles  = length(filenames);
NumTLFiles = length(trlbfiles);

Period      = 4;
Classes     = [769 770];
NumClasses  = length(Classes);
NumFeatures = 15;

P          = [];
Rk         = [];
TYP        = [];
POS        = [];
DUR        = [];
TrueTrialLb = [];
SampleRate = [];
%% Loading data
for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    cdata = load(cfilename);
    P = cat(1, P, cdata.psd);
    
    TYP = cat(1, TYP, cdata.analysis.event.typ);
    DUR = cat(1, DUR, cdata.analysis.event.dur);
    POS = cat(1, POS, cdata.analysis.event.pos + size(P, 1));
    
    Rk  = cat(1, Rk, fId*ones(size(cdata.psd, 1), 1));
    
    SampleRate  = cdata.analysis.buffer.SampleRate;
    Freqs       = cdata.analysis.proc.freqs;
    FrameSize   = cdata.analysis.buffer.FrameSize;
end   

%% Creating vector labels
SmrLb  = false(size(P, 1), 1);
RejLb  = false(size(P, 1), 1);
SmrPos = [];
SmrDur = [];
Mk     = [];
for fId = 1:NumFiles
    cfilename = filenames{fId};

    cdata = load(cfilename);
    cevent.TYP  = cdata.analysis.event.typ;  
    cevent.DUR  = cdata.analysis.event.dur;
    cevent.POS  = (find(Rk == fId, 1) - 1) + cdata.analysis.event.pos;
    
    if(isempty(regexp(cfilename, '[T]', 'ONCE')) == false)
        cmod = 0;
        for cId = 1:NumClasses
            [csmrlb, csmrpos, csmrdur]  = proc_getevents(cevent, size(P, 1), Classes(cId), floor(Period*SampleRate/FrameSize));
            SmrLb  = SmrLb | csmrlb;
            SmrPos = cat(1, SmrPos, csmrpos);
            SmrDur = cat(1, SmrDur, csmrdur);
        end  
    elseif(isempty(regexp(cfilename, '[E]', 'ONCE')) == false)
        cmod = 1;
        [csmrlb, csmrpos, csmrdur] = proc_getevents(cevent, size(P, 1), 781);
        SmrLb  = SmrLb | csmrlb;
        SmrPos = cat(1, SmrPos, csmrpos);
        SmrDur = cat(1, SmrDur, csmrdur);
    end
    
    [SmrPos, idx] = sort(SmrPos);
    SmrDur = SmrDur(idx);
    
    RejLb = RejLb | proc_getevents(cevent, size(P, 1), 1023);
    
    Mk  = cat(1, Mk, cmod*ones(size(cdata.psd, 1), 1));
end

for flId = 1:NumTLFiles
    ctlfilename = trlbfiles{flId};
    disp(['[io] - Loading True Label filename ' num2str(flId) '/' num2str(NumTLFiles)]);
    disp(['       File: ' ctlfilename]);
    ctrlb = load(ctlfilename);
    TrueTrialLb = cat(1, TrueTrialLb, ctrlb.classlabel);
end

TrueLb = zeros(size(P, 1), 1);
for evtId = 1:length(SmrPos)
    cstart = SmrPos(evtId);
    cstop  = cstart + SmrDur(evtId);
    TrueLb(cstart:cstop) = TrueTrialLb(evtId);
end

TrainLb = SmrLb & RejLb == 0 & Mk == 0;
EvalLb  = SmrLb & RejLb == 0 & Mk == 1;

%% Features creation
lP = log(1 + P(:, 5:35, :));



F = proc_reshape_ts_bc(lP);
Fk = TrueLb;

%% Feature selection
% dp = cva_tun_opt(F(TrainLb, :), Fk(TrainLb));
% dp_r = reshape(dp, [size(P, 2) size(P, 3)]);

[dp, idfeatures] = proc_cva_selection(F(TrainLb, :), Fk(TrainLb), Rk(TrainLb), 15, 'draw', [size(lP, 2) size(lP, 3)]);

% [sort_dp, sort_id] = sort(dp, 'descend');
% idfeatures = sort(sort_id(1:NumFeatures));

%% Classification
disp('[proc] - Training classifier on Training data');
ClassifierType = 'lda'; % 'lda'; 'qda'; 'knn'; 'gau'
model = classTrain(F(TrainLb, idfeatures), Fk(TrainLb), ClassifierType);

disp('[proc] - Testing classifier on all data');
[pp, Gk] = classTest(model, F(:, idfeatures)); 

disp(['[proc] - Average accuracy on Training data: ' num2str(mean(diag(100*2*confusionmat(Fk(TrainLb), Gk(TrainLb))/sum(TrainLb)))) '%'])
disp(['[proc] - Average accuracy on Testing data: ' num2str(mean(diag(100*2*confusionmat(Fk(EvalLb), Gk(EvalLb))/sum(EvalLb)))) '%'])


kstr_train = kappa(Gk(TrainLb), Fk(TrainLb));
disp(['[proc] - Kappa value on Training data: ' num2str(kstr_train.kappa)]);

kstr_eval = kappa(Gk(EvalLb), Fk(EvalLb));
disp(['[proc] - Kappa value on Testing data: ' num2str(kstr_eval.kappa)]);


