%clear all; clc;

%subject  = 'B02';
pattern  = 'T';
rootpath = '/mnt/data/Research/bcicompetition-iv/2b/';
savepath = 'analysis/bcicompetition-vi/';


[ret, msg] = mkdir(pwd, savepath);
if (ret == true)
    util_bdisp(['[proc] - Processed data will be saved in ' savepath]);
else
    disp(msg);
end

filenames   = util_getfile(rootpath, '.gdf', [subject '*' pattern]);
NumFiles    = length(filenames);

ChannelIds  = 1:3;
NumChannels = length(ChannelIds);
Period      = 4;
Classes     = [769 770];
NumClasses  = length(Classes);

s          = [];
TYP        = [];
POS        = [];
DUR        = [];
SampleRate = [];
for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);

    [cdata, cheader] = sload(cfilename);

    cdata = cdata(:, ChannelIds);
        
    TYP = cat(1, TYP, cheader.EVENT.TYP);
    DUR = cat(1, DUR, cheader.EVENT.DUR);
    POS = cat(1, POS, cheader.EVENT.POS + size(s, 1));
    
    s = cat(1, s, cdata);
    SampleRate = cat(1, SampleRate, cheader.SampleRate);
end

s(isnan(s)) = 0;

SampleRate = unique(SampleRate);
if(length(SampleRate) ~= 1)
    error('ch:proc', 'Different sample rates in the files');
end

event.TYP = TYP;
event.POS = POS;
event.DUR = DUR;

RejLb = proc_getevents(event, size(s, 1), 1023);

TrainLb = false(size(s, 1), 1);
ClassLb = zeros(size(s, 1), 1);
for cId = 1:NumClasses
    clabel  = proc_getevents(event, size(s, 1), Classes(cId), floor(Period*SampleRate));
    TrainLb = TrainLb | clabel;
    ClassLb = ClassLb + cId*clabel;
end

TrainLb = TrainLb & RejLb == 0;

%% Band pass data between [5 35 Hz]
disp('[proc] - Bandpassing data between [5 35] Hz');
bs = filt_bp(s, 4, [5 35], SampleRate);

disp('[proc] - Computing CSP coefficients');
csp.coeff = filt_csp_dec(bs(TrainLb, :), ClassLb(TrainLb));

sfilename = [savepath '/' subject '_csp.mat'];
disp(['[out] - Saving csp in: ' sfilename]);
save(sfilename, 'csp'); 