function req_task = smr_get_task(taskid)

    tasks = std_tasks;

    if nargin < 1
        req_task = tasks;
    else
        if ischar(taskid)
            for tId = 1:length(tasks)
                if (strcmp(taskid, tasks(tId).id))
                    req_task = tasks(tId);
                    break;
                end
            end
        else
            for tId = 1:length(tasks)
                if (taskid == tasks(tId).event)
                    req_task = tasks(tId);
                    break;
                end
            end
        end
                    
        if(isempty(req_task))
            error('chk:tsk', ['No task found with key: ' taskid])
        end
    end
end


function tasks = std_tasks

    tasks(1).id         = 'mi_hand_left';
    tasks(1).desc       = 'Left_Hand_MI';
    tasks(1).event      = 769;
    tasks(1).hexevent   = '301';
    
    tasks(2).id         = 'mi_hand_right';
    tasks(2).desc       = 'Right_Hand_MI';
    tasks(2).event      = 770;
    tasks(2).hexevent   = '302';
    
    tasks(3).id         = 'mi_both_feet';
    tasks(3).desc       = 'Both_Feet_MI';
    tasks(3).event      = 771;
    tasks(3).hexevent   = '303';
    
    tasks(5).id         = 'mi_tongue';
    tasks(5).desc       = 'Tongue_MI';
    tasks(5).event      = 772;
    tasks(5).hexevent   = '304';
    
    tasks(4).id         = 'mi_both_hands';
    tasks(4).desc       = 'Both_Hands_MI';
    tasks(4).event      = 773;
    tasks(4).hexevent   = '305';
    
    tasks(6).id         = 'mi_rest';
    tasks(6).desc       = 'Rest_MI';
    tasks(6).event      = 783;
    tasks(6).hexevent   = '30f';

end