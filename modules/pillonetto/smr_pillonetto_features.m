clc;
clear all;
close all;

subject = 'a9';
root    = '/mnt/data/Research/smr/';
subfolder = '/analysis/';
dataroot  = util_getdir(root, subject);
filenames = util_getfile(cellfun(@cat, repmat({2}, 1, length(dataroot)), dataroot(:)', repmat({subfolder}, 1, length(dataroot)), 'UniformOutput', 0), '.mat');

NumFiles  = length(filenames);

FreqGrid    = 4:2:48; 
NumFreqs    = length(FreqGrid);
NumChans    = 16;
SampleRate  = 512;
WinStep     = 32;
NumFeatures = 5;
ModalityRun = [0 0 0 1 1 1 1];

EVENT_LEFT_HAND  = 769;
EVENT_RIGHT_HAND = 770;
EVENT_BOTH_FEET  = 771;
EVENT_LABELS{769} = 'lefthand';
EVENT_LABELS{770} = 'righthand';
EVENT_LABELS{771} = 'bothfeet';

EVENT_OF_INTEREST = [EVENT_LEFT_HAND EVENT_BOTH_FEET];
NumClasses = length(EVENT_OF_INTEREST);

SMR_PERIOD = [1 4];                                     % In seconds with respect to the cue
SMR_POS    = floor(SMR_PERIOD*SampleRate/WinStep);      % In samples with respect to the cue

P  = [];
Pk = [];
Rk = [];
Mk = [];

for fId = 1:NumFiles
    disp(['[proc] - Loading psd ' num2str(fId) '/' num2str(NumFiles) ': '  filenames{fId}])
    
    cdata = load(filenames{fId});
    cpos = cdata.analysis.event.pos;
    ctyp = cdata.analysis.event.typ;
    cpsd = cdata.psd;
    
   
    cWinStep    = cdata.analysis.WinStep;
    cSampleRate = cdata.analysis.SampleRate;
    cFreqs      = cdata.analysis.frequencies;
    
    
    % Extract Event Informations
    cCueId = false(length(ctyp), 1);
    for cId = 1:NumClasses
        cCueId = cCueId | (ctyp == EVENT_OF_INTEREST(cId));
    end
    cCuePos = cpos(cCueId);
    cTrialLb = ctyp(cCueId);
    
    % General useful information
    cNumTrials = length(cCuePos);                                
    [~, cFreqId] = intersect(cFreqs, FreqGrid);  % Select frequency range
    
    
    % Extract dataset
    cSmrStart = cCuePos + abs(SMR_POS(1));
    cSmrStop  = cCuePos + abs(SMR_POS(2)) - 1;
    cNumWins  = abs(diff(SMR_POS));

    cD = zeros(cNumWins, NumFreqs, NumChans, cNumTrials);
    for trId = 1:cNumTrials
        cstart = cSmrStart(trId); 
        cstop  = cSmrStop(trId);
        cD(:, :, :, trId) = cpsd(cstart:cstop, cFreqId, :);
    end
    
    % Reshape dataset to features
    cNumFeatures     = size(cD, 2)*size(cD, 3);
    cNumObservations = size(cD, 1)*size(cD, 4);
    cD_tmp = permute(cD, [1 4 2 3]);
    cP     = reshape(cD_tmp, [cNumObservations cNumFeatures]);
    
    % Reshape Trial Labels
    cPk = reshape(repmat(cTrialLb, [1 size(cD, 1)])', size(cD, 1)*size(cD, 4), 1);
    
    % Concatenate label vectors
    Rk = cat(1, Rk, fId*ones(length(cPk), 1));
    Pk = cat(1, Pk, cPk);
    Mk = cat(1, Mk, ModalityRun(fId)*ones(length(cPk), 1));
    
    % Concatenate features matrix
    P = cat(1, P, cP);
end

%% Extract observation from offline
Ck = zeros(size(Mk));
Offline_TrainIdx = find(Mk == 0, floor(0.75*sum(Mk == 0)), 'first');
Offline_TestIdx  = find(Mk == 0, floor(0.25*sum(Mk == 0)), 'last');
Online_TestIdx   = find(Mk == 1);

Ck(Offline_TrainIdx) = 0;
Ck(Offline_TestIdx)  = 1;
Ck(Online_TestIdx)   = 2;

disp('[proc] - Computing CVA on offline runs');
U  = P(Ck == 0, :);
Uk = Pk(Ck == 0, :);

% Canonical Variate analysis
dp = cva_tun_opt(U, Uk);
dp_r = reshape(dp, [NumFreqs NumChans]);

[sort_dp, sort_id] = sort(dp, 'descend');
idfeatures = sort(sort_id(1:NumFeatures));


%% Training classifier on offline data (first 3/4)
disp('[proc] - Training classifier on offline data (3/4)');
ClassifierType = 'gau'; % 'lda'; 'qda'; 'knn'; 'gau'
model = classTrain(U(:, idfeatures), Uk, ClassifierType); 

%% Testing classifier on all data
disp('[proc] - Testing classifier on all data');
[pp, Gk] = classTest(model, P(:, idfeatures)); 

%% Single Sample accuracy
% Accuracy on OFFLINE data - TrainSet
Acc(1) = 100*sum(Gk(Ck==0) == Pk(Ck==0))./length(Pk(Ck==0));
% Accuracy on OFFLINE data - TestSet
Acc(2) = 100*sum(Gk(Ck==1) == Pk(Ck==1))./length(Pk(Ck==1));
% Accuracy on ONLINE data  - TestSet
Acc(3) = 100*sum(Gk(Ck==2) == Pk(Ck==2))./length(Pk(Ck==2));

disp(['[res] - Accuracy on OFFLINE data - TrainSet: ' num2str(Acc(1), '%2.2f') '%']);
disp(['[res] - Accuracy on OFFLINE data - Testset : ' num2str(Acc(2), '%2.2f') '%']);
disp(['[res] - Accuracy on ONLINE  data - Testset : ' num2str(Acc(3), '%2.2f') '%']);

%% Plotting feature map on offline data
figure;

imagesc(dp_r')
set(gca, 'XTickLabel', num2str(FreqGrid(get(gca, 'XTick'))'));
colorbar;
hold on;
for i = 1:length(idfeatures)
    [cx, cy] = ind2sub([NumFreqs NumChans], idfeatures(i));
    plot(cx, cy, 'or', 'LineWidth', 3);
end
hold off;

title([subject ' - DP map: ' EVENT_LABELS{EVENT_OF_INTEREST(1)} ' vs. ' EVENT_LABELS{EVENT_OF_INTEREST(2)}]);
xlabel('Frequencies [Hz]');
ylabel('Channels');

%fig_figure2pdf(gcf, ['./smr_' subject '_dpmap.pdf'], 'HighResolution', [0 0 0 0]);
export_fig(['./smr_' subject '_dpmap.pdf'], '-pdf')

%% Saving data
disp('Saving data');
save(['smr_' subject '_dataset.mat'], 'P', 'Pk', 'Rk', 'Mk', 'Ck', 'Gk', 'pp', 'dp', 'idfeatures', 'model');



