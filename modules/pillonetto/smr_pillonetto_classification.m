clc;
clear all;
close all;

subject  = 'b7';
root    = '/mnt/data/Research/smr/';
subfolder = '/analysis/';
dataroot  = util_getdir(root, subject);
filenames = util_getfile(cellfun(@cat, repmat({2}, 1, length(dataroot)), dataroot(:)', repmat({subfolder}, 1, length(dataroot)), 'UniformOutput', 0), '.mat');

classifpath = util_getfile(cellfun(@cat, repmat({2}, 1, length(dataroot)), dataroot(:)', repmat({subfolder}, 1, length(dataroot)), 'UniformOutput', 0), '.dat');

NumFiles  = length(filenames);
ModalityRun = [0 0 0 1 1 1 1];
NumFeatures = 5;

EVENT_LEFT_HAND  = 769;
EVENT_RIGHT_HAND = 770;
EVENT_BOTH_FEET  = 771;
EVENT_LABELS{769} = 'lefthand';
EVENT_LABELS{770} = 'righthand';
EVENT_LABELS{771} = 'bothfeet';

EVENT_OF_INTEREST = [EVENT_LEFT_HAND EVENT_BOTH_FEET];
%EVENT_OF_INTEREST = [EVENT_RIGHT_HAND EVENT_BOTH_FEET];
%EVENT_OF_INTEREST = [EVENT_RIGHT_HAND EVENT_LEFT_HAND];
NumClasses = length(EVENT_OF_INTEREST);

D   = [];
Rk  = [];
Mk  = [];
typ = [];
pos = [];
dur = [];

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading psd data ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    cdata = load(cfilename);
    ctyp  = cdata.analysis.event.typ;
    cpos  = cdata.analysis.event.pos;
    cdur  = cdata.analysis.event.dur;
    
    typ = cat(1, typ, ctyp);
    dur = cat(1, dur, cdur);
    pos = cat(1, pos, cpos + size(D, 1));
    Rk  = cat(1, Rk, fId*ones(size(cdata.psd, 1), 1));
    Mk  = cat(1, Mk, ModalityRun(fId)*ones(size(cdata.psd, 1), 1));
    
    D = cat(1, D, cdata.psd);
    
   
    SampleRate  = cdata.analysis.buffer.SampleRate;
    Freqs       = cdata.analysis.proc.freqs;
    WinLength   = floor(cdata.analysis.buffer.WinPeriod*SampleRate);
    WinStep     = floor(cdata.analysis.buffer.WinOverlap*SampleRate);
end

hdr.typ = typ;
hdr.pos = pos;
hdr.dur = dur;

FreqGrid = 4:2:48;
[~, FreqId] = intersect(Freqs, FreqGrid); 

NumSamples  = size(D, 1);

[Ek, evtcue, evtdur] = smr_data_label(NumSamples, hdr);

Idx = false(length(Ek), 1);
for cId = 1:NumClasses
    Idx = Idx | Ek == EVENT_OF_INTEREST(cId);
end

U = log(D(Idx, FreqId, :) + 1);
%U = D(Idx, FreqId, :);

NumObs   = size(U, 1);
NumFreqs = size(U, 2);
NumChans = size(U, 3);

F = proc_reshape_ts_bc(U);
Fk = Ek(Idx);
Fm = Mk(Idx);
Fr = Rk(Idx);

%% Dataset creation
Fd = zeros(length(Fm), 1);
Offline_TrainIdx = find(Fm == 0, floor(0.75*sum(Fm == 0)), 'first');
Offline_TestIdx  = find(Fm == 0, floor(0.25*sum(Fm == 0)), 'last');
Online_TestIdx   = find(Fm == 1);

Fd(Offline_TrainIdx) = 0;
Fd(Offline_TestIdx)  = 1;
Fd(Online_TestIdx)   = 2;

%% Canonical Variate analysis
disp('[proc] - Computing CVA on offline runs (Fd = 0)');

dp = cva_tun_opt(F(Fd == 0, :), Fk(Fd == 0));
dp_r = reshape(dp, [NumFreqs NumChans]);

[sort_dp, sort_id] = sort(dp, 'descend');
idfeatures = sort(sort_id(1:NumFeatures));

%% Training classifier on offline data (first 3/4)
disp('[proc] - Training classifier on offline data (3/4)');
ClassifierType = 'gau'; % 'lda'; 'qda'; 'knn'; 'gau'
model = classTrain(F(Fd == 0, idfeatures), Fk(Fd == 0), ClassifierType);  

%% Testing classifier on all data
disp('[proc] - Testing classifier on all data');
[pp, Gk] = classTest(model, F(:, idfeatures)); 

%% Single Sample accuracy
% Accuracy on OFFLINE data - TrainSet
Acc(1) = 100*sum(Gk(Fd==0) == Fk(Fd==0))./length(Fk(Fd==0));
% Accuracy on OFFLINE data - TestSet
Acc(2) = 100*sum(Gk(Fd==1) == Fk(Fd==1))./length(Fk(Fd==1));
% Accuracy on ONLINE data  - TestSet
Acc(3) = 100*sum(Gk(Fd==2) == Fk(Fd==2))./length(Fk(Fd==2));

disp(['[res] - Accuracy on OFFLINE data - TrainSet: ' num2str(Acc(1), '%2.2f') '%']);
disp(['[res] - Accuracy on OFFLINE data - Testset : ' num2str(Acc(2), '%2.2f') '%']);
disp(['[res] - Accuracy on ONLINE  data - Testset : ' num2str(Acc(3), '%2.2f') '%']);

%% Plotting feature map on offline data
figure;

imagesc(dp_r');
colorbar;
hold on;
for i = 1:length(idfeatures)
    [cx, cy] = ind2sub([NumFreqs NumChans], idfeatures(i));
    plot(cx, cy, 'or', 'LineWidth', 3);
end
hold off;
set(gca, 'XTickLabel', num2str(FreqGrid(get(gca, 'XTick'))'));
title([subject ' - DP map: ' EVENT_LABELS{EVENT_OF_INTEREST(1)} ' vs. ' EVENT_LABELS{EVENT_OF_INTEREST(2)}]);
xlabel('Frequencies [Hz]');
ylabel('Channels');


%fig_figure2pdf(gcf, ['./smr_' subject '_dpmap.pdf'], 'HighResolution', [0 0 0 0]);
export_fig(['./smr_' subject '_dpmap.pdf'], '-pdf')


%% Saving data
disp('Saving data');
save(['smr_' subject '_dataset.mat'], 'F', 'Fk', 'Fr', 'Fm', 'Fd', 'Gk', 'pp', 'dp', 'idfeatures', 'model');
