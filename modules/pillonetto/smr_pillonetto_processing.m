% clc;
% clear all;

% subject = 'a4';
root    = '/mnt/data/Research/smr/';

%dataroot = util_getdirbysubj(subject, root);
dataroot = util_getdir(root, subject);
%pattern  = 'offline'; 
pattern  = 'online'; 
filenames = util_getfile(dataroot, '.gdf', pattern);

load('lapmask_16ch.mat', 'lapmask');

WinPeriod = 0.5;
WinStep   = 32;
NumFiles  = length(filenames);

disp(['[proc] - Subject: ' subject]);
for fId = 1:NumFiles
    cfile = filenames{fId};
    disp(['[proc] - Processing file ' num2str(fId) '/' num2str(NumFiles)])
    disp(['[proc] - GDF File: ' cfile]);
    [currs, currh] = sload(cfile);
    
    pos = currh.EVENT.POS;
    typ = currh.EVENT.TYP;
    SampleRate = currh.SampleRate;
    
    s = currs(:, 1:end-1);
    
    NumSamples  = size(s, 1);
    NumChans    = size(s, 2);
    WinLength   = WinPeriod*SampleRate;
    WinStart    = 1:WinStep:NumSamples - WinLength;
    WinStop     = WinStart + WinLength - 1;
    NumWins     = length(WinStart);
    
    NFFT   = max(2^nextpow2(WinLength), 256);

    if mod(NFFT, 2) == 0        % Even
        NumFreqs = (NFFT/2 + 1);
    else                        % Odd
        NumFreqs = (NFFT+1)/2;
    end
      
    
    % Laplacian filter
    slap = s*lapmask;
    
    data = slap;
    
    % Periodogram
    psd = zeros(NumWins, NumFreqs, NumChans);
    for wId = 1:NumWins
        cstart = WinStart(wId);
        cstop  = WinStop(wId);
        for chId = 1:NumChans 
            [psd(wId, :, chId), f] = periodogram(data(cstart:cstop, chId), hann(WinLength), NFFT, SampleRate);     
        end
    end
    
    
    analysis.SampleRate  = SampleRate;
    analysis.WinLength   = WinLength;
    analysis.WinStep     = WinStep;
    analysis.laplacian   = lapmask;
    analysis.frequencies = f;
    analysis.event.pos   = floor(pos/WinStep);
    analysis.event.typ   = typ;
    
    [savepath, name] = fileparts(currh.FileName);
    
    if(exist([savepath '/analysis/'], 'dir') == 0)
        disp(['Creating analysis folder in: ' savepath]);
        mkdir(savepath, 'analysis');
    end
    
    if(exist([savepath '/analysis/'], 'dir') == 7)
        disp(['Saving psd processed data in: ' savepath '/analysis/']);
        psdfilename = [savepath '/analysis/' name '.mat'];   
        save(psdfilename, 'psd', 'analysis');
    end
end