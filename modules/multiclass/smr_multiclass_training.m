clc; clear all;

smr_multiclass_support;

support.info.subfolder = '20150326_p1/';
support.info.subject   = 'p1';
support.info.basename  = [support.info.rootpath support.info.subfolder];



%% Importing and concatenating
disp('[io] - Importing and concatenate gdf files');
eegpaths = util_getfiles({support.info.basename}, '.gdf', 'offline');
[s, h] = util_cat_gdf(eegpaths(:));

disp('[io] - Importing and concatenate psd files');
psdpaths = util_getfiles({[support.info.basename '/psd/']}, '.dat', 'offline');
if isempty(psdpaths)
    error('chk:psd', 'PSDs not found. Please generate them with wp_pwelch');
end
psd = [];
for fId = 1:length(psdpaths)
    cpsd      = wc_load(psdpaths{fId}, 'eigen', 'double');
    csamples  = size(cpsd, 1)/support.processing.psd.numfreqs;
    cchannels = size(cpsd, 2);
    cpsd      = wc_2dTo3d(cpsd, [support.processing.psd.numfreqs cchannels csamples]);
    psd       = cat(1, psd, permute(cpsd, [3 1 2]));
end

%% Initialize data parameters
NumSamples  = size(s, 1);
NumChannels = size(s, 2);
NumFrames   = size(psd, 1);
NumFreqs    = size(psd, 2);
 