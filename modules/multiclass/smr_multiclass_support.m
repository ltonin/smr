
support.info.rootpath  = '/mnt/data/Research/smr/';
support.info.subfolder = '';
support.info.subject   = '';

support.info.classes.event  = [769 770 771 783];
support.info.classes.id     = {'mi_hand_left', 'mi_hand_right', 'mi_both_feet', 'mi_rest'};


support.acquisition.buffersize      = 512;
support.acquisition.winshift        = 32;
support.acquisition.samplerate      = 512;
support.processing.psd.freqgrid     = 0:2:256;
support.processing.psd.numfreqs     = length(support.processing.psd.freqgrid);
