clear all; clc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

root  = '/mnt/data/Research/smr/20150311_b4/';

fgdf    = [root 'b4.20150311.123433.online.mi.mi_rhbf.gdf'];
fpsd    = [root 'b4.20150311.123433.online.mi.mi_rhbf_psd.dat'];
clfpath = [root 'b4_rhbf_20150311.mat'];

BCI_REJECTION = 0.55;
BCI_EXPALPHA     = 0.98;

BUFFER_SIZE  = 512;
BUFFER_SHIFT = 32;
PSD_WLENGTH  = 256;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[s, h]  = sload(fgdf);
psd     = wc_load(fpsd, 'eigen', 'double');
clf     = load(clfpath);

SampleRate  = h.SampleRate;
NumChannels = size(s, 2) - 1;
NumFreqs    = PSD_WLENGTH/2 + 1;
NumFrames   = size(psd, 1)/NumFreqs;
FreqGrid    = linspace(0, SampleRate/2, NumFreqs);


psd = permute(wc_2dTo3d(psd, [NumFreqs NumChannels NumFrames]), [3 1 2]);

features = proc_reshape_ts_bc(psd);
[frqId, chnId] = fcnbi_feature_conversion(clf.analysis.tools.features, FreqGrid);
featId = fcnbi_feature_sub2ind([NumFreqs NumChannels], frqId, chnId);

evttyp      = h.EVENT.TYP;
evtpos      = ceil(h.EVENT.POS/BUFFER_SHIFT);

events = zeros(NumFrames, 1);
events(evtpos) = evttyp;

rawpp = 0.5*ones(NumFrames, 2);
rejpp = 0.5*ones(NumFrames, 2);
intpp = inf(NumFrames, 2);

p_intpp = [0.5 0.5];

for frId = 1:NumFrames
   
    if frId < BUFFER_SIZE/BUFFER_SHIFT
        continue;
    end
    
    cfeatures = features(frId, featId);
    
    
    [~, crawpp] = gauClassifier(clf.analysis.tools.net.gau.M, ... 
                                     clf.analysis.tools.net.gau.C, ...
                                     cfeatures);
    crejpp = crawpp;                         
    if max(crawpp) < BCI_REJECTION
        crejpp = p_intpp;
    end
    
    cintpp = eegc3_expsmooth(p_intpp, crejpp, BCI_EXPALPHA);
    
    rawpp(frId, :) = crawpp; 
    rejpp(frId, :) = crejpp; 
    intpp(frId, :) = cintpp;
    
    if (events(frId) == 781) || (events(frId) == 898) || (events(frId) == 897)
        cintpp = [0.5 0.5];
    end
    
    
    
    p_intpp = cintpp;
                                 

    
    
    
end
bci = cnbi_smr_simloop(fgdf, [], clfpath, BCI_REJECTION, BCI_EXPALPHA);
keyboard

evtlist         = get_gdfevt_list;
CueNames = {'RightHand', 'BothFeet'};
CueEvent = get_gdfevt(CueNames, evtlist);
TargetNames = {'TargetHit', 'TargetMiss'};
NumTrials       = sum(evttyp == get_gdfevt('CFeedback', evtlist));
PosTargetHit    = evtpos(evttyp == get_gdfevt('TargetHit', evtlist));
PosTargetMiss   = evtpos(evttyp == get_gdfevt('TargetMiss', evtlist));
LbTarget        = evttyp(evttyp == get_gdfevt('TargetHit', evtlist) | evttyp == get_gdfevt('TargetMiss', evtlist));
LbClass         = evttyp(evttyp == get_gdfevt(CueNames{1}, evtlist) | evttyp == get_gdfevt(CueNames{2}, evtlist));
StartTrial      = evtpos(evttyp == get_gdfevt('CFeedback', evtlist));
StopTrial       = sort([PosTargetHit; PosTargetMiss]);

% Model matrix 
% F = [A,B;
%      O,A];
F = [1 0; 0 1];
% Measurement matrix 
% H = zeros(6);
% for i = 1:3
%     H(i,i) = 1;
% end
H = [1 0; 0 0];

% Covariance Matrix
% kappa = 1/100000;
kappa = 1/10^2;

R = eye(2);
Q = kappa*eye(2);

Pstart = F*Q*F';
Xstart=[0.5 0.5]';





rawpp = [];
rpp = [];
ipp = [];

pp_k = [];

tLbClass = [];
tLbTarget = [];
TrialStart = 1;

for trId = 1:NumTrials
    cstart  = StartTrial(trId); 
    cstop   = StopTrial(trId);
    clength = length(cstart:cstop);
    
    cprobs = bci.cprobs(cstart:cstop, :);
    rprobs = bci.rprobs(cstart:cstop, :);
    iprobs = bci.iprobs(cstart:cstop, :);
    
    pnew = Pstart;
    xnew = Xstart;
    cprobs_k = cprobs;
    
    
    for sId = 2:size(cprobs, 1)
        cpp  = cprobs(sId, 1);
        cvel = diff(cprobs(sId-1:sId, 1))*0.5;
        [cpp_k, pnew, xnew] = filt_kalman([cpp cvel]', pnew, xnew, F, H, Q, R);
        cprobs_k(sId, :) = [cpp_k(1, :) (1-cpp_k(1, :))];
    end    
    
    
    rawpp  = cat(1, rawpp, cprobs);
    rpp = cat(1, rpp, rprobs);
    ipp = cat(1, ipp, iprobs);
    pp_k = cat(1, pp_k, cprobs_k);

    tLbClass = cat(1, tLbClass, LbClass(trId)*ones(clength, 1));
    tLbTarget = cat(1, tLbTarget, LbTarget(trId)*ones(clength, 1));
    
    TrialStart = cat(1, TrialStart, TrialStart(end) + clength);
end



sLbClass = nan(length(tLbClass), 1);
sLbClass(tLbClass == CueEvent(1)) = 0.8;
sLbClass(tLbClass == CueEvent(2)) = 0.2;
sLbClass(TrialStart) = nan;

sLbTarget = nan(length(tLbTarget), 1);
sLbTarget(tLbTarget == get_gdfevt('TargetHit', evtlist)) = 0.9;


hold on; 
plot(rawpp(:, 2), '.', 'Color', [190 190 190]/255); 
plot(ipp(:, 2), 'LineWidth', 2); 
plot(pp_k(:, 2), 'g', 'LineWidth', 2); 
% plot(rpp_k(:, 2), 'm', 'LineWidth', 2); 
% plot(ipp_k(:, 2), 'c', 'LineWidth', 2); 
plot(sLbClass, 'r', 'LineWidth', 4); 
plot(sLbTarget, 'g', 'LineWidth', 4); 
plot_vline(TrialStart, 'k');
plot_hline(0.7, 'k');
plot_hline(0.3, 'k');
grid on;
hold off;
