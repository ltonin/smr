function whitk_smr_simloop(gdfpath, clfpath, varargin)

    opt  = getarg_default(varargin);
    path = get_datapath(gdfpath, clfpath);
    
    % Loading data
    h  = sopen(path.data,'r'); sclose(h);
    psd     = wc_load(path.features, 'eigen', 'double');
    clf     = load(path.classifier);
    
    % Getting information
    SampleRate  = h.SampleRate;
    NumSamples  = h.NRec*SampleRate;
    NumChannels = length(h.Label) - 1;
    NumFrames   = NumSamples/opt.bshift;
    NumFreqs    = size(psd, 1)/NumFrames;
    FreqGrid    = linspace(0, SampleRate/2, NumFreqs);
      
    % Preparing features
    psd      = permute(wc_2dTo3d(psd, [NumFreqs NumChannels NumFrames]), [3 1 2]);
    features = proc_reshape_ts_bc(psd);
    [frqId, chnId] = fcnbi_feature_conversion(clf.analysis.tools.features, FreqGrid);
    featId         = fcnbi_feature_sub2ind([NumFreqs NumChannels], frqId, chnId);
    
    % Preparing events
    pos    = ceil(h.EVENT.POS/opt.bshift);
    events = zeros(NumFrames, 1);
    events(pos) = h.EVENT.TYP;
    
    % Starting the simulated loop
    pp    = 0.5*ones(NumFrames, 2);
    rpp   = 0.5*ones(NumFrames, 2);
    ipp   = inf(NumFrames, 2);
    kpp   = 0.5*ones(NumFrames, 2);
    p_ipp = [0.5 0.5];
    p_kpp = 0.5;
    
    for frId = 1:NumFrames
   
        if frId < opt.minframes
            continue;
        end
    
        cfeatures = features(frId, featId);
    
    
        [~, c_pp] = gauClassifier(clf.analysis.tools.net.gau.M, ... 
                                    clf.analysis.tools.net.gau.C, ...
                                    cfeatures);
    
        % cnbi postprocessing
        [c_ipp, c_rpp] = postproc_cnbi(c_pp, p_ipp, opt.rejection, opt.integration);
        
        % kalman postprocessing
        [c_kpp, opt.klmstr] = postproc_kalman(c_pp, p_kpp, opt.klmstr);
        
        
        pp(frId, :)  = c_pp; 
        rpp(frId, :) = c_rpp; 
        ipp(frId, :) = c_ipp;
        %keyboard
        kpp(frId, :) = [c_kpp (1 - c_kpp)]';
    
        if isevent(events(frId), opt.resetevt)
            c_ipp = [0.5 0.5];
            c_kpp = 0.5;
            opt.klmstr.pnew = opt.klmstr.pstart;
            opt.klmstr.xnew = opt.klmstr.xstart;
        end

    
        p_ipp = c_ipp;
        p_kpp = c_kpp;
    end
    
    keyboard

end

function [ipp, rpp] = postproc_cnbi(c_pp, p_ipp, rej, alpha)
% cnbi pp postprocessing: rejection + integration

    rpp = c_pp;                         
    if max(c_pp) < rej
        rpp = p_ipp;
    end

    ipp = eegc3_expsmooth(p_ipp, rpp, alpha);

end

function [kpp, klmstr] = postproc_kalman(c_pp, p_pp, klmstr)
    pnew = klmstr.pnew;
    xnew = klmstr.xnew;
    F    = klmstr.F;
    H    = klmstr.H;
    Q    = klmstr.Q;
    R    = klmstr.R;
    
    %keyboard
    c_pp = c_pp(1);
    p_pp = p_pp(1);
    cvel = diff([p_pp c_pp]);
    [kpp, klmstr.pnew, klmstr.xnew] = filt_kalman([c_pp cvel]', pnew, xnew, F, H, Q, R);
    kpp = kpp(1);
end

function res = isevent(evt, resetevt)
    res = false;
    for eId = 1:length(resetevt)
        if(evt == resetevt(eId))
            res = res || true;
        end
    end
end

function path = get_datapath(gdfpath, clfpath, root)
    
    if nargin < 3
        root = [];
    end

    default_pattern = '_psd';
    path.data       = [root gdfpath];
    path.features   = [root gdfpath(1:end-4) default_pattern '.dat'];
    path.classifier = [root clfpath];

end

function opt = getarg_default(args)

        nargs = length(args);
        opt.rejection   = 0.6;
        opt.integration = 0.96;
        opt.resetevt    = [781 898 897];
        opt.bsize       = 512;
        opt.bshift      = 32;
        opt.klmstr.F        = [1 0; 0 1];
        opt.klmstr.H        = [1 0; 0 0];
        opt.klmstr.k        = 1/10000;
        opt.klmstr.R        = eye(2);
        opt.klmstr.Q        = opt.klmstr.k*opt.klmstr.R;
        opt.klmstr.pstart   = opt.klmstr.F*opt.klmstr.Q*opt.klmstr.F';
        opt.klmstr.xstart   = [0.5 0.5]';
        opt.klmstr.pnew     = opt.klmstr.pstart;
        opt.klmstr.xnew     = opt.klmstr.xstart;
        
        switch(nargs)
            case 1
                opt.rejection   = getarg(args{1}, opt.rejection);
            case 2
                opt.rejection   = getarg(args{1}, opt.rejection);
                opt.integration = getarg(args{2}, opt.integration);
            case 3
                opt.rejection   = getarg(args{1}, opt.rejection);
                opt.integration = getarg(args{2}, opt.integration);
                opt.klmstr      = getarg(args{3}, opt.klmstr);
            case 4
                opt.rejection   = getarg(args{1}, opt.rejection);
                opt.integration = getarg(args{2}, opt.integration);
                opt.klmstr      = getarg(args{3}, opt.klmstr);
                opt.resetevt    = getarg(args{4}, opt.resetevt);
            case 5
                opt.rejection   = getarg(args{1}, opt.rejection);
                opt.integration = getarg(args{2}, opt.integration);
                opt.klmstr      = getarg(args{3}, opt.klmstr);
                opt.resetevt    = getarg(args{4}, opt.resetevt);
                opt.bsize       = getarg(args{5}, opt.bsize);
            case 6
                opt.rejection   = getarg(args{1}, opt.rejection);
                opt.integration = getarg(args{2}, opt.integration);
                opt.klmstr      = getarg(args{3}, opt.klmstr);
                opt.resetevt    = getarg(args{4}, opt.resetevt);
                opt.bsize       = getarg(args{5}, opt.bsize);
                opt.bshift      = getarg(args{6}, opt.bshift);
            otherwise
                error('chk:opt', 'Too many optional arguments.');
        
        end
        
        opt.minframes = opt.bsize/opt.bshift;
end

function arg = getarg(arg, default) 
    
    if(isempty(arg))
        arg = default;
    end
        
end