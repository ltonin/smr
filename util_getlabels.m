function [lbl, pos] = util_getlabels(evttyp, evtpos, evtstart, evtstop, NumSamples)
% [lbl, pos] = util_getlabels(evttyp, evtpos, evtstart, evtstop, NumSamples)
%
% Given the list of the events and the related list of position, the
% function create a label vectors for the events identified by evtstart and
% evtstop.
%
% Input:
%       evttyp [entries x 1]    Ordered list with event types
%       evtpos [entries x 1]    Ordered list with event positions
%       evtstart                Event type related to the start period
%       evtstop                 Event type related to the stop period
%                               evtstart and evtstop might be vector. Each
%                               value inside the vectors corresponds to
%                               the start position or to the end position.
%       NumSamples              Number of total samples in the file
%
% Output:
%       lbl [samples x 1]       Logical vector with 1 for the periods
%                               indicated by evtstart and evtstop
%       pos [numevt x 2]         Array with the positions of evtstart
%                               and evtstop 

    idstart = [];
    for i = 1:length(evtstart)
        idstart = cat(1, idstart, find(evttyp == evtstart(i)));
    end
    idstart = sort(idstart);
    
    idstop = [];
    for i = 1:length(evtstop)
        idstop  = cat(1, idstop, find(evttyp == evtstop(i)));
    end
    idstop = sort(idstop);


    if(isempty(idstart))
        error('chk:evt', ['Event ' num2str(evtstart) ' not found']);
    end
    
    if(isempty(idstop))
        error('chk:evt', ['Event ' num2str(evtstart) ' not found']);
    end

    if (length(idstart) ~= length(idstop))
        error('chk:evt', ['Different number of events for start and stop']);
    end
    
    lbl = false(NumSamples, 1);
    start = [];
    stop  = [];
    for i = 1:length(idstart)
        cstart = evtpos(idstart(i), 1);
        cstop  = evtpos(idstop(i), 1);
        lbl(cstart:cstop) = true;
        start = cat(1, start, cstart);
        stop  = cat(1, stop, cstop);
    end
    
    pos = [start stop];

end