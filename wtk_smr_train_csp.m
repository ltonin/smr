function [CSP, gau, qda] = wtk_smr_train_csp(TrainFiles, buffercfg, evtcfg, gaucfg, qdacfg)


    NumFeatures = 4;

    %% Utilities
    displabel = '[wtk_smr_train_csp] - ';
    
    %% Get Windows/Buffer parameters
    WinShift = buffercfg.winshift;
    WinSize  = buffercfg.winsize;
    
    %% Get Events values
    cfeedback   = evtcfg.cfeedback;
    cues        = evtcfg.cues;
    mindur      = evtcfg.minduration;
    
    %% Import file for train and test
    disp([displabel 'Importing GDF files']);
    [s, h] = util_cat_gdf(TrainFiles(:));

    NumSamples  = size(s, 1);
    NumChannels = size(s, 2);
    NumEvtCues  = length(cues);


    %% Labeling
    disp([displabel 'Data Labeling']);
    LblClass = zeros(NumSamples, 1);
  
    IdxCue = [];
    for cId = 1:NumEvtCues
        IdxCue   = cat(1, IdxCue, find(h.typ == cues(cId)));
    end
    TypCue   = h.typ(sort(IdxCue));
    NumCues  = length(TypCue);
    
    PosCFeed = zeros(NumCues, 1);
    DurCFeed = zeros(NumCues, 1);
    
    for i = 1:length(IdxCue)
        posidx = IdxCue(i) + 1;
        if(h.typ(posidx) ~= cfeedback)
            warning('chk:pos', 'Error event order');
        end
        PosCFeed(i) = h.pos(posidx);
        DurCFeed(i) = h.dur(posidx);
    end
    NumCFeed = length(PosCFeed);
    
    if (NumCues ~= NumCFeed)
        error('chk:lbl', 'Mismatch between total number of cues and number of cfeedback events');
    end
    
    TrialDuration = zeros(NumCFeed, 1);
    for trId = 1:NumCFeed
        ctyp = TypCue(trId);
        cstart = PosCFeed(trId);
        cdur   = DurCFeed(trId);
        if (cdur == 0)
           cdur = mindur;
           warning('chk:dur', ['Duration not available for trial ' num2str(trId) '. Using ' num2str(mindur) ' instead.']);
        end
        cstop = cstart + cdur - 1;
        LblClass(cstart:cstop) = ctyp;
        TrialDuration(trId) = cstop - cstart;
    end
    
    disp([displabel 'Trials found: ' num2str(NumCFeed)]);
    
    figure;
    hist(TrialDuration/512);
    xlabel('Trial duration [s]');
    ylabel('#');
    title('Histogram of trial durations');
    grid on;
    drawnow;
    
    %% Get Classes
    Classes    = unique(TypCue);
    NumClasses = length(Classes);
    
    if (NumClasses ~= 2)
        error('chk:cls', 'Only two classes are allowed');
    end
   
    disp([displabel 'Two classes found: ' num2str(Classes(1)) '/' num2str(Classes(2))]);
    
    %% Filtering data
    s = filt_bp(s, 4, [6 80], 512);
    
    %% Windowing
    WinStart = 1:WinShift:NumSamples - WinSize;
    WinStop  = WinStart + WinSize - 1;
    NumWindows = length(WinStart);
    sW = zeros(NumWindows, NumChannels);
    LblClassW = zeros(NumWindows, 1);
    for wId = 1:length(WinStart)
        cstart = WinStart(wId);
        cstop  = WinStop(wId);
        sW(wId, :) = mean(s(cstart:cstop, :), 1);
        LblClassW(wId) = LblClass(cstart);
    end

    %% CSP
    disp([displabel 'Computing CSP filters']);
    % Computing CSP coefficient
    CSP = filt_csp_dec(sW(LblClassW > 0, :), LblClassW(LblClassW > 0));

    disp([displabel 'Applying CSP filters']);
    % Applying CSP
    [sCSP, pCSP] = filt_csp_apply(sW, CSP, 6);

    %% Discriminancy
    disp([displabel 'Computing discriminancy on CSP patterns']);
    dp = eegc3_cva2(sCSP(LblClassW == Classes(1), :), sCSP(LblClassW == Classes(2), :));

    
    [~, FeatId] = sort(dp, 'descend'); 
    
    NumCols = 2;
    NumRows = 1 + ceil(sqrt(NumFeatures));
    subplot(NumRows, NumCols, 1:NumCols);
    hold on;
    bar(dp);
    hdl = plot(FeatId(1:NumFeatures), max(dp) + 3, 'ro', 'MarkerSize', 9, 'MarkerFaceColor', 'r');
    hold off;
    grid on;
    xlabel('CSP patterns');
    ylabel('dp');
    title('Discriminant power for CSP patterns on TrainSet');
    legend(hdl, 'Selected feature');
    
    for pId = 1:NumFeatures
        load('./chanlocs16');
        MaxCSP = max(max(abs(pCSP)));

        subplot(NumRows, NumCols, pId + NumCols);
    
        topoplot(pCSP(:, FeatId(pId)), chanlocs16, 'intrad', 0.4, 'headrad', 0.4);
        axis image;
        title([num2str(pId) ' - Selected pattern ' num2str(FeatId(pId))]);
        plot_setCLim(gca, [-MaxCSP MaxCSP]);
        drawnow;
    end

    %% Classifier Gaussian Training
    disp([displabel 'Training Gaussian classifier']);
    gau = wtk_train_gau(sCSP(LblClassW > 0, FeatId(1:NumFeatures)), LblClassW(LblClassW > 0), gaucfg);

    %% Classifier QDA Training
    disp([displabel 'Training QDA classifier']);
    qda = wtk_train_qda(sCSP(LblClassW > 0, FeatId(1:NumFeatures)), LblClassW(LblClassW > 0), qdacfg);


end