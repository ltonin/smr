clear all; clc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
root  = [getenv('WHISMR') 'extra/offlineloop/'];

BCI_PSD_GRID      = 0:2:256;
BCI_EXP_REJECTION = 0.6;
BCI_EXP_ALPHA     = 0.98;

fgdf    = [root 'offlineloop.gdf'];
fgau    = [root 'a1_lhbf_20150309.mat'];

evtlist = get_gdfevt_list;

CueNames = {'LeftHand', 'BothFeet'};
CueEvent = get_gdfevt(CueNames, evtlist);
TargetNames = {'TargetHit', 'TargetMiss'};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

bci = cnbi_smr_simloop(fgdf, [], fgau, BCI_EXP_REJECTION, BCI_EXP_ALPHA);

evtpos = bci.evt;
evttyp = bci.lbl;


NumTrials       = sum(evttyp == get_gdfevt('CFeedback', evtlist));
PosTargetHit    = evtpos(evttyp == get_gdfevt('TargetHit', evtlist));
PosTargetMiss   = evtpos(evttyp == get_gdfevt('TargetMiss', evtlist));
LbTarget        = evttyp(evttyp == get_gdfevt('TargetHit', evtlist) | evttyp == get_gdfevt('TargetMiss', evtlist));
LbClass         = evttyp(evttyp == get_gdfevt(CueNames{1}, evtlist) | evttyp == get_gdfevt(CueNames{2}, evtlist));
StartTrial      = evtpos(evttyp == get_gdfevt('CFeedback', evtlist));
StopTrial       = sort([PosTargetHit PosTargetMiss]);


pp = [];
rpp = [];
ipp = [];

tLbClass = [];
tLbTarget = [];
TrialStart = 1;

for trId = 1:NumTrials
    cstart  = StartTrial(trId);
    cstop   = StopTrial(trId);
    clength = length(cstart:cstop);
    
    pp = cat(1, pp, bci.cprobs(cstart:cstop, :));
    rpp = cat(1, rpp, bci.rprobs(cstart:cstop, :));
    ipp = cat(1, ipp, bci.iprobs(cstart:cstop, :));
    
    tLbClass = cat(1, tLbClass, LbClass(trId)*ones(clength, 1));
    tLbTarget = cat(1, tLbTarget, LbTarget(trId)*ones(clength, 1));
    
    TrialStart = cat(1, TrialStart, TrialStart(end) + clength);
end

sLbClass = nan(length(tLbClass), 1);
sLbClass(tLbClass == CueEvent(1)) = 0.8;
sLbClass(tLbClass == CueEvent(2)) = 0.2;
sLbClass(TrialStart) = nan;

sLbTarget = nan(length(tLbTarget), 1);
sLbTarget(tLbTarget == get_gdfevt('TargetHit', evtlist)) = 0.9;


hold on; 
plot(pp(:, 2), '.', 'Color', [190 190 190]/255); 
plot(ipp(:, 2), 'LineWidth', 2); 
plot(sLbClass, 'r', 'LineWidth', 4); 
plot(sLbTarget, 'g', 'LineWidth', 4); 
plot_vline(TrialStart, 'k');
plot_hline(0.7, 'k');
plot_hline(0.3, 'k');
grid on;
hold off;
