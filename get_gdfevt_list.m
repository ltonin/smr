function evtlist = get_gdfevt_list

    evtlist.value = [];
    evtlist.label = [];

    evtlist.value(end+1)    = 897;
    evtlist.value(end+1)    = 898;
    evtlist.value(end+1)    = 781;
    evtlist.value(end+1)    = 782;
    evtlist.value(end+1)    = 786;
    evtlist.value(end+1)    = 783;
    evtlist.value(end+1)    = 1;
    evtlist.value(end+1)    = 770;
    evtlist.value(end+1)    = 769;
    evtlist.value(end+1)    = 771;

    
    evtlist.label{end+1}    = 'TargetHit';
    evtlist.label{end+1}    = 'TargetMiss';
    evtlist.label{end+1}    = 'CFeedback';
    evtlist.label{end+1}    = 'DFeedback';
    evtlist.label{end+1}    = 'Fixation';
    evtlist.label{end+1}    = 'Cue';
    evtlist.label{end+1}    = 'Wait';
    evtlist.label{end+1}    = 'RightHand';
    evtlist.label{end+1}    = 'LeftHand';
    evtlist.label{end+1}    = 'BothFeet';
end
