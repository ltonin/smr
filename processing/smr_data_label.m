function [dlabel, evtcue, evtdur] = smr_data_label(sized, hdr, period, refevent) 
% SMR_DATA_LABEL
%
% [dlabel, tlabel, c] = smr_data_label(sized, hdr, [period, refevent]) 
%
% The function labels the data according to the hdr information.
% The structure of the event is assumed to be:
%
%   CueEvent - RefEvent
%
% Input:
%   - sized     data size
%   - hdr.pos   Position of the events
%   - hdr.typ   Type of the events
%   - hdr.dur   Duration of the events
%   - period    Period in samples to be extracted (optional, otherwise it
%               uses [0 hdr.dur]
%   - refevent  Reference event to be used to make extraction (optional,
%               default value is 781: continuous feedback)
%
% SEE ALSO: sload

    usedur = false;
    
    if nargin < 3
        usedur = true;
        refevent = 781;
    end

    if nargin < 4
        refevent = 781;
    end    
    
    
    RefId  = hdr.typ == refevent;
    RefPos = hdr.pos(RefId);
    RefDur = hdr.dur(RefId);
    RefCue = hdr.typ(find(RefId) - 1);
    
    NumRef = length(RefPos);
    
    dlabel = zeros(sized, 1);
    evtcue = zeros(NumRef, 1);
    evtdur = zeros(NumRef, 1);
    for eId = 1:NumRef
        if (usedur == true)
            period(1) = 0;
            period(2) = RefDur(eId);
        end
        
        cstart = RefPos(eId) + period(1);
        cstop  = RefPos(eId) + period(2) - 1;
        dlabel(cstart:cstop) = RefCue(eId);
        evtcue(eId) = RefCue(eId);
        evtdur(eId) = cstop - cstart +1;
    end
    
   
end