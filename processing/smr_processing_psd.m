%clear all; clc;

% subject  = 'b4';
pattern  = 'online';
rootpath = '/mnt/data/Research/smr/';
savepath = [pwd '/analysis/psd/'];

dirpath   = util_getdir(rootpath, subject);
filenames = util_getfile(dirpath, '.gdf', pattern);
load('lapmask_16ch');
NumFiles = length(filenames);

WinPeriod    = 0.5;       % seconds
WinOverlap   = 0.0625;    % seconds

if (exist(savepath, 'dir') ~= 7)
    mkdir(savepath);
end

for fId = 1:NumFiles
    cfilename = filenames{fId};
    disp(['[io] - Loading filename ' num2str(fId) '/' num2str(NumFiles)]);
    disp(['[io] - File: ' cfilename]);
    
    
    [s, h] = sload(cfilename);
    
    s = s(:, 1:end-1);
    
    NumSamples  = size(s, 1);
    NumChannels = size(s, 2);
    WinLength   = floor(WinPeriod*h.SampleRate);
    WinStep     = floor(WinOverlap*h.SampleRate);
    
    WinStart = 1:WinStep:(NumSamples - WinLength);
    WinStop  = WinStart + WinLength - 1;
    NumWins  = length(WinStart); 
    
    [NFFT, NumFreqs] = proc_getNFFT(WinLength);
    psd = zeros(NumWins, NumFreqs, NumChannels);
    
    disp( '[proc] - Computing psd:');
    disp(['       - SampleRate: ' num2str(h.SampleRate)  '  [Hz]']);
    disp(['       - WinPeriod:  ' num2str(WinPeriod)     '  [s]']);
    disp(['       - WinOverlap: ' num2str(WinPeriod)     '  [s]']);
    disp(['       - WinLength:  ' num2str(WinLength)     '  [samples]']);
    disp(['       - WinStep:    ' num2str(WinStep)       '  [samples]']);
    disp( '       - Type:       periodogram');
    disp( '       - WinType:    hann' );
    disp(['       - NFFT:       ' num2str(NFFT)]);
    
    for wId = 1:NumWins
       cwstart = WinStart(wId);
       cwstop  = WinStop(wId);
       
       cbuffer = s(cwstart:cwstop, :);
       
       cbuffer = cbuffer*lapmask;
       
       for chId = 1:NumChannels 
            [psd(wId, :, chId), f] = periodogram(cbuffer(:, chId), hann(WinLength), NFFT, h.SampleRate);     
        end
    end
    
    analysis.buffer.WinPeriod  = WinPeriod;
    analysis.buffer.WinOverlap = WinOverlap;
    analysis.buffer.SampleRate = h.SampleRate;
    
    analysis.event.typ = h.EVENT.TYP;
    analysis.event.pos = floor(h.EVENT.POS/WinStep);
    analysis.event.dur = floor(h.EVENT.DUR/WinStep);
    
    analysis.proc.type       = 'periodogram';
    analysis.proc.WinType    = 'hann';
    analysis.proc.NFFT       = NFFT;
    analysis.proc.freqs      = f;

    [~, name] = fileparts(cfilename);
    sfilename = [savepath name '.mat'];
    disp(['[out] - Saving psd in: ' sfilename]);
    save(sfilename, 'psd', 'analysis'); 
end