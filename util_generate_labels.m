function lbl = util_generate_labels(nsamples, typ, pos, dur)
% lbl = util_generate_labels(nsamples, typ, pos [, dur])
%
% The function convert the event position in a vector of labels 
% (nsamples x  1). 
%
% Input:
%
%   nsamples        Total number of samples of the data associated to the
%                   events
%   typ             Vector with events types
%   pos             Vector with events positions
%   dur             Vector with events duration [optional]
%
% dur argument is optional. If it is not provided then the function assumes
% the event strictly consecutive (each event lasts until the next one).

    nevt = length(typ);

    if nargin < 4
        dur = zeros(nevt, 1);
    end
    
    if length(pos) ~= nevt
        error('chk:posl', 'Number of positions different from number of events');
    end
    
    if length(dur) ~= nevt
        error('chk:durl', 'Number of durations different from number of events');
    end
    
 
    lbl = zeros(nsamples, 1);
    
    for eId = 1:nevt
       ctyp = typ(eId);
       cpos = pos(eId);
       cdur = dur(eId);
       cstart = cpos;
       cstop  = cstart + cdur;
       
       if (cdur == 0)                   % If duration is not provided OR duration is equal to 0,
                                        % then the label last until the next one (or until the 
                                        % end of the file)
           if eId == nevt
               cstop = nsamples;
           else
               cstop = pos(eId + 1);
           end
       end
       
       lbl(cstart:cstop) = ctyp;
        
    end
    
    
    

end