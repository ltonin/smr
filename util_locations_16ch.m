function chanlocs = util_locations_16ch(selection, origin)

    NumOrigins    = length(origin);
    NumSelections = length(selection);
    
    chanlocs = struct([]);
    
    for chId = 1:NumSelections
       csel = selection{chId};
       cId = [];
       for locId = 1:NumOrigins
           if(strcmpi(csel, origin(locId).labels))
                cId = cat(1, cId, locId);
                break;
           end
       end
       
       if (length(cId) ~= 1)
           warning('chk:ch', ['Channel ' csel ' not found']);
       else
            chanlocs(chId).labels    = origin(cId).labels;
            chanlocs(chId).theta     = origin(cId).theta;
            chanlocs(chId).radius    = origin(cId).radius;
            chanlocs(chId).X         = origin(cId).X;
            chanlocs(chId).Y         = origin(cId).Y;
            chanlocs(chId).Z         = origin(cId).Z;
            chanlocs(chId).sph_theta = origin(cId).sph_theta;
            chanlocs(chId).sph_phi   = origin(cId).sph_phi;
            chanlocs(chId).sph_radius= origin(cId).sph_radius;
            chanlocs(chId).type      = origin(cId).type;
            chanlocs(chId).urchan    = origin(cId).urchan;
       end
           
           
        
    end
    
end