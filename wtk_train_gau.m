function gau = wtk_train_gau(data, labels, gaucfg, gaustr)
% function gau = eegc3_train_gau(settings, data, labels, has_init)
%
% Function to train a CNBI Gaussian Classifier. From the input data, 
% random 70% is used for training and the remaining 30% for testing. 
% Function utilizes the gauInitiallization, gauUpdate, gauEval functions.
%
% Inputs:
%
% data:         Data matrix samples x features
%
% labels:       Labels vector samples x 1
%
% gaucfg:       Struct array with the following field:
%               .somunits   (typical: [2 2])
%               .sharedcov  (typical: 't')
%               .mimean     (typical: 0.0001)
%               .micov      (typical: 0.00001)
%               .epochs     (typical: 20)
%               .th         (typical: 0.7)
%               .terminate  (typical: true)
%
%
% gaustr:       If empty a new classifier will be trained from
%               scratch using SOM initialization (false), or a previously existing
%               classifier (gaustr) will be updated with the current data (true). 
%
% Outputs:
%
% gaustr MATLAB struct containing the means and covariances of the Gaussian
% classifier prototypes
%

if(nargin < 4)
    has_init = false;
end

values = unique(labels);
if(length(values) ~= 2)
    error('chk:lbl', 'Labels vector must have only two unique values (classes)');
end

labels(labels == values(1)) = 1;
labels(labels == values(2)) = 2;

% Split possible 50/50
% Find class samples
C{1} = find(labels==1);
C{2} = find(labels==2);

% Find minimum
[CminN Cmin]  = min([length(C{1}) length(C{2})]);
[CmaxN Cmax]  = max([length(C{1}) length(C{2})]);
disp(['[eegc3_train_gau] Dropping ' num2str(100*(CmaxN-CminN)/CmaxN) '% first'... 
    ' coming data of class ' num2str(Cmax) ' to avoid unbalanced classes']);
C{Cmax} = C{Cmax}(CmaxN-CminN+1:end);
dataind = union(C{1},C{2});
bdata = data(dataind,:);
blabels = labels(dataind,:);

% Random permutation
brand = randperm(length(blabels));
blabels = blabels(brand);
bdata = bdata(brand,:);

% Now split to training and testing set, 70/30 split
Pind = [1:round(0.7*length(blabels))];
Tind = [max(Pind)+1:length(blabels)];

P = bdata(Pind,:);
Pk = blabels(Pind,:);
   
T = bdata(Tind,:);
Tk = blabels(Tind,:);

Size = size(bdata,1);
TrSize = size(Pk,1);

% Test the balance of samples in training set
Pc1 = length(find(Pk==1))/TrSize;
Pc2 = length(find(Pk==2))/TrSize;

disp(['[wtk_train_gau] Training samples (%): Class 1 vs Class 2: '...
    num2str(100*Pc1) ' - ' num2str(100*Pc2)]);
if(abs(Pc1-Pc2)>0.2)
    disp(['[wtk_train_gau] Warning: Training samples seem to be too unbalanced...']);
end

% Test the balance of samples in testing set
Tc1 = length(find(Tk==1))/(Size-TrSize);
Tc2 = length(find(Tk==2))/(Size-TrSize);
disp(['[wtk_train_gau] Testing samples (%): Class 1 vs Class 2: '...
    num2str(100*Tc1) ' - ' num2str(100*Tc2)]);
if(abs(Tc1-Tc2)>0.2)
    disp(['[wtk_train_gau] Warning: Testing samples seem to be too unbalanced...']);
end

gau = {};

% Gaussian classifier
disp('[wtk_train_gau] Training CNBI Gaussian Classifier');
M = {};
C = {};

if(~has_init)
    
    disp('[wtk_train_gau] Initializing with SOM.');
    [M{1}, C{1}] = gauInitialization([P Pk], ...
	gaucfg.somunits, ...
	gaucfg.sharedcov, 0);

else
    
    disp('[wtk_train_gau] Initializing with loaded initial classifier.');
    M{1} = gaustr.M;
    C{1} = gaustr.C;
end

perfP = nan(1, gaucfg.epochs);
perfT = perfP;
rejP = perfP;
rejT = perfP;
ccP = perfP;
ccT = perfP;
confP = {};
confT = {};
epMax = 0;

disp('[wtk_train_gau] Evaluation of gaussian.');
for ep = 1:gaucfg.epochs
	[perfP(ep), rejP(ep), confP{ep}] = eval_GAU(gaucfg.th, P, Pk, M{end}, C{end});
	[perfT(ep), rejT(ep), confT{ep}] = eval_GAU(gaucfg.th, T, Tk, M{end}, C{end});
	
	ccP(ep) = eegc3_channel_capacity(1 - perfP(ep), rejP(ep), 2);
	ccT(ep) = eegc3_channel_capacity(1 - perfT(ep), rejT(ep), 2);
	
	fprintf('  Epoch %d/%d: %.3f/%.3f %.3f/%.3f\n', ...
		ep, gaucfg.epochs, ...
		perfP(ep), rejP(ep), perfT(ep), rejT(ep));
	
	plot_GAU(gaucfg.epochs, 60, perfP, rejP, perfT, rejT, ccP, ccT); 


	if(gaucfg.terminate) 
		if(perfP(end) >= perfP(end-1) && perfT(end) <= perfT(end-1) && ep > 1)
			epMax = ep - 1;
			break;
		end
	end
	
	[M{end+1}, C{end+1}] = gauUpdate(M{end}, C{end}, [P Pk], ...
		gaucfg.mimean, gaucfg.micov, gaucfg.sharedcov);
end


if(~gaucfg.terminate)
	fprintf('  Termination criterion met\n');
else
	fprintf('  Checking for max channel capacity\n');

    %Note by M.Tavella <michele.tavella@epfl.ch> on 01/09/09 11:03:11
	% Avoid overtraining
	%[perfMax, epMax] = max(ccP);
	[perfMax, epMax] = max(ccP(2:end));
	epMax = epMax + 1;
end
gau.M = M{epMax};
gau.C = C{epMax};

fprintf('  Epoch %d: %.3f/%.3f [%.3f/%.3f %.3f/%.3f]\n', ...
	epMax, ...
	ccP(epMax), ccT(epMax), ...
	perfP(epMax), rejP(epMax), ...
	perfT(epMax), rejT(epMax));


function [perf, rej] = eval_GAU2(gauth, dataset, labels, M, C)
	
	post = zeros(eegc2_cdataset_size(dataset, 's'), 2);

	for s = 1:size(dataset, 1)
		[act post(s, :)] = gauClassifier(M, C, dataset(s, :));
	end
	
	[perf, rej] = eegc2_prob_soft(labels, post, ...
		[1 2], gauth);

function [perf, rej, conf] = eval_GAU(gauth, dataset, labels, M, C)
	[cm, pv] = gauEval(M, C, [dataset labels], gauth);
	perf = 1.00 - pv(2);
	rej = pv(3);
	conf = cm(end/2:end, :);

function plot_GAU(gauepochs, fig, pP, rP, pT, rT, ccP, ccT)
	eegc3_figure(fig);
	clf;
	subplot(2, 1, 1);
		hold on;
			plot(pP, 'k', 'LineWidth', 1);
			plot(rP, 'r', 'LineWidth', 1);
			plot(pT, 'k', 'LineWidth', 2);
			plot(rT, 'r', 'LineWidth', 2); 
		hold off;
		axis([1 gauepochs 0.00 1.00]);
		grid on;
		title('GAU training');
		ylabel('Accuracy/Rejection')
	subplot(2, 1, 2);
		hold on;
			plot(ccP, 'b', 'LineWidth', 1);
			plot(ccT, 'b', 'LineWidth', 2);
		hold off;
		axis([1 gauepochs 0.00 1.00]);
		grid on;
		xlabel('Epochs')
		ylabel('Channel Capacity')
	drawnow;
