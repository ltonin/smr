function out = get_gdfevt(in, evtlist)

    if ischar(in)
        out = getvalue(in, evtlist.value, evtlist.label);
    elseif iscell(in)
        out = getvaluelist(in, evtlist.value, evtlist.label);
    elseif (isnumeric(in) && length(in) == 1)
        out = getlabel(in, evtlist.value, evtlist.label);
    elseif isnumeric(in)
        out = getlabellist(in, evtlist.value, evtlist.label);
    else 
        error('chk:input', 'Only numeric and char value are allowed');
    end


end

function out = getvalue(in, values, labels)
        id = util_cellfind(labels, {in});
        
        if isempty(id)
            error('chk:id', ['No label find equal to: ' in]);
        end
        
        out = values(id);
end

function out = getlabel(in, values, labels)
        id = values == in;
        
        if sum(id) == 0
            error('chk:id', ['No event find equal to: ' in]);
        end
        
        out = labels{id};
end

function out = getvaluelist(in, values, labels)
    
    numIn = length(in);
    out = zeros(numIn, 1);
    
    for i = 1:numIn
       out(i) = getvalue(in{i}, values, labels); 
    end
end

function out = getlabellist(in, values, labels)
    numIn = length(in);
    out = cell(numIn, 1);
    
    for i = 1:numIn
       out{i} = getlabel(in(i), values, labels); 
    end
end